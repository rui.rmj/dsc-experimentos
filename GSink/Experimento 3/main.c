# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <pthread.h>
# include <semaphore.h>
# include "include/const.h"
# include "include/types.h"
# include "include/fnc.h"
 
 
// Declaration of Functions used by the trheads 
int R1Function(reg_sink *ps); 

//Global Variables
reg_multiset *multiset;     // Multiset elements list
reg_vertex *vertices;       // Graph - list of vertices and its edges
reg_sink *sinks;            // Sink list
reg_I *instances_I1;        // Instances List (I1)
sem_t sem_multiset;       	// Semaphore to control the shared memory access - the multiset
sem_t sem_graph;   			// Semaphore to control the shared memory access - the graph
int id_element;             // Multiset Id element
 
// Main
int main (void){ 
 
	/************ VARIABLES DECLARATION ************/
	// Local Variables
	reg_multiset *multiset_M1;                      // Copy of Multiset Elements List (M1)
	reg_I *usedinstances_I1;                        // Used Instances List (I1) - store the history of instances provided

	// Local Auxiliary Variables
	reg_multiset *pm;
	reg_vertex *pv;
	reg_sink *ps;
	reg_I element_I1; 
	reg_I *pi, *pix; 
	reg_data data; 
	int id_vertex, i; 
	int allow_1 = FALSE;                   // To allow the opportunity to generate again some instances...  
	pthread_t threads[THREADS]; 
 
 
	/********** VARIABLES INITIALIZATION ***********/ 
	// Initializing pointers 
	vertices = (reg_vertex *) NULL; 
	multiset = (reg_multiset *) NULL;  
	sinks = (reg_sink *) NULL; 
	instances_I1 = (reg_I *) NULL;  
	usedinstances_I1 = (reg_I *) NULL;  
	multiset_M1 = (reg_multiset *) NULL;  

	// Initializing variables 
	id_vertex = 0;                  //General Identifier for each vertex included 
	id_element = 0;                 //General Identifier for each multiset element included 
 
	//Initializing Semaphores 
	sem_init(&sem_multiset, 0, 1); 
	sem_init(&sem_graph, 0, 1); 
 
 
	/************** INITIAL MULTISET ***************/ 
	pm = AddMultisetElement (&id_element, 1); 
	pm = AddMultisetElement (&id_element, 2); 
	pm = AddMultisetElement (&id_element, 3); 
	pm = AddMultisetElement (&id_element, 4); 
	pm = AddMultisetElement (&id_element, 5); 
	pm = AddMultisetElement (&id_element, 6); 
	pm = AddMultisetElement (&id_element, 7); 
	pm = AddMultisetElement (&id_element, 8); 
	pm = AddMultisetElement (&id_element, 9); 
	pm = AddMultisetElement (&id_element, 10); 

 
	/************ INSTANCES GENERATION *************/
	GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);

 
	/****************** MAIN LOOP ******************/
	while ((instances_I1 != NULL)) { 
 
		/*************** GRAPH GENERATION **************/
		//Creating Initial Graph (from I1 Instances List) 
		pix = instances_I1; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 1); 
			CreateOutputEdges (data, 1); 
			element_I1.data = pix->data; 
			element_I1.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I1, element_I1); 
			pix = pix->next; 
		} 
 
 
		/************ SINKS IDENTIFICATION *************/ 
		IdentifySinks(); 
 
 
		/*********** THREADS INITIALIZATION ************/ 
		while(vertices != NULL){ 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_create(&threads[ps->id], NULL, ThreadFunction, (void *)(&(*ps))); 
				ps = ps->next; 
			} 
			// Finishing the threads 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_join(threads[ps->id], NULL); 
				ps = ps->next; 
			} 
 
			// Delete the sink list before starting another sinks identification... 
			DeleteAllSinks(); 
 
			// Identify new sinks to start another iteration... 
			IdentifySinks(); 
		} 
 
 
		/********** NEW INSTANCES GENERATION ***********/
		DeleteAllInstanceIx (&instances_I1);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);

 
		//Allowing the opportunity to generate again some instances...
		if((instances_I1==NULL) && (!allow_1)){
			allow_1 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I1);
			GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
		}
 
	}		// End of the Main Loop 
 
 
	// Deleting the history of instances provided to both reactions... 
	DeleteAllInstanceIx (&usedinstances_I1); 

 
	printf("\n \n"); 
	printf("######################## \n"); 
	printf("END OF THE PROGRAM! \n \n"); 
	printf("Final Multiset: \n \n"); 
	ListingMultiset(); 
	printf("\n"); 
	printf("######################## \n"); 
 
	exit (0); 
} 
 
 
/***************************************************************/ 
/*******  Function related to the R1 reaction execution ********/ 
/***************************************************************/ 
int R1Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: factorial 
	int x, y, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 
 
	x  = ps->data.element1; 
	y  = ps->data.element2; 
	element_Ix.data.element1 = ps->data.element1; 
	element_Ix.data.element2 = ps->data.element2; 
	result = (x * y); 
 
	if (1) { 
		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I1, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 

/***************************************************************/ 
/*******  Function for the Threads fired by the Sinks **********/ 
/***************************************************************/ 
void * ThreadFunction (void *arg){ 
	reg_sink *ps; 
	reg_multiset *pm; 
	ps = (reg_sink *)(arg); 
	int reacted; 
 
	/************** REACTION EXECUTION *************/ 
	if (ps->reaction == 1){   // Try to execute the reaction 1 ... 
		reacted = R1Function(ps); 
	} 

	sem_wait(&sem_graph); 
 
	EdgesReversalSink(ps); 
 
	/********* VERTICES AFFECTED REMOVAL ***********/ 
	/******************** AND **********************/ 
	/**************** SINK REMOVAL *****************/ 
 
	if (reacted){ 
		DeleteVerticesAffected(ps);     // all vertices affected, included its own vertex 
	} 
	else { 
		DeleteVertex(ps->data, ps->reaction);   // Delete the vertex that execute as a Sink 
	} 
 
	sem_post(&sem_graph);  
 
}  
 
// Tests Rui - Definitions 
// Program->p_defs->d_definition->d_name: factorial 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: x 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: y 

