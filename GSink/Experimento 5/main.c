# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <pthread.h>
# include <semaphore.h>
# include "include/const.h"
# include "include/types.h"
# include "include/fnc.h"
 
 
// Declaration of Functions used by the trheads 
int R1Function(reg_sink *ps); 
int R2Function(reg_sink *ps); 

//Global Variables
reg_multiset *multiset;     // Multiset elements list
reg_vertex *vertices;       // Graph - list of vertices and its edges
reg_sink *sinks;            // Sink list
reg_I *instances_I1;        // Instances List (I1)
reg_I *instances_I2;        // Instances List (I2)
sem_t sem_multiset;       	// Semaphore to control the shared memory access - the multiset
sem_t sem_graph;   			// Semaphore to control the shared memory access - the graph
int id_element;             // Multiset Id element
 
// Main
int main (void){ 
 
	/************ VARIABLES DECLARATION ************/
	// Local Variables
	reg_multiset *multiset_M1;                      // Copy of Multiset Elements List (M1)
	reg_I *usedinstances_I1;                        // Used Instances List (I1) - store the history of instances provided
	reg_multiset *multiset_M2;                      // Copy of Multiset Elements List (M2)
	reg_I *usedinstances_I2;                        // Used Instances List (I2) - store the history of instances provided

	// Local Auxiliary Variables
	reg_multiset *pm;
	reg_vertex *pv;
	reg_sink *ps;
	reg_I element_I1; 
	reg_I element_I2; 
	reg_I *pi, *pix; 
	reg_data data; 
	int id_vertex, i; 
	int allow_1 = FALSE;                   // To allow the opportunity to generate again some instances...  
	int allow_2 = FALSE;                   // To allow the opportunity to generate again some instances...  
	pthread_t threads[THREADS]; 
 
 
	/********** VARIABLES INITIALIZATION ***********/ 
	// Initializing pointers 
	vertices = (reg_vertex *) NULL; 
	multiset = (reg_multiset *) NULL;  
	sinks = (reg_sink *) NULL; 
	instances_I1 = (reg_I *) NULL;  
	usedinstances_I1 = (reg_I *) NULL;  
	multiset_M1 = (reg_multiset *) NULL;  
	instances_I2 = (reg_I *) NULL;  
	usedinstances_I2 = (reg_I *) NULL;  
	multiset_M2 = (reg_multiset *) NULL;  

	// Initializing variables 
	id_vertex = 0;                  //General Identifier for each vertex included 
	id_element = 0;                 //General Identifier for each multiset element included 
 
	//Initializing Semaphores 
	sem_init(&sem_multiset, 0, 1); 
	sem_init(&sem_graph, 0, 1); 
 
 
	/************** INITIAL MULTISET ***************/ 
	pm = AddMultisetElement (&id_element, 1); 
	pm = AddMultisetElement (&id_element, 2); 
	pm = AddMultisetElement (&id_element, 3); 
	pm = AddMultisetElement (&id_element, 4); 
	pm = AddMultisetElement (&id_element, 5); 
	pm = AddMultisetElement (&id_element, 6); 
	pm = AddMultisetElement (&id_element, 7); 
	pm = AddMultisetElement (&id_element, 8); 
	pm = AddMultisetElement (&id_element, 9); 
	pm = AddMultisetElement (&id_element, 10); 
	pm = AddMultisetElement (&id_element, 11); 
	pm = AddMultisetElement (&id_element, 12); 
	pm = AddMultisetElement (&id_element, 13); 
	pm = AddMultisetElement (&id_element, 14); 
	pm = AddMultisetElement (&id_element, 15); 
	pm = AddMultisetElement (&id_element, 16); 
	pm = AddMultisetElement (&id_element, 17); 
	pm = AddMultisetElement (&id_element, 18); 
	pm = AddMultisetElement (&id_element, 19); 
	pm = AddMultisetElement (&id_element, 20); 
	pm = AddMultisetElement (&id_element, 21); 
	pm = AddMultisetElement (&id_element, 22); 
	pm = AddMultisetElement (&id_element, 23); 
	pm = AddMultisetElement (&id_element, 24); 
	pm = AddMultisetElement (&id_element, 25); 
	pm = AddMultisetElement (&id_element, 26); 
	pm = AddMultisetElement (&id_element, 27); 
	pm = AddMultisetElement (&id_element, 28); 
	pm = AddMultisetElement (&id_element, 29); 
	pm = AddMultisetElement (&id_element, 30); 
	pm = AddMultisetElement (&id_element, 31); 
	pm = AddMultisetElement (&id_element, 32); 
	pm = AddMultisetElement (&id_element, 33); 
	pm = AddMultisetElement (&id_element, 34); 
	pm = AddMultisetElement (&id_element, 35); 
	pm = AddMultisetElement (&id_element, 36); 
	pm = AddMultisetElement (&id_element, 37); 
	pm = AddMultisetElement (&id_element, 38); 
	pm = AddMultisetElement (&id_element, 39); 
	pm = AddMultisetElement (&id_element, 40); 
	pm = AddMultisetElement (&id_element, 41); 
	pm = AddMultisetElement (&id_element, 42); 
	pm = AddMultisetElement (&id_element, 43); 
	pm = AddMultisetElement (&id_element, 44); 
	pm = AddMultisetElement (&id_element, 45); 
	pm = AddMultisetElement (&id_element, 46); 
	pm = AddMultisetElement (&id_element, 47); 
	pm = AddMultisetElement (&id_element, 48); 
	pm = AddMultisetElement (&id_element, 49); 
	pm = AddMultisetElement (&id_element, 50); 
	pm = AddMultisetElement (&id_element, 51); 
	pm = AddMultisetElement (&id_element, 52); 
	pm = AddMultisetElement (&id_element, 53); 
	pm = AddMultisetElement (&id_element, 54); 
	pm = AddMultisetElement (&id_element, 55); 
	pm = AddMultisetElement (&id_element, 56); 
	pm = AddMultisetElement (&id_element, 57); 
	pm = AddMultisetElement (&id_element, 58); 
	pm = AddMultisetElement (&id_element, 59); 
	pm = AddMultisetElement (&id_element, 60); 
	pm = AddMultisetElement (&id_element, 61); 
	pm = AddMultisetElement (&id_element, 62); 
	pm = AddMultisetElement (&id_element, 63); 
	pm = AddMultisetElement (&id_element, 64); 
	pm = AddMultisetElement (&id_element, 65); 
	pm = AddMultisetElement (&id_element, 66); 
	pm = AddMultisetElement (&id_element, 67); 
	pm = AddMultisetElement (&id_element, 68); 
	pm = AddMultisetElement (&id_element, 69); 
	pm = AddMultisetElement (&id_element, 70); 
	pm = AddMultisetElement (&id_element, 71); 
	pm = AddMultisetElement (&id_element, 72); 
	pm = AddMultisetElement (&id_element, 73); 
	pm = AddMultisetElement (&id_element, 74); 
	pm = AddMultisetElement (&id_element, 75); 
	pm = AddMultisetElement (&id_element, 76); 
	pm = AddMultisetElement (&id_element, 77); 
	pm = AddMultisetElement (&id_element, 78); 
	pm = AddMultisetElement (&id_element, 79); 
	pm = AddMultisetElement (&id_element, 80); 
	pm = AddMultisetElement (&id_element, 81); 
	pm = AddMultisetElement (&id_element, 82); 
	pm = AddMultisetElement (&id_element, 83); 
	pm = AddMultisetElement (&id_element, 84); 
	pm = AddMultisetElement (&id_element, 85); 
	pm = AddMultisetElement (&id_element, 86); 
	pm = AddMultisetElement (&id_element, 87); 
	pm = AddMultisetElement (&id_element, 88); 
	pm = AddMultisetElement (&id_element, 89); 
	pm = AddMultisetElement (&id_element, 90); 
	pm = AddMultisetElement (&id_element, 91); 
	pm = AddMultisetElement (&id_element, 92); 
	pm = AddMultisetElement (&id_element, 93); 
	pm = AddMultisetElement (&id_element, 94); 
	pm = AddMultisetElement (&id_element, 95); 
	pm = AddMultisetElement (&id_element, 96); 
	pm = AddMultisetElement (&id_element, 97); 
	pm = AddMultisetElement (&id_element, 98); 
	pm = AddMultisetElement (&id_element, 99); 
	pm = AddMultisetElement (&id_element, 100); 

 
	/************ INSTANCES GENERATION *************/
	GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
	GeneratesInstancesRx (&multiset_M2, &instances_I2, &usedinstances_I2);

 
	/****************** MAIN LOOP ******************/
	while ((instances_I1 != NULL) || (instances_I2 != NULL)) { 
 
		/*************** GRAPH GENERATION **************/
		//Creating Initial Graph (from I1 Instances List) 
		pix = instances_I1; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 1); 
			CreateOutputEdges (data, 1); 
			element_I1.data = pix->data; 
			element_I1.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I1, element_I1); 
			pix = pix->next; 
		} 
 
 
		//Creating Initial Graph (from I2 Instances List) 
		pix = instances_I2; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 2); 
			CreateOutputEdges (data, 2); 
			element_I2.data = pix->data; 
			element_I2.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I2, element_I2); 
			pix = pix->next; 
		} 
 
 
		/************ SINKS IDENTIFICATION *************/ 
		IdentifySinks(); 
 
 
		/*********** THREADS INITIALIZATION ************/ 
		while(vertices != NULL){ 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_create(&threads[ps->id], NULL, ThreadFunction, (void *)(&(*ps))); 
				ps = ps->next; 
			} 
			// Finishing the threads 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_join(threads[ps->id], NULL); 
				ps = ps->next; 
			} 
 
			// Delete the sink list before starting another sinks identification... 
			DeleteAllSinks(); 
 
			// Identify new sinks to start another iteration... 
			IdentifySinks(); 
		} 
 
 
		/********** NEW INSTANCES GENERATION ***********/
		DeleteAllInstanceIx (&instances_I1);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
		DeleteAllInstanceIx (&instances_I2);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M2, &instances_I2, &usedinstances_I2);

 
		//Allowing the opportunity to generate again some instances...
		if((instances_I1==NULL) && (!allow_1)){
			allow_1 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I1);
			GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
		}
 
		//Allowing the opportunity to generate again some instances...
		if((instances_I2==NULL) && (!allow_2)){
			allow_2 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I2);
			GeneratesInstancesRx (&multiset_M2, &instances_I2, &usedinstances_I2);
		}
 
	}		// End of the Main Loop 
 
 
	// Deleting the history of instances provided to both reactions... 
	DeleteAllInstanceIx (&usedinstances_I1); 
	DeleteAllInstanceIx (&usedinstances_I2); 

 
	printf("\n \n"); 
	printf("######################## \n"); 
	printf("END OF THE PROGRAM! \n \n"); 
	printf("Final Multiset: \n \n"); 
	ListingMultiset(); 
	printf("\n"); 
	printf("######################## \n"); 
 
	exit (0); 
} 
 
 
/***************************************************************/ 
/*******  Function related to the R1 reaction execution ********/ 
/***************************************************************/ 
int R1Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: R1 
	int x, y, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 
 
	x  = ps->data.element1; 
	y  = ps->data.element2; 
	element_Ix.data.element1 = ps->data.element1; 
	element_Ix.data.element2 = ps->data.element2; 
	result = (x + y); 
 
	if ((x >  20 )) { 
		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I1, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 
/***************************************************************/ 
/*******  Function related to the R2 reaction execution ********/ 
/***************************************************************/ 
int R2Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: R2 
	int a, b, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 
 
	a  = ps->data.element1; 
	b  = ps->data.element2; 
	element_Ix.data.element1 = ps->data.element1; 
	element_Ix.data.element2 = ps->data.element2; 
	result = (a * b); 
 
	if (((a + b) <  100 )) { 
		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I2, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 

/***************************************************************/ 
/*******  Function for the Threads fired by the Sinks **********/ 
/***************************************************************/ 
void * ThreadFunction (void *arg){ 
	reg_sink *ps; 
	reg_multiset *pm; 
	ps = (reg_sink *)(arg); 
	int reacted; 
 
	/************** REACTION EXECUTION *************/ 
	if (ps->reaction == 1){   // Try to execute the reaction 1 ... 
		reacted = R1Function(ps); 
	} 
	if (ps->reaction == 2){   // Try to execute the reaction 2 ... 
		reacted = R2Function(ps); 
	} 

	sem_wait(&sem_graph); 
 
	EdgesReversalSink(ps); 
 
	/********* VERTICES AFFECTED REMOVAL ***********/ 
	/******************** AND **********************/ 
	/**************** SINK REMOVAL *****************/ 
 
	if (reacted){ 
		DeleteVerticesAffected(ps);     // all vertices affected, included its own vertex 
	} 
	else { 
		DeleteVertex(ps->data, ps->reaction);   // Delete the vertex that execute as a Sink 
	} 
 
	sem_post(&sem_graph);  
 
}  
 
// Tests Rui - Definitions 
// Program->p_defs->d_definition->d_name: R1 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: x 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: y 

// Program->p_defs->d_definition->d_name: R2 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: a 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: b 

