# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <pthread.h>
# include <semaphore.h>
# include "include/const.h"
# include "include/types.h"
# include "include/fnc.h"
 
 
// Declaration of Functions used by the trheads 
int R1Function(reg_sink *ps); 

//Global Variables
reg_multiset *multiset;     // Multiset elements list
reg_vertex *vertices;       // Graph - list of vertices and its edges
reg_sink *sinks;            // Sink list
reg_I *instances_I1;        // Instances List (I1)
sem_t sem_multiset;       	// Semaphore to control the shared memory access - the multiset
sem_t sem_graph;   			// Semaphore to control the shared memory access - the graph
int id_element;             // Multiset Id element
 
// Main
int main (void){ 
 
	/************ VARIABLES DECLARATION ************/
	// Local Variables
	reg_multiset *multiset_M1;                      // Copy of Multiset Elements List (M1)
	reg_I *usedinstances_I1;                        // Used Instances List (I1) - store the history of instances provided

	// Local Auxiliary Variables
	reg_multiset *pm;
	reg_vertex *pv;
	reg_sink *ps;
	reg_I element_I1; 
	reg_I *pi, *pix; 
	reg_data data; 
	int id_vertex, i; 
	int allow_1 = FALSE;                   // To allow the opportunity to generate again some instances...  
	pthread_t threads[THREADS]; 
 
 
	/********** VARIABLES INITIALIZATION ***********/ 
	// Initializing pointers 
	vertices = (reg_vertex *) NULL; 
	multiset = (reg_multiset *) NULL;  
	sinks = (reg_sink *) NULL; 
	instances_I1 = (reg_I *) NULL;  
	usedinstances_I1 = (reg_I *) NULL;  
	multiset_M1 = (reg_multiset *) NULL;  

	// Initializing variables 
	id_vertex = 0;                  //General Identifier for each vertex included 
	id_element = 0;                 //General Identifier for each multiset element included 
 
	//Initializing Semaphores 
	sem_init(&sem_multiset, 0, 1); 
	sem_init(&sem_graph, 0, 1); 
 
 
	/************** INITIAL MULTISET ***************/ 
	pm = AddMultisetElement (&id_element, 1); 
	pm = AddMultisetElement (&id_element, 2); 
	pm = AddMultisetElement (&id_element, 3); 
	pm = AddMultisetElement (&id_element, 4); 
	pm = AddMultisetElement (&id_element, 5); 
	pm = AddMultisetElement (&id_element, 6); 
	pm = AddMultisetElement (&id_element, 7); 
	pm = AddMultisetElement (&id_element, 8); 
	pm = AddMultisetElement (&id_element, 9); 
	pm = AddMultisetElement (&id_element, 10); 
	pm = AddMultisetElement (&id_element, 11); 
	pm = AddMultisetElement (&id_element, 12); 
	pm = AddMultisetElement (&id_element, 13); 
	pm = AddMultisetElement (&id_element, 14); 
	pm = AddMultisetElement (&id_element, 15); 
	pm = AddMultisetElement (&id_element, 16); 
	pm = AddMultisetElement (&id_element, 17); 
	pm = AddMultisetElement (&id_element, 18); 
	pm = AddMultisetElement (&id_element, 19); 
	pm = AddMultisetElement (&id_element, 20); 
	pm = AddMultisetElement (&id_element, 21); 
	pm = AddMultisetElement (&id_element, 22); 
	pm = AddMultisetElement (&id_element, 23); 
	pm = AddMultisetElement (&id_element, 24); 
	pm = AddMultisetElement (&id_element, 25); 
	pm = AddMultisetElement (&id_element, 26); 
	pm = AddMultisetElement (&id_element, 27); 
	pm = AddMultisetElement (&id_element, 28); 
	pm = AddMultisetElement (&id_element, 29); 
	pm = AddMultisetElement (&id_element, 30); 
	pm = AddMultisetElement (&id_element, 31); 
	pm = AddMultisetElement (&id_element, 32); 
	pm = AddMultisetElement (&id_element, 33); 
	pm = AddMultisetElement (&id_element, 34); 
	pm = AddMultisetElement (&id_element, 35); 
	pm = AddMultisetElement (&id_element, 36); 
	pm = AddMultisetElement (&id_element, 37); 
	pm = AddMultisetElement (&id_element, 38); 
	pm = AddMultisetElement (&id_element, 39); 
	pm = AddMultisetElement (&id_element, 40); 
	pm = AddMultisetElement (&id_element, 41); 
	pm = AddMultisetElement (&id_element, 42); 
	pm = AddMultisetElement (&id_element, 43); 
	pm = AddMultisetElement (&id_element, 44); 
	pm = AddMultisetElement (&id_element, 45); 
	pm = AddMultisetElement (&id_element, 46); 
	pm = AddMultisetElement (&id_element, 47); 
	pm = AddMultisetElement (&id_element, 48); 
	pm = AddMultisetElement (&id_element, 49); 
	pm = AddMultisetElement (&id_element, 50); 
	pm = AddMultisetElement (&id_element, 51); 
	pm = AddMultisetElement (&id_element, 52); 
	pm = AddMultisetElement (&id_element, 53); 
	pm = AddMultisetElement (&id_element, 54); 
	pm = AddMultisetElement (&id_element, 55); 
	pm = AddMultisetElement (&id_element, 56); 
	pm = AddMultisetElement (&id_element, 57); 
	pm = AddMultisetElement (&id_element, 58); 
	pm = AddMultisetElement (&id_element, 59); 
	pm = AddMultisetElement (&id_element, 60); 
	pm = AddMultisetElement (&id_element, 61); 
	pm = AddMultisetElement (&id_element, 62); 
	pm = AddMultisetElement (&id_element, 63); 
	pm = AddMultisetElement (&id_element, 64); 
	pm = AddMultisetElement (&id_element, 65); 
	pm = AddMultisetElement (&id_element, 66); 
	pm = AddMultisetElement (&id_element, 67); 
	pm = AddMultisetElement (&id_element, 68); 
	pm = AddMultisetElement (&id_element, 69); 
	pm = AddMultisetElement (&id_element, 70); 
	pm = AddMultisetElement (&id_element, 71); 
	pm = AddMultisetElement (&id_element, 72); 
	pm = AddMultisetElement (&id_element, 73); 
	pm = AddMultisetElement (&id_element, 74); 
	pm = AddMultisetElement (&id_element, 75); 
	pm = AddMultisetElement (&id_element, 76); 
	pm = AddMultisetElement (&id_element, 77); 
	pm = AddMultisetElement (&id_element, 78); 
	pm = AddMultisetElement (&id_element, 79); 
	pm = AddMultisetElement (&id_element, 80); 
	pm = AddMultisetElement (&id_element, 81); 
	pm = AddMultisetElement (&id_element, 82); 
	pm = AddMultisetElement (&id_element, 83); 
	pm = AddMultisetElement (&id_element, 84); 
	pm = AddMultisetElement (&id_element, 85); 
	pm = AddMultisetElement (&id_element, 86); 
	pm = AddMultisetElement (&id_element, 87); 
	pm = AddMultisetElement (&id_element, 88); 
	pm = AddMultisetElement (&id_element, 89); 
	pm = AddMultisetElement (&id_element, 90); 
	pm = AddMultisetElement (&id_element, 91); 
	pm = AddMultisetElement (&id_element, 92); 
	pm = AddMultisetElement (&id_element, 93); 
	pm = AddMultisetElement (&id_element, 94); 
	pm = AddMultisetElement (&id_element, 95); 
	pm = AddMultisetElement (&id_element, 96); 
	pm = AddMultisetElement (&id_element, 97); 
	pm = AddMultisetElement (&id_element, 98); 
	pm = AddMultisetElement (&id_element, 99); 
	pm = AddMultisetElement (&id_element, 100); 
	pm = AddMultisetElement (&id_element, 101); 
	pm = AddMultisetElement (&id_element, 102); 
	pm = AddMultisetElement (&id_element, 103); 
	pm = AddMultisetElement (&id_element, 104); 
	pm = AddMultisetElement (&id_element, 105); 
	pm = AddMultisetElement (&id_element, 106); 
	pm = AddMultisetElement (&id_element, 107); 
	pm = AddMultisetElement (&id_element, 108); 
	pm = AddMultisetElement (&id_element, 109); 
	pm = AddMultisetElement (&id_element, 110); 
	pm = AddMultisetElement (&id_element, 111); 
	pm = AddMultisetElement (&id_element, 112); 
	pm = AddMultisetElement (&id_element, 113); 
	pm = AddMultisetElement (&id_element, 114); 
	pm = AddMultisetElement (&id_element, 115); 
	pm = AddMultisetElement (&id_element, 116); 
	pm = AddMultisetElement (&id_element, 117); 
	pm = AddMultisetElement (&id_element, 118); 
	pm = AddMultisetElement (&id_element, 119); 
	pm = AddMultisetElement (&id_element, 120); 
	pm = AddMultisetElement (&id_element, 121); 
	pm = AddMultisetElement (&id_element, 122); 
	pm = AddMultisetElement (&id_element, 123); 
	pm = AddMultisetElement (&id_element, 124); 
	pm = AddMultisetElement (&id_element, 125); 
	pm = AddMultisetElement (&id_element, 126); 
	pm = AddMultisetElement (&id_element, 127); 
	pm = AddMultisetElement (&id_element, 128); 
	pm = AddMultisetElement (&id_element, 129); 
	pm = AddMultisetElement (&id_element, 130); 
	pm = AddMultisetElement (&id_element, 131); 
	pm = AddMultisetElement (&id_element, 132); 
	pm = AddMultisetElement (&id_element, 133); 
	pm = AddMultisetElement (&id_element, 134); 
	pm = AddMultisetElement (&id_element, 135); 
	pm = AddMultisetElement (&id_element, 136); 
	pm = AddMultisetElement (&id_element, 137); 
	pm = AddMultisetElement (&id_element, 138); 
	pm = AddMultisetElement (&id_element, 139); 
	pm = AddMultisetElement (&id_element, 140); 
	pm = AddMultisetElement (&id_element, 141); 
	pm = AddMultisetElement (&id_element, 142); 
	pm = AddMultisetElement (&id_element, 143); 
	pm = AddMultisetElement (&id_element, 144); 
	pm = AddMultisetElement (&id_element, 145); 
	pm = AddMultisetElement (&id_element, 146); 
	pm = AddMultisetElement (&id_element, 147); 
	pm = AddMultisetElement (&id_element, 148); 
	pm = AddMultisetElement (&id_element, 149); 
	pm = AddMultisetElement (&id_element, 150); 
	pm = AddMultisetElement (&id_element, 151); 
	pm = AddMultisetElement (&id_element, 152); 
	pm = AddMultisetElement (&id_element, 153); 
	pm = AddMultisetElement (&id_element, 154); 
	pm = AddMultisetElement (&id_element, 155); 
	pm = AddMultisetElement (&id_element, 156); 
	pm = AddMultisetElement (&id_element, 157); 
	pm = AddMultisetElement (&id_element, 158); 
	pm = AddMultisetElement (&id_element, 159); 
	pm = AddMultisetElement (&id_element, 160); 
	pm = AddMultisetElement (&id_element, 161); 
	pm = AddMultisetElement (&id_element, 162); 
	pm = AddMultisetElement (&id_element, 163); 
	pm = AddMultisetElement (&id_element, 164); 
	pm = AddMultisetElement (&id_element, 165); 
	pm = AddMultisetElement (&id_element, 166); 
	pm = AddMultisetElement (&id_element, 167); 
	pm = AddMultisetElement (&id_element, 168); 
	pm = AddMultisetElement (&id_element, 169); 
	pm = AddMultisetElement (&id_element, 170); 
	pm = AddMultisetElement (&id_element, 171); 
	pm = AddMultisetElement (&id_element, 172); 
	pm = AddMultisetElement (&id_element, 173); 
	pm = AddMultisetElement (&id_element, 174); 
	pm = AddMultisetElement (&id_element, 175); 
	pm = AddMultisetElement (&id_element, 176); 
	pm = AddMultisetElement (&id_element, 177); 
	pm = AddMultisetElement (&id_element, 178); 
	pm = AddMultisetElement (&id_element, 179); 
	pm = AddMultisetElement (&id_element, 180); 
	pm = AddMultisetElement (&id_element, 181); 
	pm = AddMultisetElement (&id_element, 182); 
	pm = AddMultisetElement (&id_element, 183); 
	pm = AddMultisetElement (&id_element, 184); 
	pm = AddMultisetElement (&id_element, 185); 
	pm = AddMultisetElement (&id_element, 186); 
	pm = AddMultisetElement (&id_element, 187); 
	pm = AddMultisetElement (&id_element, 188); 
	pm = AddMultisetElement (&id_element, 189); 
	pm = AddMultisetElement (&id_element, 190); 
	pm = AddMultisetElement (&id_element, 191); 
	pm = AddMultisetElement (&id_element, 192); 
	pm = AddMultisetElement (&id_element, 193); 
	pm = AddMultisetElement (&id_element, 194); 
	pm = AddMultisetElement (&id_element, 195); 
	pm = AddMultisetElement (&id_element, 196); 
	pm = AddMultisetElement (&id_element, 197); 
	pm = AddMultisetElement (&id_element, 198); 
	pm = AddMultisetElement (&id_element, 199); 
	pm = AddMultisetElement (&id_element, 200); 
	pm = AddMultisetElement (&id_element, 201); 
	pm = AddMultisetElement (&id_element, 202); 
	pm = AddMultisetElement (&id_element, 203); 
	pm = AddMultisetElement (&id_element, 204); 
	pm = AddMultisetElement (&id_element, 205); 
	pm = AddMultisetElement (&id_element, 206); 
	pm = AddMultisetElement (&id_element, 207); 
	pm = AddMultisetElement (&id_element, 208); 
	pm = AddMultisetElement (&id_element, 209); 
	pm = AddMultisetElement (&id_element, 210); 
	pm = AddMultisetElement (&id_element, 211); 
	pm = AddMultisetElement (&id_element, 212); 
	pm = AddMultisetElement (&id_element, 213); 
	pm = AddMultisetElement (&id_element, 214); 
	pm = AddMultisetElement (&id_element, 215); 
	pm = AddMultisetElement (&id_element, 216); 
	pm = AddMultisetElement (&id_element, 217); 
	pm = AddMultisetElement (&id_element, 218); 
	pm = AddMultisetElement (&id_element, 219); 
	pm = AddMultisetElement (&id_element, 220); 
	pm = AddMultisetElement (&id_element, 221); 
	pm = AddMultisetElement (&id_element, 222); 
	pm = AddMultisetElement (&id_element, 223); 
	pm = AddMultisetElement (&id_element, 224); 
	pm = AddMultisetElement (&id_element, 225); 
	pm = AddMultisetElement (&id_element, 226); 
	pm = AddMultisetElement (&id_element, 227); 
	pm = AddMultisetElement (&id_element, 228); 
	pm = AddMultisetElement (&id_element, 229); 
	pm = AddMultisetElement (&id_element, 230); 
	pm = AddMultisetElement (&id_element, 231); 
	pm = AddMultisetElement (&id_element, 232); 
	pm = AddMultisetElement (&id_element, 233); 
	pm = AddMultisetElement (&id_element, 234); 
	pm = AddMultisetElement (&id_element, 235); 
	pm = AddMultisetElement (&id_element, 236); 
	pm = AddMultisetElement (&id_element, 237); 
	pm = AddMultisetElement (&id_element, 238); 
	pm = AddMultisetElement (&id_element, 239); 
	pm = AddMultisetElement (&id_element, 240); 
	pm = AddMultisetElement (&id_element, 241); 
	pm = AddMultisetElement (&id_element, 242); 
	pm = AddMultisetElement (&id_element, 243); 
	pm = AddMultisetElement (&id_element, 244); 
	pm = AddMultisetElement (&id_element, 245); 
	pm = AddMultisetElement (&id_element, 246); 
	pm = AddMultisetElement (&id_element, 247); 
	pm = AddMultisetElement (&id_element, 248); 
	pm = AddMultisetElement (&id_element, 249); 
	pm = AddMultisetElement (&id_element, 250); 
	pm = AddMultisetElement (&id_element, 251); 
	pm = AddMultisetElement (&id_element, 252); 
	pm = AddMultisetElement (&id_element, 253); 
	pm = AddMultisetElement (&id_element, 254); 
	pm = AddMultisetElement (&id_element, 255); 
	pm = AddMultisetElement (&id_element, 256); 
	pm = AddMultisetElement (&id_element, 257); 
	pm = AddMultisetElement (&id_element, 258); 
	pm = AddMultisetElement (&id_element, 259); 
	pm = AddMultisetElement (&id_element, 260); 
	pm = AddMultisetElement (&id_element, 261); 
	pm = AddMultisetElement (&id_element, 262); 
	pm = AddMultisetElement (&id_element, 263); 
	pm = AddMultisetElement (&id_element, 264); 
	pm = AddMultisetElement (&id_element, 265); 
	pm = AddMultisetElement (&id_element, 266); 
	pm = AddMultisetElement (&id_element, 267); 
	pm = AddMultisetElement (&id_element, 268); 
	pm = AddMultisetElement (&id_element, 269); 
	pm = AddMultisetElement (&id_element, 270); 
	pm = AddMultisetElement (&id_element, 271); 
	pm = AddMultisetElement (&id_element, 272); 
	pm = AddMultisetElement (&id_element, 273); 
	pm = AddMultisetElement (&id_element, 274); 
	pm = AddMultisetElement (&id_element, 275); 
	pm = AddMultisetElement (&id_element, 276); 
	pm = AddMultisetElement (&id_element, 277); 
	pm = AddMultisetElement (&id_element, 278); 
	pm = AddMultisetElement (&id_element, 279); 
	pm = AddMultisetElement (&id_element, 280); 
	pm = AddMultisetElement (&id_element, 281); 
	pm = AddMultisetElement (&id_element, 282); 
	pm = AddMultisetElement (&id_element, 283); 
	pm = AddMultisetElement (&id_element, 284); 
	pm = AddMultisetElement (&id_element, 285); 
	pm = AddMultisetElement (&id_element, 286); 
	pm = AddMultisetElement (&id_element, 287); 
	pm = AddMultisetElement (&id_element, 288); 
	pm = AddMultisetElement (&id_element, 289); 
	pm = AddMultisetElement (&id_element, 290); 
	pm = AddMultisetElement (&id_element, 291); 
	pm = AddMultisetElement (&id_element, 292); 
	pm = AddMultisetElement (&id_element, 293); 
	pm = AddMultisetElement (&id_element, 294); 
	pm = AddMultisetElement (&id_element, 295); 
	pm = AddMultisetElement (&id_element, 296); 
	pm = AddMultisetElement (&id_element, 297); 
	pm = AddMultisetElement (&id_element, 298); 
	pm = AddMultisetElement (&id_element, 299); 
	pm = AddMultisetElement (&id_element, 300); 
	pm = AddMultisetElement (&id_element, 301); 
	pm = AddMultisetElement (&id_element, 302); 
	pm = AddMultisetElement (&id_element, 303); 
	pm = AddMultisetElement (&id_element, 304); 
	pm = AddMultisetElement (&id_element, 305); 
	pm = AddMultisetElement (&id_element, 306); 
	pm = AddMultisetElement (&id_element, 307); 
	pm = AddMultisetElement (&id_element, 308); 
	pm = AddMultisetElement (&id_element, 309); 
	pm = AddMultisetElement (&id_element, 310); 
	pm = AddMultisetElement (&id_element, 311); 
	pm = AddMultisetElement (&id_element, 312); 
	pm = AddMultisetElement (&id_element, 313); 
	pm = AddMultisetElement (&id_element, 314); 
	pm = AddMultisetElement (&id_element, 315); 
	pm = AddMultisetElement (&id_element, 316); 
	pm = AddMultisetElement (&id_element, 317); 
	pm = AddMultisetElement (&id_element, 318); 
	pm = AddMultisetElement (&id_element, 319); 
	pm = AddMultisetElement (&id_element, 320); 
	pm = AddMultisetElement (&id_element, 321); 
	pm = AddMultisetElement (&id_element, 322); 
	pm = AddMultisetElement (&id_element, 323); 
	pm = AddMultisetElement (&id_element, 324); 
	pm = AddMultisetElement (&id_element, 325); 
	pm = AddMultisetElement (&id_element, 326); 
	pm = AddMultisetElement (&id_element, 327); 
	pm = AddMultisetElement (&id_element, 328); 
	pm = AddMultisetElement (&id_element, 329); 
	pm = AddMultisetElement (&id_element, 330); 
	pm = AddMultisetElement (&id_element, 331); 
	pm = AddMultisetElement (&id_element, 332); 
	pm = AddMultisetElement (&id_element, 333); 
	pm = AddMultisetElement (&id_element, 334); 
	pm = AddMultisetElement (&id_element, 335); 
	pm = AddMultisetElement (&id_element, 336); 
	pm = AddMultisetElement (&id_element, 337); 
	pm = AddMultisetElement (&id_element, 338); 
	pm = AddMultisetElement (&id_element, 339); 
	pm = AddMultisetElement (&id_element, 340); 
	pm = AddMultisetElement (&id_element, 341); 
	pm = AddMultisetElement (&id_element, 342); 
	pm = AddMultisetElement (&id_element, 343); 
	pm = AddMultisetElement (&id_element, 344); 
	pm = AddMultisetElement (&id_element, 345); 
	pm = AddMultisetElement (&id_element, 346); 
	pm = AddMultisetElement (&id_element, 347); 
	pm = AddMultisetElement (&id_element, 348); 
	pm = AddMultisetElement (&id_element, 349); 
	pm = AddMultisetElement (&id_element, 350); 
	pm = AddMultisetElement (&id_element, 351); 
	pm = AddMultisetElement (&id_element, 352); 
	pm = AddMultisetElement (&id_element, 353); 
	pm = AddMultisetElement (&id_element, 354); 
	pm = AddMultisetElement (&id_element, 355); 
	pm = AddMultisetElement (&id_element, 356); 
	pm = AddMultisetElement (&id_element, 357); 
	pm = AddMultisetElement (&id_element, 358); 
	pm = AddMultisetElement (&id_element, 359); 
	pm = AddMultisetElement (&id_element, 360); 
	pm = AddMultisetElement (&id_element, 361); 
	pm = AddMultisetElement (&id_element, 362); 
	pm = AddMultisetElement (&id_element, 363); 
	pm = AddMultisetElement (&id_element, 364); 
	pm = AddMultisetElement (&id_element, 365); 
	pm = AddMultisetElement (&id_element, 366); 
	pm = AddMultisetElement (&id_element, 367); 
	pm = AddMultisetElement (&id_element, 368); 
	pm = AddMultisetElement (&id_element, 369); 
	pm = AddMultisetElement (&id_element, 370); 
	pm = AddMultisetElement (&id_element, 371); 
	pm = AddMultisetElement (&id_element, 372); 
	pm = AddMultisetElement (&id_element, 373); 
	pm = AddMultisetElement (&id_element, 374); 
	pm = AddMultisetElement (&id_element, 375); 
	pm = AddMultisetElement (&id_element, 376); 
	pm = AddMultisetElement (&id_element, 377); 
	pm = AddMultisetElement (&id_element, 378); 
	pm = AddMultisetElement (&id_element, 379); 
	pm = AddMultisetElement (&id_element, 380); 
	pm = AddMultisetElement (&id_element, 381); 
	pm = AddMultisetElement (&id_element, 382); 
	pm = AddMultisetElement (&id_element, 383); 
	pm = AddMultisetElement (&id_element, 384); 
	pm = AddMultisetElement (&id_element, 385); 
	pm = AddMultisetElement (&id_element, 386); 
	pm = AddMultisetElement (&id_element, 387); 
	pm = AddMultisetElement (&id_element, 388); 
	pm = AddMultisetElement (&id_element, 389); 
	pm = AddMultisetElement (&id_element, 390); 
	pm = AddMultisetElement (&id_element, 391); 
	pm = AddMultisetElement (&id_element, 392); 
	pm = AddMultisetElement (&id_element, 393); 
	pm = AddMultisetElement (&id_element, 394); 
	pm = AddMultisetElement (&id_element, 395); 
	pm = AddMultisetElement (&id_element, 396); 
	pm = AddMultisetElement (&id_element, 397); 
	pm = AddMultisetElement (&id_element, 398); 
	pm = AddMultisetElement (&id_element, 399); 
	pm = AddMultisetElement (&id_element, 400); 
	pm = AddMultisetElement (&id_element, 401); 
	pm = AddMultisetElement (&id_element, 402); 
	pm = AddMultisetElement (&id_element, 403); 
	pm = AddMultisetElement (&id_element, 404); 
	pm = AddMultisetElement (&id_element, 405); 
	pm = AddMultisetElement (&id_element, 406); 
	pm = AddMultisetElement (&id_element, 407); 
	pm = AddMultisetElement (&id_element, 408); 
	pm = AddMultisetElement (&id_element, 409); 
	pm = AddMultisetElement (&id_element, 410); 
	pm = AddMultisetElement (&id_element, 411); 
	pm = AddMultisetElement (&id_element, 412); 
	pm = AddMultisetElement (&id_element, 413); 
	pm = AddMultisetElement (&id_element, 414); 
	pm = AddMultisetElement (&id_element, 415); 
	pm = AddMultisetElement (&id_element, 416); 
	pm = AddMultisetElement (&id_element, 417); 
	pm = AddMultisetElement (&id_element, 418); 
	pm = AddMultisetElement (&id_element, 419); 
	pm = AddMultisetElement (&id_element, 420); 
	pm = AddMultisetElement (&id_element, 421); 
	pm = AddMultisetElement (&id_element, 422); 
	pm = AddMultisetElement (&id_element, 423); 
	pm = AddMultisetElement (&id_element, 424); 
	pm = AddMultisetElement (&id_element, 425); 
	pm = AddMultisetElement (&id_element, 426); 
	pm = AddMultisetElement (&id_element, 427); 
	pm = AddMultisetElement (&id_element, 428); 
	pm = AddMultisetElement (&id_element, 429); 
	pm = AddMultisetElement (&id_element, 430); 
	pm = AddMultisetElement (&id_element, 431); 
	pm = AddMultisetElement (&id_element, 432); 
	pm = AddMultisetElement (&id_element, 433); 
	pm = AddMultisetElement (&id_element, 434); 
	pm = AddMultisetElement (&id_element, 435); 
	pm = AddMultisetElement (&id_element, 436); 
	pm = AddMultisetElement (&id_element, 437); 
	pm = AddMultisetElement (&id_element, 438); 
	pm = AddMultisetElement (&id_element, 439); 
	pm = AddMultisetElement (&id_element, 440); 
	pm = AddMultisetElement (&id_element, 441); 
	pm = AddMultisetElement (&id_element, 442); 
	pm = AddMultisetElement (&id_element, 443); 
	pm = AddMultisetElement (&id_element, 444); 
	pm = AddMultisetElement (&id_element, 445); 
	pm = AddMultisetElement (&id_element, 446); 
	pm = AddMultisetElement (&id_element, 447); 
	pm = AddMultisetElement (&id_element, 448); 
	pm = AddMultisetElement (&id_element, 449); 
	pm = AddMultisetElement (&id_element, 450); 
	pm = AddMultisetElement (&id_element, 451); 
	pm = AddMultisetElement (&id_element, 452); 
	pm = AddMultisetElement (&id_element, 453); 
	pm = AddMultisetElement (&id_element, 454); 
	pm = AddMultisetElement (&id_element, 455); 
	pm = AddMultisetElement (&id_element, 456); 
	pm = AddMultisetElement (&id_element, 457); 
	pm = AddMultisetElement (&id_element, 458); 
	pm = AddMultisetElement (&id_element, 459); 
	pm = AddMultisetElement (&id_element, 460); 
	pm = AddMultisetElement (&id_element, 461); 
	pm = AddMultisetElement (&id_element, 462); 
	pm = AddMultisetElement (&id_element, 463); 
	pm = AddMultisetElement (&id_element, 464); 
	pm = AddMultisetElement (&id_element, 465); 
	pm = AddMultisetElement (&id_element, 466); 
	pm = AddMultisetElement (&id_element, 467); 
	pm = AddMultisetElement (&id_element, 468); 
	pm = AddMultisetElement (&id_element, 469); 
	pm = AddMultisetElement (&id_element, 470); 
	pm = AddMultisetElement (&id_element, 471); 
	pm = AddMultisetElement (&id_element, 472); 
	pm = AddMultisetElement (&id_element, 473); 
	pm = AddMultisetElement (&id_element, 474); 
	pm = AddMultisetElement (&id_element, 475); 
	pm = AddMultisetElement (&id_element, 476); 
	pm = AddMultisetElement (&id_element, 477); 
	pm = AddMultisetElement (&id_element, 478); 
	pm = AddMultisetElement (&id_element, 479); 
	pm = AddMultisetElement (&id_element, 480); 
	pm = AddMultisetElement (&id_element, 481); 
	pm = AddMultisetElement (&id_element, 482); 
	pm = AddMultisetElement (&id_element, 483); 
	pm = AddMultisetElement (&id_element, 484); 
	pm = AddMultisetElement (&id_element, 485); 
	pm = AddMultisetElement (&id_element, 486); 
	pm = AddMultisetElement (&id_element, 487); 
	pm = AddMultisetElement (&id_element, 488); 
	pm = AddMultisetElement (&id_element, 489); 
	pm = AddMultisetElement (&id_element, 490); 
	pm = AddMultisetElement (&id_element, 491); 
	pm = AddMultisetElement (&id_element, 492); 
	pm = AddMultisetElement (&id_element, 493); 
	pm = AddMultisetElement (&id_element, 494); 
	pm = AddMultisetElement (&id_element, 495); 
	pm = AddMultisetElement (&id_element, 496); 
	pm = AddMultisetElement (&id_element, 497); 
	pm = AddMultisetElement (&id_element, 498); 
	pm = AddMultisetElement (&id_element, 499); 
	pm = AddMultisetElement (&id_element, 500); 
	pm = AddMultisetElement (&id_element, 501); 
	pm = AddMultisetElement (&id_element, 502); 
	pm = AddMultisetElement (&id_element, 503); 
	pm = AddMultisetElement (&id_element, 504); 
	pm = AddMultisetElement (&id_element, 505); 
	pm = AddMultisetElement (&id_element, 506); 
	pm = AddMultisetElement (&id_element, 507); 
	pm = AddMultisetElement (&id_element, 508); 
	pm = AddMultisetElement (&id_element, 509); 
	pm = AddMultisetElement (&id_element, 510); 
	pm = AddMultisetElement (&id_element, 511); 
	pm = AddMultisetElement (&id_element, 512); 
	pm = AddMultisetElement (&id_element, 513); 
	pm = AddMultisetElement (&id_element, 514); 
	pm = AddMultisetElement (&id_element, 515); 
	pm = AddMultisetElement (&id_element, 516); 
	pm = AddMultisetElement (&id_element, 517); 
	pm = AddMultisetElement (&id_element, 518); 
	pm = AddMultisetElement (&id_element, 519); 
	pm = AddMultisetElement (&id_element, 520); 
	pm = AddMultisetElement (&id_element, 521); 
	pm = AddMultisetElement (&id_element, 522); 
	pm = AddMultisetElement (&id_element, 523); 
	pm = AddMultisetElement (&id_element, 524); 
	pm = AddMultisetElement (&id_element, 525); 
	pm = AddMultisetElement (&id_element, 526); 
	pm = AddMultisetElement (&id_element, 527); 
	pm = AddMultisetElement (&id_element, 528); 
	pm = AddMultisetElement (&id_element, 529); 
	pm = AddMultisetElement (&id_element, 530); 
	pm = AddMultisetElement (&id_element, 531); 
	pm = AddMultisetElement (&id_element, 532); 
	pm = AddMultisetElement (&id_element, 533); 
	pm = AddMultisetElement (&id_element, 534); 
	pm = AddMultisetElement (&id_element, 535); 
	pm = AddMultisetElement (&id_element, 536); 
	pm = AddMultisetElement (&id_element, 537); 
	pm = AddMultisetElement (&id_element, 538); 
	pm = AddMultisetElement (&id_element, 539); 
	pm = AddMultisetElement (&id_element, 540); 
	pm = AddMultisetElement (&id_element, 541); 
	pm = AddMultisetElement (&id_element, 542); 
	pm = AddMultisetElement (&id_element, 543); 
	pm = AddMultisetElement (&id_element, 544); 
	pm = AddMultisetElement (&id_element, 545); 
	pm = AddMultisetElement (&id_element, 546); 
	pm = AddMultisetElement (&id_element, 547); 
	pm = AddMultisetElement (&id_element, 548); 
	pm = AddMultisetElement (&id_element, 549); 
	pm = AddMultisetElement (&id_element, 550); 
	pm = AddMultisetElement (&id_element, 551); 
	pm = AddMultisetElement (&id_element, 552); 
	pm = AddMultisetElement (&id_element, 553); 
	pm = AddMultisetElement (&id_element, 554); 
	pm = AddMultisetElement (&id_element, 555); 
	pm = AddMultisetElement (&id_element, 556); 
	pm = AddMultisetElement (&id_element, 557); 
	pm = AddMultisetElement (&id_element, 558); 
	pm = AddMultisetElement (&id_element, 559); 
	pm = AddMultisetElement (&id_element, 560); 
	pm = AddMultisetElement (&id_element, 561); 
	pm = AddMultisetElement (&id_element, 562); 
	pm = AddMultisetElement (&id_element, 563); 
	pm = AddMultisetElement (&id_element, 564); 
	pm = AddMultisetElement (&id_element, 565); 
	pm = AddMultisetElement (&id_element, 566); 
	pm = AddMultisetElement (&id_element, 567); 
	pm = AddMultisetElement (&id_element, 568); 
	pm = AddMultisetElement (&id_element, 569); 
	pm = AddMultisetElement (&id_element, 570); 
	pm = AddMultisetElement (&id_element, 571); 
	pm = AddMultisetElement (&id_element, 572); 
	pm = AddMultisetElement (&id_element, 573); 
	pm = AddMultisetElement (&id_element, 574); 
	pm = AddMultisetElement (&id_element, 575); 
	pm = AddMultisetElement (&id_element, 576); 
	pm = AddMultisetElement (&id_element, 577); 
	pm = AddMultisetElement (&id_element, 578); 
	pm = AddMultisetElement (&id_element, 579); 
	pm = AddMultisetElement (&id_element, 580); 
	pm = AddMultisetElement (&id_element, 581); 
	pm = AddMultisetElement (&id_element, 582); 
	pm = AddMultisetElement (&id_element, 583); 
	pm = AddMultisetElement (&id_element, 584); 
	pm = AddMultisetElement (&id_element, 585); 
	pm = AddMultisetElement (&id_element, 586); 
	pm = AddMultisetElement (&id_element, 587); 
	pm = AddMultisetElement (&id_element, 588); 
	pm = AddMultisetElement (&id_element, 589); 
	pm = AddMultisetElement (&id_element, 590); 
	pm = AddMultisetElement (&id_element, 591); 
	pm = AddMultisetElement (&id_element, 592); 
	pm = AddMultisetElement (&id_element, 593); 
	pm = AddMultisetElement (&id_element, 594); 
	pm = AddMultisetElement (&id_element, 595); 
	pm = AddMultisetElement (&id_element, 596); 
	pm = AddMultisetElement (&id_element, 597); 
	pm = AddMultisetElement (&id_element, 598); 
	pm = AddMultisetElement (&id_element, 599); 
	pm = AddMultisetElement (&id_element, 600); 
	pm = AddMultisetElement (&id_element, 601); 
	pm = AddMultisetElement (&id_element, 602); 
	pm = AddMultisetElement (&id_element, 603); 
	pm = AddMultisetElement (&id_element, 604); 
	pm = AddMultisetElement (&id_element, 605); 
	pm = AddMultisetElement (&id_element, 606); 
	pm = AddMultisetElement (&id_element, 607); 
	pm = AddMultisetElement (&id_element, 608); 
	pm = AddMultisetElement (&id_element, 609); 
	pm = AddMultisetElement (&id_element, 610); 
	pm = AddMultisetElement (&id_element, 611); 
	pm = AddMultisetElement (&id_element, 612); 
	pm = AddMultisetElement (&id_element, 613); 
	pm = AddMultisetElement (&id_element, 614); 
	pm = AddMultisetElement (&id_element, 615); 
	pm = AddMultisetElement (&id_element, 616); 
	pm = AddMultisetElement (&id_element, 617); 
	pm = AddMultisetElement (&id_element, 618); 
	pm = AddMultisetElement (&id_element, 619); 
	pm = AddMultisetElement (&id_element, 620); 
	pm = AddMultisetElement (&id_element, 621); 
	pm = AddMultisetElement (&id_element, 622); 
	pm = AddMultisetElement (&id_element, 623); 
	pm = AddMultisetElement (&id_element, 624); 
	pm = AddMultisetElement (&id_element, 625); 
	pm = AddMultisetElement (&id_element, 626); 
	pm = AddMultisetElement (&id_element, 627); 
	pm = AddMultisetElement (&id_element, 628); 
	pm = AddMultisetElement (&id_element, 629); 
	pm = AddMultisetElement (&id_element, 630); 
	pm = AddMultisetElement (&id_element, 631); 
	pm = AddMultisetElement (&id_element, 632); 
	pm = AddMultisetElement (&id_element, 633); 
	pm = AddMultisetElement (&id_element, 634); 
	pm = AddMultisetElement (&id_element, 635); 
	pm = AddMultisetElement (&id_element, 636); 
	pm = AddMultisetElement (&id_element, 637); 
	pm = AddMultisetElement (&id_element, 638); 
	pm = AddMultisetElement (&id_element, 639); 
	pm = AddMultisetElement (&id_element, 640); 
	pm = AddMultisetElement (&id_element, 641); 
	pm = AddMultisetElement (&id_element, 642); 
	pm = AddMultisetElement (&id_element, 643); 
	pm = AddMultisetElement (&id_element, 644); 
	pm = AddMultisetElement (&id_element, 645); 
	pm = AddMultisetElement (&id_element, 646); 
	pm = AddMultisetElement (&id_element, 647); 
	pm = AddMultisetElement (&id_element, 648); 
	pm = AddMultisetElement (&id_element, 649); 
	pm = AddMultisetElement (&id_element, 650); 
	pm = AddMultisetElement (&id_element, 651); 
	pm = AddMultisetElement (&id_element, 652); 
	pm = AddMultisetElement (&id_element, 653); 
	pm = AddMultisetElement (&id_element, 654); 
	pm = AddMultisetElement (&id_element, 655); 
	pm = AddMultisetElement (&id_element, 656); 
	pm = AddMultisetElement (&id_element, 657); 
	pm = AddMultisetElement (&id_element, 658); 
	pm = AddMultisetElement (&id_element, 659); 
	pm = AddMultisetElement (&id_element, 660); 
	pm = AddMultisetElement (&id_element, 661); 
	pm = AddMultisetElement (&id_element, 662); 
	pm = AddMultisetElement (&id_element, 663); 
	pm = AddMultisetElement (&id_element, 664); 
	pm = AddMultisetElement (&id_element, 665); 
	pm = AddMultisetElement (&id_element, 666); 
	pm = AddMultisetElement (&id_element, 667); 
	pm = AddMultisetElement (&id_element, 668); 
	pm = AddMultisetElement (&id_element, 669); 
	pm = AddMultisetElement (&id_element, 670); 
	pm = AddMultisetElement (&id_element, 671); 
	pm = AddMultisetElement (&id_element, 672); 
	pm = AddMultisetElement (&id_element, 673); 
	pm = AddMultisetElement (&id_element, 674); 
	pm = AddMultisetElement (&id_element, 675); 
	pm = AddMultisetElement (&id_element, 676); 
	pm = AddMultisetElement (&id_element, 677); 
	pm = AddMultisetElement (&id_element, 678); 
	pm = AddMultisetElement (&id_element, 679); 
	pm = AddMultisetElement (&id_element, 680); 
	pm = AddMultisetElement (&id_element, 681); 
	pm = AddMultisetElement (&id_element, 682); 
	pm = AddMultisetElement (&id_element, 683); 
	pm = AddMultisetElement (&id_element, 684); 
	pm = AddMultisetElement (&id_element, 685); 
	pm = AddMultisetElement (&id_element, 686); 
	pm = AddMultisetElement (&id_element, 687); 
	pm = AddMultisetElement (&id_element, 688); 
	pm = AddMultisetElement (&id_element, 689); 
	pm = AddMultisetElement (&id_element, 690); 
	pm = AddMultisetElement (&id_element, 691); 
	pm = AddMultisetElement (&id_element, 692); 
	pm = AddMultisetElement (&id_element, 693); 
	pm = AddMultisetElement (&id_element, 694); 
	pm = AddMultisetElement (&id_element, 695); 
	pm = AddMultisetElement (&id_element, 696); 
	pm = AddMultisetElement (&id_element, 697); 
	pm = AddMultisetElement (&id_element, 698); 
	pm = AddMultisetElement (&id_element, 699); 
	pm = AddMultisetElement (&id_element, 700); 
	pm = AddMultisetElement (&id_element, 701); 
	pm = AddMultisetElement (&id_element, 702); 
	pm = AddMultisetElement (&id_element, 703); 
	pm = AddMultisetElement (&id_element, 704); 
	pm = AddMultisetElement (&id_element, 705); 
	pm = AddMultisetElement (&id_element, 706); 
	pm = AddMultisetElement (&id_element, 707); 
	pm = AddMultisetElement (&id_element, 708); 
	pm = AddMultisetElement (&id_element, 709); 
	pm = AddMultisetElement (&id_element, 710); 
	pm = AddMultisetElement (&id_element, 711); 
	pm = AddMultisetElement (&id_element, 712); 
	pm = AddMultisetElement (&id_element, 713); 
	pm = AddMultisetElement (&id_element, 714); 
	pm = AddMultisetElement (&id_element, 715); 
	pm = AddMultisetElement (&id_element, 716); 
	pm = AddMultisetElement (&id_element, 717); 
	pm = AddMultisetElement (&id_element, 718); 
	pm = AddMultisetElement (&id_element, 719); 
	pm = AddMultisetElement (&id_element, 720); 
	pm = AddMultisetElement (&id_element, 721); 
	pm = AddMultisetElement (&id_element, 722); 
	pm = AddMultisetElement (&id_element, 723); 
	pm = AddMultisetElement (&id_element, 724); 
	pm = AddMultisetElement (&id_element, 725); 
	pm = AddMultisetElement (&id_element, 726); 
	pm = AddMultisetElement (&id_element, 727); 
	pm = AddMultisetElement (&id_element, 728); 
	pm = AddMultisetElement (&id_element, 729); 
	pm = AddMultisetElement (&id_element, 730); 
	pm = AddMultisetElement (&id_element, 731); 
	pm = AddMultisetElement (&id_element, 732); 
	pm = AddMultisetElement (&id_element, 733); 
	pm = AddMultisetElement (&id_element, 734); 
	pm = AddMultisetElement (&id_element, 735); 
	pm = AddMultisetElement (&id_element, 736); 
	pm = AddMultisetElement (&id_element, 737); 
	pm = AddMultisetElement (&id_element, 738); 
	pm = AddMultisetElement (&id_element, 739); 
	pm = AddMultisetElement (&id_element, 740); 
	pm = AddMultisetElement (&id_element, 741); 
	pm = AddMultisetElement (&id_element, 742); 
	pm = AddMultisetElement (&id_element, 743); 
	pm = AddMultisetElement (&id_element, 744); 
	pm = AddMultisetElement (&id_element, 745); 
	pm = AddMultisetElement (&id_element, 746); 
	pm = AddMultisetElement (&id_element, 747); 
	pm = AddMultisetElement (&id_element, 748); 
	pm = AddMultisetElement (&id_element, 749); 
	pm = AddMultisetElement (&id_element, 750); 
	pm = AddMultisetElement (&id_element, 751); 
	pm = AddMultisetElement (&id_element, 752); 
	pm = AddMultisetElement (&id_element, 753); 
	pm = AddMultisetElement (&id_element, 754); 
	pm = AddMultisetElement (&id_element, 755); 
	pm = AddMultisetElement (&id_element, 756); 
	pm = AddMultisetElement (&id_element, 757); 
	pm = AddMultisetElement (&id_element, 758); 
	pm = AddMultisetElement (&id_element, 759); 
	pm = AddMultisetElement (&id_element, 760); 
	pm = AddMultisetElement (&id_element, 761); 
	pm = AddMultisetElement (&id_element, 762); 
	pm = AddMultisetElement (&id_element, 763); 
	pm = AddMultisetElement (&id_element, 764); 
	pm = AddMultisetElement (&id_element, 765); 
	pm = AddMultisetElement (&id_element, 766); 
	pm = AddMultisetElement (&id_element, 767); 
	pm = AddMultisetElement (&id_element, 768); 
	pm = AddMultisetElement (&id_element, 769); 
	pm = AddMultisetElement (&id_element, 770); 
	pm = AddMultisetElement (&id_element, 771); 
	pm = AddMultisetElement (&id_element, 772); 
	pm = AddMultisetElement (&id_element, 773); 
	pm = AddMultisetElement (&id_element, 774); 
	pm = AddMultisetElement (&id_element, 775); 
	pm = AddMultisetElement (&id_element, 776); 
	pm = AddMultisetElement (&id_element, 777); 
	pm = AddMultisetElement (&id_element, 778); 
	pm = AddMultisetElement (&id_element, 779); 
	pm = AddMultisetElement (&id_element, 780); 
	pm = AddMultisetElement (&id_element, 781); 
	pm = AddMultisetElement (&id_element, 782); 
	pm = AddMultisetElement (&id_element, 783); 
	pm = AddMultisetElement (&id_element, 784); 
	pm = AddMultisetElement (&id_element, 785); 
	pm = AddMultisetElement (&id_element, 786); 
	pm = AddMultisetElement (&id_element, 787); 
	pm = AddMultisetElement (&id_element, 788); 
	pm = AddMultisetElement (&id_element, 789); 
	pm = AddMultisetElement (&id_element, 790); 
	pm = AddMultisetElement (&id_element, 791); 
	pm = AddMultisetElement (&id_element, 792); 
	pm = AddMultisetElement (&id_element, 793); 
	pm = AddMultisetElement (&id_element, 794); 
	pm = AddMultisetElement (&id_element, 795); 
	pm = AddMultisetElement (&id_element, 796); 
	pm = AddMultisetElement (&id_element, 797); 
	pm = AddMultisetElement (&id_element, 798); 
	pm = AddMultisetElement (&id_element, 799); 
	pm = AddMultisetElement (&id_element, 800); 
	pm = AddMultisetElement (&id_element, 801); 
	pm = AddMultisetElement (&id_element, 802); 
	pm = AddMultisetElement (&id_element, 803); 
	pm = AddMultisetElement (&id_element, 804); 
	pm = AddMultisetElement (&id_element, 805); 
	pm = AddMultisetElement (&id_element, 806); 
	pm = AddMultisetElement (&id_element, 807); 
	pm = AddMultisetElement (&id_element, 808); 
	pm = AddMultisetElement (&id_element, 809); 
	pm = AddMultisetElement (&id_element, 810); 
	pm = AddMultisetElement (&id_element, 811); 
	pm = AddMultisetElement (&id_element, 812); 
	pm = AddMultisetElement (&id_element, 813); 
	pm = AddMultisetElement (&id_element, 814); 
	pm = AddMultisetElement (&id_element, 815); 
	pm = AddMultisetElement (&id_element, 816); 
	pm = AddMultisetElement (&id_element, 817); 
	pm = AddMultisetElement (&id_element, 818); 
	pm = AddMultisetElement (&id_element, 819); 
	pm = AddMultisetElement (&id_element, 820); 
	pm = AddMultisetElement (&id_element, 821); 
	pm = AddMultisetElement (&id_element, 822); 
	pm = AddMultisetElement (&id_element, 823); 
	pm = AddMultisetElement (&id_element, 824); 
	pm = AddMultisetElement (&id_element, 825); 
	pm = AddMultisetElement (&id_element, 826); 
	pm = AddMultisetElement (&id_element, 827); 
	pm = AddMultisetElement (&id_element, 828); 
	pm = AddMultisetElement (&id_element, 829); 
	pm = AddMultisetElement (&id_element, 830); 
	pm = AddMultisetElement (&id_element, 831); 
	pm = AddMultisetElement (&id_element, 832); 
	pm = AddMultisetElement (&id_element, 833); 
	pm = AddMultisetElement (&id_element, 834); 
	pm = AddMultisetElement (&id_element, 835); 
	pm = AddMultisetElement (&id_element, 836); 
	pm = AddMultisetElement (&id_element, 837); 
	pm = AddMultisetElement (&id_element, 838); 
	pm = AddMultisetElement (&id_element, 839); 
	pm = AddMultisetElement (&id_element, 840); 
	pm = AddMultisetElement (&id_element, 841); 
	pm = AddMultisetElement (&id_element, 842); 
	pm = AddMultisetElement (&id_element, 843); 
	pm = AddMultisetElement (&id_element, 844); 
	pm = AddMultisetElement (&id_element, 845); 
	pm = AddMultisetElement (&id_element, 846); 
	pm = AddMultisetElement (&id_element, 847); 
	pm = AddMultisetElement (&id_element, 848); 
	pm = AddMultisetElement (&id_element, 849); 
	pm = AddMultisetElement (&id_element, 850); 
	pm = AddMultisetElement (&id_element, 851); 
	pm = AddMultisetElement (&id_element, 852); 
	pm = AddMultisetElement (&id_element, 853); 
	pm = AddMultisetElement (&id_element, 854); 
	pm = AddMultisetElement (&id_element, 855); 
	pm = AddMultisetElement (&id_element, 856); 
	pm = AddMultisetElement (&id_element, 857); 
	pm = AddMultisetElement (&id_element, 858); 
	pm = AddMultisetElement (&id_element, 859); 
	pm = AddMultisetElement (&id_element, 860); 
	pm = AddMultisetElement (&id_element, 861); 
	pm = AddMultisetElement (&id_element, 862); 
	pm = AddMultisetElement (&id_element, 863); 
	pm = AddMultisetElement (&id_element, 864); 
	pm = AddMultisetElement (&id_element, 865); 
	pm = AddMultisetElement (&id_element, 866); 
	pm = AddMultisetElement (&id_element, 867); 
	pm = AddMultisetElement (&id_element, 868); 
	pm = AddMultisetElement (&id_element, 869); 
	pm = AddMultisetElement (&id_element, 870); 
	pm = AddMultisetElement (&id_element, 871); 
	pm = AddMultisetElement (&id_element, 872); 
	pm = AddMultisetElement (&id_element, 873); 
	pm = AddMultisetElement (&id_element, 874); 
	pm = AddMultisetElement (&id_element, 875); 
	pm = AddMultisetElement (&id_element, 876); 
	pm = AddMultisetElement (&id_element, 877); 
	pm = AddMultisetElement (&id_element, 878); 
	pm = AddMultisetElement (&id_element, 879); 
	pm = AddMultisetElement (&id_element, 880); 
	pm = AddMultisetElement (&id_element, 881); 
	pm = AddMultisetElement (&id_element, 882); 
	pm = AddMultisetElement (&id_element, 883); 
	pm = AddMultisetElement (&id_element, 884); 
	pm = AddMultisetElement (&id_element, 885); 
	pm = AddMultisetElement (&id_element, 886); 
	pm = AddMultisetElement (&id_element, 887); 
	pm = AddMultisetElement (&id_element, 888); 
	pm = AddMultisetElement (&id_element, 889); 
	pm = AddMultisetElement (&id_element, 890); 
	pm = AddMultisetElement (&id_element, 891); 
	pm = AddMultisetElement (&id_element, 892); 
	pm = AddMultisetElement (&id_element, 893); 
	pm = AddMultisetElement (&id_element, 894); 
	pm = AddMultisetElement (&id_element, 895); 
	pm = AddMultisetElement (&id_element, 896); 
	pm = AddMultisetElement (&id_element, 897); 
	pm = AddMultisetElement (&id_element, 898); 
	pm = AddMultisetElement (&id_element, 899); 
	pm = AddMultisetElement (&id_element, 900); 
	pm = AddMultisetElement (&id_element, 901); 
	pm = AddMultisetElement (&id_element, 902); 
	pm = AddMultisetElement (&id_element, 903); 
	pm = AddMultisetElement (&id_element, 904); 
	pm = AddMultisetElement (&id_element, 905); 
	pm = AddMultisetElement (&id_element, 906); 
	pm = AddMultisetElement (&id_element, 907); 
	pm = AddMultisetElement (&id_element, 908); 
	pm = AddMultisetElement (&id_element, 909); 
	pm = AddMultisetElement (&id_element, 910); 
	pm = AddMultisetElement (&id_element, 911); 
	pm = AddMultisetElement (&id_element, 912); 
	pm = AddMultisetElement (&id_element, 913); 
	pm = AddMultisetElement (&id_element, 914); 
	pm = AddMultisetElement (&id_element, 915); 
	pm = AddMultisetElement (&id_element, 916); 
	pm = AddMultisetElement (&id_element, 917); 
	pm = AddMultisetElement (&id_element, 918); 
	pm = AddMultisetElement (&id_element, 919); 
	pm = AddMultisetElement (&id_element, 920); 
	pm = AddMultisetElement (&id_element, 921); 
	pm = AddMultisetElement (&id_element, 922); 
	pm = AddMultisetElement (&id_element, 923); 
	pm = AddMultisetElement (&id_element, 924); 
	pm = AddMultisetElement (&id_element, 925); 
	pm = AddMultisetElement (&id_element, 926); 
	pm = AddMultisetElement (&id_element, 927); 
	pm = AddMultisetElement (&id_element, 928); 
	pm = AddMultisetElement (&id_element, 929); 
	pm = AddMultisetElement (&id_element, 930); 
	pm = AddMultisetElement (&id_element, 931); 
	pm = AddMultisetElement (&id_element, 932); 
	pm = AddMultisetElement (&id_element, 933); 
	pm = AddMultisetElement (&id_element, 934); 
	pm = AddMultisetElement (&id_element, 935); 
	pm = AddMultisetElement (&id_element, 936); 
	pm = AddMultisetElement (&id_element, 937); 
	pm = AddMultisetElement (&id_element, 938); 
	pm = AddMultisetElement (&id_element, 939); 
	pm = AddMultisetElement (&id_element, 940); 
	pm = AddMultisetElement (&id_element, 941); 
	pm = AddMultisetElement (&id_element, 942); 
	pm = AddMultisetElement (&id_element, 943); 
	pm = AddMultisetElement (&id_element, 944); 
	pm = AddMultisetElement (&id_element, 945); 
	pm = AddMultisetElement (&id_element, 946); 
	pm = AddMultisetElement (&id_element, 947); 
	pm = AddMultisetElement (&id_element, 948); 
	pm = AddMultisetElement (&id_element, 949); 
	pm = AddMultisetElement (&id_element, 950); 
	pm = AddMultisetElement (&id_element, 951); 
	pm = AddMultisetElement (&id_element, 952); 
	pm = AddMultisetElement (&id_element, 953); 
	pm = AddMultisetElement (&id_element, 954); 
	pm = AddMultisetElement (&id_element, 955); 
	pm = AddMultisetElement (&id_element, 956); 
	pm = AddMultisetElement (&id_element, 957); 
	pm = AddMultisetElement (&id_element, 958); 
	pm = AddMultisetElement (&id_element, 959); 
	pm = AddMultisetElement (&id_element, 960); 
	pm = AddMultisetElement (&id_element, 961); 
	pm = AddMultisetElement (&id_element, 962); 
	pm = AddMultisetElement (&id_element, 963); 
	pm = AddMultisetElement (&id_element, 964); 
	pm = AddMultisetElement (&id_element, 965); 
	pm = AddMultisetElement (&id_element, 966); 
	pm = AddMultisetElement (&id_element, 967); 
	pm = AddMultisetElement (&id_element, 968); 
	pm = AddMultisetElement (&id_element, 969); 
	pm = AddMultisetElement (&id_element, 970); 
	pm = AddMultisetElement (&id_element, 971); 
	pm = AddMultisetElement (&id_element, 972); 
	pm = AddMultisetElement (&id_element, 973); 
	pm = AddMultisetElement (&id_element, 974); 
	pm = AddMultisetElement (&id_element, 975); 
	pm = AddMultisetElement (&id_element, 976); 
	pm = AddMultisetElement (&id_element, 977); 
	pm = AddMultisetElement (&id_element, 978); 
	pm = AddMultisetElement (&id_element, 979); 
	pm = AddMultisetElement (&id_element, 980); 
	pm = AddMultisetElement (&id_element, 981); 
	pm = AddMultisetElement (&id_element, 982); 
	pm = AddMultisetElement (&id_element, 983); 
	pm = AddMultisetElement (&id_element, 984); 
	pm = AddMultisetElement (&id_element, 985); 
	pm = AddMultisetElement (&id_element, 986); 
	pm = AddMultisetElement (&id_element, 987); 
	pm = AddMultisetElement (&id_element, 988); 
	pm = AddMultisetElement (&id_element, 989); 
	pm = AddMultisetElement (&id_element, 990); 
	pm = AddMultisetElement (&id_element, 991); 
	pm = AddMultisetElement (&id_element, 992); 
	pm = AddMultisetElement (&id_element, 993); 
	pm = AddMultisetElement (&id_element, 994); 
	pm = AddMultisetElement (&id_element, 995); 
	pm = AddMultisetElement (&id_element, 996); 
	pm = AddMultisetElement (&id_element, 997); 
	pm = AddMultisetElement (&id_element, 998); 
	pm = AddMultisetElement (&id_element, 999); 
	pm = AddMultisetElement (&id_element, 1000); 

 
	/************ INSTANCES GENERATION *************/
	GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);

 
	/****************** MAIN LOOP ******************/
	while ((instances_I1 != NULL)) { 
 
		/*************** GRAPH GENERATION **************/
		//Creating Initial Graph (from I1 Instances List) 
		pix = instances_I1; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 1); 
			CreateOutputEdges (data, 1); 
			element_I1.data = pix->data; 
			element_I1.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I1, element_I1); 
			pix = pix->next; 
		} 
 
 
		/************ SINKS IDENTIFICATION *************/ 
		IdentifySinks(); 
 
 
		/*********** THREADS INITIALIZATION ************/ 
		while(vertices != NULL){ 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_create(&threads[ps->id], NULL, ThreadFunction, (void *)(&(*ps))); 
				ps = ps->next; 
			} 
			// Finishing the threads 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_join(threads[ps->id], NULL); 
				ps = ps->next; 
			} 
 
			// Delete the sink list before starting another sinks identification... 
			DeleteAllSinks(); 
 
			// Identify new sinks to start another iteration... 
			IdentifySinks(); 
		} 
 
 
		/********** NEW INSTANCES GENERATION ***********/
		DeleteAllInstanceIx (&instances_I1);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);

 
		//Allowing the opportunity to generate again some instances...
		if((instances_I1==NULL) && (!allow_1)){
			allow_1 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I1);
			GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
		}
 
	}		// End of the Main Loop 
 
 
	// Deleting the history of instances provided to both reactions... 
	DeleteAllInstanceIx (&usedinstances_I1); 

 
	printf("\n \n"); 
	printf("######################## \n"); 
	printf("END OF THE PROGRAM! \n \n"); 
	printf("Final Multiset: \n \n"); 
	ListingMultiset(); 
	printf("\n"); 
	printf("######################## \n"); 
 
	exit (0); 
} 
 
 
/***************************************************************/ 
/*******  Function related to the R1 reaction execution ********/ 
/***************************************************************/ 
int R1Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: sum 
	int x, y, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 


    // Test - Matrix multiplication
	int line,row, i, aux;
    int dim = 100;
    int mat1[dim][dim], mat2[dim][dim], mat3[dim][dim];
    for(line=0; line<dim; line++)
        for(row=0; row<dim; row++){
            aux=0;
            for(i=0; i<dim; i++) {
                aux = aux + (mat1[line][i]*mat2[i][row]);
            }
            mat3[line][row]=aux;
        }
	// End Test Matrix Multiplication

 
	x  = ps->data.element1; 
	y  = ps->data.element2; 
	element_Ix.data.element1 = ps->data.element1; 
	element_Ix.data.element2 = ps->data.element2; 
	result = (x + y); 
 
	if (1) { 
		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I1, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 

/***************************************************************/ 
/*******  Function for the Threads fired by the Sinks **********/ 
/***************************************************************/ 
void * ThreadFunction (void *arg){ 
	reg_sink *ps; 
	reg_multiset *pm; 
	ps = (reg_sink *)(arg); 
	int reacted; 
 
	/************** REACTION EXECUTION *************/ 
	if (ps->reaction == 1){   // Try to execute the reaction 1 ... 
		reacted = R1Function(ps); 
	} 

	sem_wait(&sem_graph); 
 
	EdgesReversalSink(ps); 
 
	/********* VERTICES AFFECTED REMOVAL ***********/ 
	/******************** AND **********************/ 
	/**************** SINK REMOVAL *****************/ 
 
	if (reacted){ 
		DeleteVerticesAffected(ps);     // all vertices affected, included its own vertex 
	} 
	else { 
		DeleteVertex(ps->data, ps->reaction);   // Delete the vertex that execute as a Sink 
	} 
 
	sem_post(&sem_graph);  
 
}  
 
// Tests Rui - Definitions 
// Program->p_defs->d_definition->d_name: sum 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: x 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: y 

