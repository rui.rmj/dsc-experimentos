
/*
 ****************************************************************
 *                                                              *
 *      Module: code.c                                          *
 *      Description: Code Generation                            *
 *      Version: 1.0 of 14/Jun/95                               *
 *      Author: Simon Gay and Juarez Muylaert                   *
 *                                                              *
 ****************************************************************
 */

/*
 ****************************************************************
 *																*
 *		Modifyed: 30/Dec/2021									*
 *		Author: Rui Rodrigues de Mello Junior					*
 * 																*
 *		Add functions to generate NSGB Runtime					*
 *		NSGB - New Scheduler Graph-Based						*
 *																*	
 ****************************************************************
 */


# include <stdio.h>
# include <string.h>
# include "include/const.h"
# include "include/types.h"
# include "include/rtbag.h"
# include "include/fnc.h"
# include "include/vars.h"

extern char expression[100];
/*
 ****************************************************************
 * Write Header with Included Files                             *
 ****************************************************************
 */

void
WriteHeader ()
{
	Output = stdout;
	fprintf (Output, "# include <stdio.h>\n");
	fprintf (Output, "# include <stdlib.h>\n");
	fprintf (Output, "# include <pthread.h>\n");
	fprintf (Output, "# include <semaphore.h>\n");
	fprintf (Output, "# include <sys/types.h>\n");
	fprintf (Output, "# include <sys/time.h>\n");
	fprintf (Output, "# include \"../include/const.h\"\n");
	fprintf (Output, "# include \"../include/types.h\"\n");
	fprintf (Output, "# include \"../include/rtbag.h\"\n");
	fprintf (Output, "# include \"../include/fnc.h\"\n");
	fprintf (Output, "# include \"../include/vars.h\"\n\n\n");

}	/* end WriteHeader */


/*
 ****************************************************************
 * Generate Code for the Initial Bag                            *
 ****************************************************************
 */

void
CodeInitialBag (pb)
BAG *pb;
{
	register BAG *paux = pb;

	fprintf (Output, "void\nCreateInitialBag ()\n{\n");
	while (paux != (BAG *) NULL)
	{
	    if (paux->b_tag == T_TUPLE)
		GenTuple (paux);
	    else 
		GenBasicExpression (paux);
            paux = paux->b_next;
        }
        fprintf (Output, "\n}   /* end CreatInitialBagNode */\n");

}       /* end CodeInitialBag */


/*
 ****************************************************************
 * Generate Code for An Expression in the Initial Bag           *
 ****************************************************************
 */

void
GenBasicExpression (paux)
BAG *paux;
{
	if (paux->b_val.b_expression->e_tag == T_NUMERAL)
	    fprintf (Output, "\tRunTimeBag = AddRTBagNode(TY_INT, %d, RunTimeBag);\n",
	                               paux->b_val.b_expression->e_val.e_numval);
	else    /* let's treat boolean values as integers */
	    fprintf (Output, "\tRunTimeBag = AddRTBagNode(TY_INT, %d, RunTimeBag);\n",
                              paux->b_val.b_expression->e_val.e_boolval);
	return;	

}	/* end GenBasicExpression */


/*
 ****************************************************************
 * Get the size of a tuple                                      *
 ****************************************************************
 */

int
GetTupleSize (pt)
TUPLE *pt;
{
	register int size = 0;
	register TUPLE *paux;

	for (paux = pt; paux != (TUPLE *) NULL; paux = paux->t_next)
	    size++;
	return (size);

}	/* end GetTupleSize */

/*
 ****************************************************************
 * Generate Code for Tuple in the Initial Bag                   *
 ****************************************************************
 */

void
GenTuple (pb)
BAG *pb;
{
	register TUPLE *pt;	
	register int size;
	register int tag;

	fprintf (Output, "\t{\n\t    register RT_TUPLE *pt;\n");
	fprintf (Output, "\t    register RT_LIST *pl;\n\n");
	size = GetTupleSize (pb->b_val.b_tuple);
	fprintf (Output, "\t    pt = GenTupleSkeleton (%d);\n", size);
	fprintf (Output, "\t    pl = pt->rtt_list;\n");
	for (pt = pb->b_val.b_tuple; pt != (TUPLE *) NULL; pt = pt->t_next)
	{
	    if ((tag = pt->t_expression->e_tag) == T_NUMERAL)
	    {
		fprintf (Output, "\t    pl->rtl_tag = TY_INT;\n");
		fprintf (Output, "\t    pl->rtl_element = (RT_ELEMENT *) Alloc (sizeof (RT_ELEMENT));\n");
		fprintf (Output, "\t    pl->rtl_element->rte_val = %d;\n", pt->t_expression->e_val.e_numval);
	    } else if (tag == T_BOOLEAN) {  /* we treat booleans as integers */
                fprintf (Output, "\t    pl->rtl_tag = TY_INT;\n");
                fprintf (Output, "\t    pl->rtl_element = (RT_ELEMENT *) Alloc (sizeof (RT_ELEMENT));\n");
                fprintf (Output, "\t    pl->rtl_element->rte_val = %d;\n", pt->t_expression->e_val.e_boolval);
	    } 
	    fprintf (Output, "\t    pl = pl->rtl_next;\n");
	}
	fprintf (Output, "\t    RunTimeBag = AddRTTupleBagNode (pt, RunTimeBag);\n\t}\n");
	return;	    

}	/* end GenTuple */


/*
 ****************************************************************
 * Generate Code for Expressions  via a Namelist                *
 ****************************************************************
 */

//void
//GenCodeExpression (pe, name,nl)
//EXPRESSION *pe;
//char *name;                   /* the name of the generated function */
//NAMELIST *nl;
//{
//	register EXPRESSION *paux = pe;
//
//	/*
//	 * Generate the Header of the Function.
//	 */
//	fprintf (Output, "%s (", name);
//	while (nl != (NAMELIST *) NULL)
//	{
//	    fprintf (Output, "%s", nl->n_name);
//	    if ((nl = nl->n_next) != (NAMELIST *) NULL)
//		fprintf (Output, ", ");
//	}
//	fprintf (Output, ")\n{\n");
//	fprintf (Output, "register int Result = 0;\n\n");
//	fprintf (Output, "Result = ");
//        CodePrintExpression (pe);
//	fprintf (Output, ";\nreturn (Result);\n");
//	fprintf (Output, "\n}\t/* end %s */\n\n", name);
//	
//}	/* end GenCodeExpression */


void
GenCodeExpression (pe, name,nl)
EXPRESSION *pe;
char *name;                   /* the name of the generated function */
NAMELIST *nl;
{
	register EXPRESSION *paux = pe;

	/*
	 * Generate the Header of the Function.
	 */
	fprintf (Output, "%s (", name);
	while (nl != (NAMELIST *) NULL)
	{
	    fprintf (Output, "%s", nl->n_name);
	    if ((nl = nl->n_next) != (NAMELIST *) NULL)
		fprintf (Output, ", ");
	}
	fprintf (Output, ")\n{\n");
	fprintf (Output, "register int Result = 0;\n\n");

	// Test Matrix Multiplication
	fprintf (Output, "// Test Matrix Multiplication\n");
	fprintf (Output, "int line,row, i, aux;\n");
	fprintf (Output, "int dim = 100;\n");
	fprintf (Output, "int mat1[dim][dim], mat2[dim][dim], mat3[dim][dim];\n");
	fprintf (Output, "for(line=0; line<dim; line++)\n");
	fprintf (Output, "\tfor(row=0; row<dim; row++){\n");
	fprintf (Output, "\t\taux=0;\n");
	fprintf (Output, "\t\tfor(i=0; i<dim; i++) {\n");
	fprintf (Output, "\t\t\taux = aux + (mat1[line][i]*mat2[i][row]);\n");
	fprintf (Output, "\t\t}\n");
	fprintf (Output, "\tmat3[line][row]=aux;\n");
	fprintf (Output, "}\n");
	fprintf (Output, "// End Test Matrix Multiplication\n");
	fprintf (Output, "\n");

	// Test Matrix Multiplication

	fprintf (Output, "Result = ");
        CodePrintExpression (pe);
	fprintf (Output, ";\nreturn (Result);\n");
	fprintf (Output, "\n}\t/* end %s */\n\n", name);
	
}	/* end GenCodeExpression */



/*
 ****************************************************************
 * Generate Code for Expressions  via a Pattern                 *
 ****************************************************************
 */

void
GenCodeExpressionWithPattern (pe, name, pp)
EXPRESSION *pe;
char *name;                   /* the name of the generated function */
PATTERN *pp;
{
        register PATTERN *paux;

        /*
         * Generate the Header of the Function.
         */
        fprintf (Output, "%s (", name);
	for (paux = pp; paux != (PATTERN *) NULL; paux = paux->p_next)
	{
	    if (paux->p_expression->e_tag == T_IDENTIFIER)	
                fprintf (Output, "%s", paux->p_expression->e_val.e_name);
	    else if (paux->p_expression->e_tag == T_TUPLE)
	    {
		register PATTERN *pt = paux->p_expression->e_val.e_pattern;

		while (pt != (PATTERN *) NULL)
		{
		    fprintf (Output, "%s", pt->p_expression->e_val.e_name);
		    if (pt->p_next != (PATTERN *) NULL)
			fprintf (Output, ", ");
		    pt = pt->p_next;
		}
	    }
	    if (paux->p_next != (PATTERN *) NULL)
		fprintf (Output, ", ");
        }
        fprintf (Output, ")\n{\n");
        fprintf (Output, "register int Result = 0;\n\n");
        fprintf (Output, "Result = ");
        CodePrintExpression (pe);
        fprintf (Output, ";\nreturn (Result);\n");
        fprintf (Output, "\n}\t/* end %s */\n\n", name);

}       /* end GenCodeExpressionWithPattern */


/*
 ****************************************************************
 * Generate Code for Reaction Condition                         *
 ****************************************************************
*/

void
GenCodeReaction (pa, pr, name)
PATTERN *pa;
EXPRESSION *pr;
char *name;
{
	NAMELIST *NamesInPattern ();
	char buffer[BUFSIZ];
	register NAMELIST *nl, *nh = (NAMELIST *) NULL;
	register PATTERN *paux;
	int i, j;

	strcpy (buffer, name);
	strcat (buffer, "_reaction");
	nh = nl = NamesInPattern (pa, (NAMELIST *) NULL);

/* Aqui acrescentei um argumento Buffer para a funcao abaixo */
/* Gabriel Antoine Louis Paillard                            */

	fprintf (Output, "%s ()\n{\nRT_VALUE *GenValueList ();\n",buffer);
	/*
	 * For each pattern we have an associated variable.
	 */
	for (paux = pa, i = 0; paux != (PATTERN *) NULL; paux = paux->p_next, i++)
	{
		if (paux->p_expression->e_tag == T_IDENTIFIER)
		{
		    fprintf (Output, "register RT_BAG *ptr_%d;\n", i);
		    fprintf (Output, "register int x_%d;\n", i);	    
		} else if (paux->p_expression->e_tag == T_TUPLE) {
		    fprintf (Output, "register RT_BAG *ptr_%d;\n", i); /* the tuple itself */
		    GenTupleVariables (paux->p_expression->e_val.e_pattern, &i); /* the elemts. of the tuple */
		} else
		    printf ("gm: Internal bloody error!\n"), exit (1);
	}
	fprintf (Output, "\nregister int result; \nregister RT_VALUE *link, *linkhead;\n\n");
	/*
	 * Generate the nested for loops 
	 */
	for (paux = pa, i = 0; paux != (PATTERN *) NULL; paux = paux->p_next, i++)
	{
	    if (paux->p_expression->e_tag == T_IDENTIFIER)
	        GenCodeBagIntSearch (i);
	    else if (paux->p_expression->e_tag == T_TUPLE) 
		GenCodeBagTupleSearch (&i, paux->p_expression->e_val.e_pattern); 
		
	}
	/*
	 * Generate the Call to the reaction function proper.
	 */
	GenCallReaction (name, pa);
	
        fprintf (Output, "    if (result == TRUE) {\n");
	/*
	 * Generate Call to Corresponding Action.
	 * But first we make a list of values with their names.
	 * This list is stored in linkhead.
	 */
	GenCallAction (pa);
	strcpy (buffer, name);
	strcat (buffer, "_action");
	fprintf (Output, "%s (linkhead);\n", buffer);
        fprintf (Output, "return (TRUE);\n}\n");

	/*
	 * Close all the nested for loops.
	 */
	CloseForLoops (pa,0);
	fprintf (Output, "UnlockBagNodes ();\n");
	fprintf (Output, "return (FALSE);\n"); 
	fprintf (Output, "\n}\t/* end %s_reaction */\n\n", name);
        strcpy (buffer, name);
        strcat (buffer, "_reaction_fnc");
	GenCodeExpressionWithPattern (pr, buffer, pa);		
		

}	/* end GenCodeReaction */

/*
 ****************************************************************
 * Generate Code for a Search of an integer in the Run Time Bag *
 ****************************************************************
*/

void
GenCodeBagIntSearch (i)
int i;
{
	fprintf (Output, "for (ptr_%d = RunTimeBag; ptr_%d != (RT_BAG *) NULL; ptr_%d = ptr_%d->rtb_next) {\n", 
										i,i,i,i);
	fprintf (Output, "   if (!NODE_LOCKED(ptr_%d->rtb_tag) && (NODE_TYPE(ptr_%d->rtb_tag) == TY_INT)) {\n", 
                                               i,i);
	fprintf (Output, "      LOCK_NODE(ptr_%d->rtb_tag);\n      x_%d = ptr_%d->rtb_element->rte_val;\n  }\n",
							i,i,i);	
	fprintf (Output, "   else continue;\n");
	return;

}	/* end GenCodeBagSearch */	

	
/*
 ****************************************************************
 * Generate Code for Actions                                    *
 ****************************************************************
*/

void
GenCodeActions (pa, name)
ACTION *pa;
char *name;
{
	char buffer_a[BUFSIZ], buffer_e[BUFSIZ];
	register NAMELIST *nl, *nh;
	register int i;
	register ACTION *paux;

	fprintf (Output, "%s_action (link) \nRT_VALUE *link;\n{", name);
	for (i = 0, paux = pa; paux != (ACTION *) NULL; i++, paux = paux->a_next)
	{
	    if (paux->a_expression->e_tag != T_EMPTY)
	        fprintf (Output, "%s_action_%d (link); \n", name, i);
	}
	fprintf (Output, "RemoveLockedBagNodes ();\n");
	fprintf (Output, "FreeListValues (link);\n");
	fprintf (Output, "\n}	/* end %s_action */\n\n", name);	
	for (i = 0, paux = pa; paux != (ACTION *) NULL; i++, paux = paux->a_next)
	{
            nh = nl = NamesInExpression (paux->a_expression, (NAMELIST *) NULL);
	    sprintf (buffer_a, "%s_action_%d", name, i);
	    sprintf (buffer_e, "%s_action_%d_e", name, i);
	    if (paux->a_expression->e_tag == T_TUPLE)
		GenCodeTupleActions (paux->a_expression->e_val.e_pattern, buffer_a);
	    else {
		GenCodeExpression (paux->a_expression, buffer_e, nl);
	        GenCodeActionItself (buffer_a, buffer_e, nl); 
	    }
	}

}	/* end GenCodeActions */


/*
 ****************************************************************
 * Generate Code for an Action                                  *
 ****************************************************************
*/

GenCodeActionItself (name_a, name_e, nh)
char *name_a;
char *name_e;
NAMELIST *nh;
{
	register NAMELIST *nl = nh;

	fprintf (Output, "%s (link) \n RT_VALUE *link; \n{\n", name_a);
	/*
	 * Generate all the values.
	 */
        while (nl != (NAMELIST *) NULL)
        {
            fprintf (Output, "\tregister int %s = GetValue (\"%s\", link);\n", nl->n_name, nl->n_name);
            nl = nl->n_next;
        }
	fprintf (Output, "register int result = 0;\n\n");
	/*
	 * Now invoke the function that evaluates the action itself.
	 */
	fprintf (Output, "result = %s (", name_e);
	nl = nh;
        while (nl != (NAMELIST *) NULL)
        {
            fprintf (Output, "%s", nl->n_name);
            if ((nl = nl->n_next) != (NAMELIST *) NULL)
                fprintf (Output, ", ");
        }
        fprintf (Output, ");");
	fprintf (Output, "RunTimeBag = AddRTBagNode (TY_INT, result);\n");
	fprintf (Output, "\n}	/* end %s */\n\n", name_a);
	
}	/* end GenCodeActionItself */


/*
 ****************************************************************
 * Generate Code for Definitions                                *
 ****************************************************************
*/

void
GenCodeDefinitions (pds)
DEFS *pds;
{
	void GenCodeBody ();
	register int tag;
	register DEFS *paux;

	for (paux = pds; paux != (DEFS *) NULL; paux = paux->d_next)
	    GenCodeBody (paux->d_definition->d_name, paux->d_definition->d_body);

}	/* end GenCodeDefinitions */
/*
 ****************************************************************
 * Generate Code for Bodies                                     *
 ****************************************************************
*/

void
GenCodeBody (name, pb)
char *name;
BODY *pb;
{
	void GenCodeCombinator ();
        register int tag;
        register BODY *paux = pb;

        if ((tag = paux->b_tag) == T_BASIC_BODY)
        {
            GenCodeReaction (paux->b_val.b_bbody->b_pattern,
                            paux->b_val.b_bbody->b_reaction,
                            name);
            GenCodeActions (paux->b_val.b_bbody->b_action,
                            name);
        } else if (tag == T_IDENTIFIER) {
            fprintf (Output, "%s_reaction ()\n{\n", name);
	    fprintf (Output, "\treturn (%s_reaction());\n\n}\t/* end %s_reaction */\n\n",
                              paux->b_val.b_name, name);
        } else if (tag == T_COMBINATOR)
            GenCodeCombinator (name, paux->b_val.b_comb); 

}	/* end GenCodeBody */

/*
 ****************************************************************
 * Generate Code for Parallel and Sequential Composition        *
 ****************************************************************
*/

void
GenCodeCombinator (name, pc)
char *name;
COMBINATOR *pc;
{
	void GenCodeParComposition (),
	     GenCodeSeqComposition ();

	if (pc->c_combinator == T_PAR)
	    GenCodeParComposition (name, pc);
	else
	    GenCodeSeqComposition (name, pc);
	
}	/* end GenCodeCombinator */

/*
 ****************************************************************
 * Generate Code for Sequential Composition                     *
 ****************************************************************
*/

void
GenCodeSeqComposition (name, pc)
char *name;
COMBINATOR *pc;
{
	char *UniqueName ();
	char n1[BUFSIZ], n2[BUFSIZ];

	strcpy (n1, UniqueName ("seq_r"));
	strcpy (n2, UniqueName ("seq_l"));
	fprintf (Output, "%s_reaction ()\n{\n", name);

/* Aqui inverti a ordem dos loops, pois nao estava utilizando o operador ; corretamente, Gabriel Paillard 07/06/1999         */
	
        fprintf (Output, "\tfor (;;) {\n");
        fprintf (Output, "\t    if (%s_reaction () == TRUE) continue;\n", n2);
        fprintf (Output, "\t    break;\n}\n"); 
       	fprintf (Output, "\tfor (;;) {\n");
	fprintf (Output, "\t    if (%s_reaction () == TRUE) continue;\n", n1);
	fprintf (Output, "\t    break;\n}\n");
	fprintf (Output, "\treturn (FALSE);\n");
	fprintf (Output, "\n}\t/* end %s_reaction */\n\n", name);
	GenCodeBody (n1, pc->c_right);
	GenCodeBody (n2, pc->c_left);


}	/* end GenCodeSeqComposition */

/*
 ****************************************************************
 * Generate Code for Parallel Composition                       *
 ****************************************************************
*/

void
GenCodeParComposition (name, pc)
char *name;
COMBINATOR *pc;
{
        char *UniqueName ();
        char n1[BUFSIZ], n2[BUFSIZ];

        strcpy (n1, UniqueName ("par_r"));
        strcpy (n2, UniqueName ("par_l"));
        fprintf (Output, "%s_reaction ()\n{\n", name);
	fprintf (Output, "long flag;\n");
	fprintf (Output, "register int res1 = TRUE, res2 = TRUE;\n\n");
        fprintf (Output, "\tfor (;;) {\n");
	fprintf (Output, "\t    flag = time((time_t *) NULL) %c 2;\n", '%');
	fprintf (Output, "\t    if (flag) {\n");
        fprintf (Output, "\t\tres1 =  %s_reaction ();\n", n1);
	fprintf (Output, "\tif (res1 == FALSE) {\n");
	fprintf (Output, "\t res2 = %s_reaction ();\n", n2);
	fprintf (Output, "\t if (res2 == FALSE) \n return (FALSE);\n}\n");
	fprintf (Output, "\t   } else {\n");
	fprintf (Output, "\t\tres2 =  %s_reaction ();\n", n2);
        fprintf (Output, "\tif (res2 == FALSE) {\n");
        fprintf (Output, "\t res1 = %s_reaction ();\n", n1);
        fprintf (Output, "\t if (res1 == FALSE) \n return (FALSE);\n}\n}\n");
	fprintf (Output, "\t}\n");
        fprintf (Output, "\n}\t/* end %s_reaction */\n\n", name);
        GenCodeBody (n1, pc->c_right);
        GenCodeBody (n2, pc->c_left);

}	/* end GenCodeParComposition */

/*
 ****************************************************************
 * Generate Code for the Gamma Function                         *
 ****************************************************************
*/

void
GenMainCode (Program)
PROGRAM *Program;
{
	fprintf (Output, "Gamma ()\n{\n");
        fprintf (Output, "\tfor (;;)\n\t{\n");
        fprintf (Output, "if (gamma_reaction () == TRUE) continue;\n");
        fprintf (Output, "\tbreak;\n\t}");
        fprintf (Output, "PrintRTBag ();\n");
        fprintf (Output, "\n}   /* end Gamma */\n\n");
        GenCodeDefinitions (Program->p_defs);
	GenCodeBody ("gamma", Program->p_body);

}       /* end GenMainCode */


/*
 ****************************************************************
 * Generate a Unique Name                                       *
 ****************************************************************
*/

char *
UniqueName (template)
char *template;
{
	static char buffer[BUFSIZ];
	static int val = 0;

	sprintf (buffer, "___%s_%d", template, ++val);
	return (buffer);

}	/* end UniqueName */

	

GenCodeTupleActions (pt, name)
PATTERN *pt;
char *name;
{
        char buffer_a[BUFSIZ], buffer_e[BUFSIZ];
        register NAMELIST *nl, *nh;
        register int i;
        register PATTERN *paux;

        fprintf (Output, "%s (ln) \nRT_VALUE *ln;\n{", name);
	fprintf (Output, "\tregister int result;\n");
	fprintf (Output, "\tregister RT_VALUE *linkhead, *link;\n\n");
        for (i = 0, paux = pt; paux != (PATTERN *) NULL; i++, paux = paux->p_next)
        {
            fprintf (Output, "result = %s_tuple_action_%d (ln); \n", name, i);
	    if (i == 0)
	    {
       		fprintf (Output, "linkhead = link = (RT_VALUE *) Alloc (sizeof (RT_VALUE));\n");
	 	fprintf (Output, "link->rtv_val = (RT_ELEMENT *) Alloc (sizeof (RT_ELEMENT));\n");
                fprintf (Output, "link->rtv_val->rte_val = result;\n");
		fprintf (Output, "link->rtv_name = (char *) NULL;");
		fprintf (Output, "link->rtv_next = (RT_VALUE *) NULL;\n");
	    } else {
		fprintf (Output, "link = link->rtv_next = (RT_VALUE *) Alloc (sizeof (RT_VALUE));\n");
	 	fprintf (Output, "link->rtv_val = (RT_ELEMENT *) Alloc (sizeof (RT_ELEMENT));\n");
                fprintf (Output, "link->rtv_val->rte_val = result;\n");
		fprintf (Output, "link->rtv_name = (char *) NULL;");
		fprintf (Output, "link->rtv_next = (RT_VALUE *) NULL;\n");
	    }	
        }
	fprintf (Output, "GenRTTuple (linkhead);\n");
        fprintf (Output, "RemoveLockedBagNodes ();\n");
        fprintf (Output, "FreeListValues (linkhead);\n");
        fprintf (Output, "\n}   /* end %s_tuple_action */\n\n", name);
        for (i = 0, paux = pt; paux != (PATTERN *) NULL; i++, paux = paux->p_next)
        {
            nh = nl = NamesInExpression (paux->p_expression, (NAMELIST *) NULL);
            sprintf (buffer_a, "%s_tuple_action_%d", name, i);
            sprintf (buffer_e, "%s_tuple_action_%d_e", name, i);
            GenCodeExpression (paux->p_expression, buffer_e, nl);
            GenCodeTupleActionItself (buffer_a, buffer_e, nl);
        }

}       /* end GenCodeTupleActions */

GenCodeTupleActionItself (name_a, name_e, nh)
char *name_a;
char *name_e;
NAMELIST *nh;
{
        register NAMELIST *nl = nh;

        fprintf (Output, "%s (link) \n RT_VALUE *link; \n{\n", name_a);
        /*
         * Generate all the values.
         */
        while (nl != (NAMELIST *) NULL)
        {
            fprintf (Output, "\tregister int %s = GetValue (\"%s\", link);\n", nl->n_name, nl->n_name);
            nl = nl->n_next;
        }
        fprintf (Output, "register int result = 0;\n\n");
        /*
         * Now invoke the function that evaluates the action itself.
         */
        fprintf (Output, "result = %s (", name_e);
        nl = nh;
        while (nl != (NAMELIST *) NULL)
        {
            fprintf (Output, "%s", nl->n_name);
            if ((nl = nl->n_next) != (NAMELIST *) NULL)
                fprintf (Output, ", ");
        }
        fprintf (Output, ");");
        fprintf (Output, "return (result);\n");
        fprintf (Output, "\n}   /* end %s */\n\n", name_a);

}       /* end GenCodeActionItself */




GenTupleVariables (pt,i)
PATTERN *pt;
int *i;
{
	register PATTERN *paux;

	for (paux = pt; paux != (PATTERN *) NULL; paux = paux->p_next)
	{
	    (*i)++;
	    fprintf (Output, "register RT_ELEMENT *ptr_%d;\n", *i);
	    fprintf (Output, "register int x_%d;\n", *i); 
	}

}	/* end GenTupleVariables */


GenCodeBagTupleSearch (i, pt)
int *i;
PATTERN *pt;
{
	register int size = 0;
	register PATTERN *paux;

	for (paux = pt; paux != (PATTERN *) NULL; paux = paux->p_next)
	    size++;
        fprintf (Output, "for(ptr_%d=RunTimeBag;ptr_%d!=(RT_BAG *)NULL;ptr_%d=ptr_%d->rtb_next) {\n", 
                                      *i, *i, *i, *i);
        fprintf (Output, "if(!NODE_LOCKED(ptr_%d->rtb_tag)&&(NODE_TYPE(ptr_%d->rtb_tag)==TY_TUPLE)){\n",
                                               *i,*i);
	fprintf (Output, "if (ptr_%d->rtb_element->rte_tuple->rtt_size == %d) {\n", *i, size);   
        fprintf (Output, "      LOCK_NODE(ptr_%d->rtb_tag);\n", *i);
	fprintf (Output, "{\nregister RT_LIST *pl;\n");
	fprintf (Output, "pl = ptr_%d->rtb_element->rte_tuple->rtt_list;\n", *i);
	for (paux = pt; paux != (PATTERN *) NULL; paux = paux->p_next)	
	{
	    (*i)++;
	    fprintf (Output, "x_%d = pl->rtl_element->rte_val;\n",*i );
	    fprintf (Output, "ptr_%d = pl->rtl_element;\n", *i);
	    fprintf (Output, "pl = pl->rtl_next;\n");
	}
	fprintf (Output, "}\n} else continue;\n");
        fprintf (Output, "   } else continue;\n");
        return;

}       /* end GenCodeBagSearch */

GenCallAction (pp)
PATTERN *pp;
{
	char buffer[BUFSIZ];
	register PATTERN *paux;
	register int size = 0;
	register int j;

	for (paux = pp; paux != (PATTERN *) NULL; paux = paux->p_next)
	{
	    if (paux->p_expression->e_tag == T_IDENTIFIER)
	        size++;
	    else {
                register PATTERN *pt = paux->p_expression->e_val.e_pattern;

                while (pt != (PATTERN *) NULL)
                {
		    size++;
		    pt = pt->p_next;
		}
	    }
	}
	fprintf (Output, "linkhead = link = GenValueList (%d);\n", size);
        for (paux = pp, j = 0; paux != (PATTERN *) NULL; paux = paux->p_next, j++)
        {
            if (paux->p_expression->e_tag == T_IDENTIFIER)
	    {
                sprintf (buffer, "ptr_%d", j);
                fprintf (Output, "link->rtv_val = %s->rtb_element;\n", buffer);
		fprintf (Output, "link->rtv_name =  Alloc (%d);\n", 
			strlen (paux->p_expression->e_val.e_name)+1);
		fprintf (Output, "strcpy (link->rtv_name, \"%s\");\n",
				paux->p_expression->e_val.e_name);
		fprintf (Output, "link = link->rtv_next;\n");
            } else if (paux->p_expression->e_tag == T_TUPLE) {
                register PATTERN *pt = paux->p_expression->e_val.e_pattern;

		j++; /* skip the pointer to the tuple node itself */
                while (pt != (PATTERN *) NULL)
                {
		    sprintf (buffer, "ptr_%d", j);
		    fprintf (Output, "link->rtv_val = %s;\n", buffer);
		     fprintf (Output, "link->rtv_name =  Alloc (%d);\n", 
                        strlen (pt->p_expression->e_val.e_name) + 1);
		     fprintf (Output, "strcpy (link->rtv_name, \"%s\");\n",
				pt->p_expression->e_val.e_name);
		    fprintf (Output, "link = link->rtv_next;\n");
                    if ((pt = pt->p_next) != (PATTERN *) NULL)
		        j++;
                }
            }
        }

}	/* end GenCallAction */



GenCallReaction (name, pp)
char *name;
PATTERN *pp;
{
	register PATTERN *paux;
	register int j;

	fprintf (Output, "    result = %s_reaction_fnc (", name);
	for (paux = pp, j = 0; paux != (PATTERN *) NULL; paux = paux->p_next)
	{
	    if (paux->p_expression->e_tag == T_IDENTIFIER)
	        fprintf (Output, "x_%d", j++);
	    else {
                register PATTERN *pt = paux->p_expression->e_val.e_pattern;

		j++;      /* skip the index for pointer to tuple itself */
                while (pt != (PATTERN *) NULL)
                {
		    fprintf (Output, "x_%d", j++);
		    if ((pt = pt->p_next) != (PATTERN *) NULL)
			fprintf (Output, ", ");
		}
	    }
	    if (paux->p_next != (PATTERN *) NULL)
		fprintf (Output, ", ");
	}
	fprintf (Output, ");\n");

}	/* end GenCallReaction */

CloseForLoops (pp,i)
PATTERN *pp;
int i;
{
	int j;

	if (pp == (PATTERN *) NULL)
	    return;
        if (pp->p_expression->e_tag == T_IDENTIFIER)
	{
	    CloseForLoops (pp->p_next, ++i);
	    fprintf (Output, "UNLOCK_NODE(ptr_%d->rtb_tag);\n}\n", i-1);
        } else {
                register PATTERN *pt = pp->p_expression->e_val.e_pattern;

		j = i++;
                while (pt != (PATTERN *) NULL)
                {
		    if ((pt = pt->p_next) != (PATTERN *) NULL)
		        i++;
                }
		CloseForLoops (pp->p_next, ++i);
		fprintf (Output, "UNLOCK_NODE(ptr_%d->rtb_tag);\n}\n", j);
        }

}	/* end CloseForLoops */


/****************************************************************/
/****************************************************************/
/****************************************************************/
/* New Scheduler Graph-Based Functions							*/
/****************************************************************/
/****************************************************************/
/****************************************************************/



/*
 ****************************************************************
 * NSGB - Write Header with Included Files                      *
 ****************************************************************
 */

void
NSGBWriteHeader ()
{
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","wa");

	fprintf (OutputNSGB, "# include <stdio.h>\n");
	fprintf (OutputNSGB, "# include <stdlib.h>\n");
	fprintf (OutputNSGB, "# include <time.h>\n");
	fprintf (OutputNSGB, "# include <pthread.h>\n");
	fprintf (OutputNSGB, "# include <semaphore.h>\n");
	fprintf (OutputNSGB, "# include \"include/const.h\"\n");
	fprintf (OutputNSGB, "# include \"include/types.h\"\n");
	fprintf (OutputNSGB, "# include \"include/fnc.h\"\n \n \n");

	fclose(OutputNSGB);

}	/* end WriteHeader */


/*
 ****************************************************************
 * Write Global Variables		                                *
 ****************************************************************
 */

void
NSGBWriteGlobalVariables (num_reactions)
int num_reactions;
{
	int i;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 

	fprintf (OutputNSGB, "//Global Variables\n");
	fprintf (OutputNSGB, "reg_multiset *multiset;     // Multiset elements list\n");
	fprintf (OutputNSGB, "reg_vertex *vertices;       // Graph - list of vertices and its edges\n");
	fprintf (OutputNSGB, "reg_sink *sinks;            // Sink list\n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "reg_I *instances_I%i;        // Instances List (I%i)\n",i,i);
	}
	fprintf (OutputNSGB, "sem_t sem_multiset;       	// Semaphore to control the shared memory access - the multiset\n");
	fprintf (OutputNSGB, "sem_t sem_graph;   			// Semaphore to control the shared memory access - the graph\n");
	fprintf (OutputNSGB, "int id_element;             // Multiset Id element\n \n");
	
	fclose(OutputNSGB);
}	/* end NSGBWriteGlobalVariables */


/*
 ****************************************************************
 * NSGB - Generate Code for the Initial Multiset                *
 ****************************************************************
 */

void
NSGBGenInitialMultiset (pb)
BAG *pb;
{
	register BAG *paux = pb;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 
	
	fprintf(OutputNSGB, "\t/************** INITIAL MULTISET ***************/ \n");

	while (paux != (BAG *) NULL){
		if (paux->b_val.b_expression->e_tag == T_NUMERAL)
			fprintf(OutputNSGB, "\tpm = AddMultisetElement (&id_element, %d); \n", paux->b_val.b_expression->e_val.e_numval);
		else    /* let's treat boolean values as integers */
			fprintf(OutputNSGB, "\tpm = AddMultisetElement (&id_element, %d); \n", paux->b_val.b_expression->e_val.e_boolval);
        paux = paux->b_next;
    }
	fprintf(OutputNSGB, "\n \n");

	fclose(OutputNSGB);

}	/* end NSGBGenInitialMultiset  */


/*
 ****************************************************************
 * NSGB - Local Variables and Initializations	                *
 ****************************************************************
 */
void
NSGBLocalVarsInicialization (num_reactions)
int num_reactions;
{
	int i;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 

	fprintf (OutputNSGB, "// Main\n");
	fprintf (OutputNSGB, "int main (void){ \n \n");
	fprintf (OutputNSGB, "\t/************ VARIABLES DECLARATION ************/\n");
	fprintf (OutputNSGB, "\t// Local Variables\n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\treg_multiset *multiset_M%i;                      // Copy of Multiset Elements List (M%i)\n",i,i);
		fprintf (OutputNSGB, "\treg_I *usedinstances_I%i;                        // Used Instances List (I%i) - store the history of instances provided\n",i,i);
	}
	fprintf (OutputNSGB, "\n");
	fprintf (OutputNSGB, "\t// Local Auxiliary Variables\n");
	fprintf (OutputNSGB, "\treg_multiset *pm;\n");
	fprintf (OutputNSGB, "\treg_vertex *pv;\n");
	fprintf (OutputNSGB, "\treg_sink *ps;\n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\treg_I element_I%i; \n",i);	 	
	}
	fprintf (OutputNSGB, "\treg_I *pi, *pix; \n");
	fprintf (OutputNSGB, "\treg_data data; \n");
	fprintf (OutputNSGB, "\tint id_vertex, i; \n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\tint allow_%i = FALSE;                   // To allow the opportunity to generate again some instances...  \n",i);	 	
	}
	fprintf (OutputNSGB, "\tpthread_t threads[THREADS]; \n \n \n");
	
	fprintf (OutputNSGB, "\t/********** VARIABLES INITIALIZATION ***********/ \n");
	fprintf (OutputNSGB, "\t// Initializing pointers \n");
	fprintf (OutputNSGB, "\tvertices = (reg_vertex *) NULL; \n");
	fprintf (OutputNSGB, "\tmultiset = (reg_multiset *) NULL;  \n");
	fprintf (OutputNSGB, "\tsinks = (reg_sink *) NULL; \n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\tinstances_I%i = (reg_I *) NULL;  \n",i);	 	
		fprintf (OutputNSGB, "\tusedinstances_I%i = (reg_I *) NULL;  \n",i);
		fprintf (OutputNSGB, "\tmultiset_M%i = (reg_multiset *) NULL;  \n",i);
	}
	fprintf (OutputNSGB, "\n");

	fprintf (OutputNSGB, "\t// Initializing variables \n");
	fprintf (OutputNSGB, "\tid_vertex = 0;                  //General Identifier for each vertex included \n");
	fprintf (OutputNSGB, "\tid_element = 0;                 //General Identifier for each multiset element included \n \n");
	fprintf (OutputNSGB, "\t//Initializing Semaphores \n");
	fprintf (OutputNSGB, "\tsem_init(&sem_multiset, 0, 1); \n");
	fprintf (OutputNSGB, "\tsem_init(&sem_graph, 0, 1); \n \n \n");
	
	fclose(OutputNSGB);

}	/* end NSGBLocalVarsInicialization */


/*
 ****************************************************************
 * NSGB - Instances Generation					                *
 ****************************************************************
 */
void
NSGBInstancesGeneration (num_reactions)
int num_reactions;
{
	int i;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 

	fprintf (OutputNSGB, "\t/************ INSTANCES GENERATION *************/\n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\tGeneratesInstancesRx (&multiset_M%i, &instances_I%i, &usedinstances_I%i);\n",i,i,i);
	}
	fprintf (OutputNSGB, "\n \n");

	fclose(OutputNSGB);

}	/* end NSGBInstancesGeneration */


/*
 ****************************************************************
 * NSGB - Main Loop								                *
 ****************************************************************
 */
void
NSGBMainLoop (num_reactions)
int num_reactions;
{
	int i;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 

	fprintf (OutputNSGB, "\t/****************** MAIN LOOP ******************/\n");
	fprintf (OutputNSGB, "\twhile (");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "(instances_I%i != NULL)",i);
		if(i < num_reactions)
			fprintf (OutputNSGB, " || ");
	}
	fprintf (OutputNSGB, ") { \n \n");

	fprintf (OutputNSGB, "\t\t/*************** GRAPH GENERATION **************/\n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\t\t//Creating Initial Graph (from I%i Instances List) \n",i);
		fprintf (OutputNSGB, "\t\tpix = instances_I%i; \n",i);
		fprintf (OutputNSGB, "\t\twhile(pix != NULL){ \n");
		fprintf (OutputNSGB, "\t\t\tdata.element1 = pix->data.element1; \n");
		fprintf (OutputNSGB, "\t\t\tdata.element2 = pix->data.element2; \n");
		fprintf (OutputNSGB, "\t\t\tpv = AddVertex (&id_vertex, data, %i); \n",i);
		fprintf (OutputNSGB, "\t\t\tCreateOutputEdges (data, %i); \n",i);
		fprintf (OutputNSGB, "\t\t\telement_I%i.data = pix->data; \n",i);
		fprintf (OutputNSGB, "\t\t\telement_I%i.id_data = pix->id_data; \n",i);
		fprintf (OutputNSGB, "\t\t\tpi = AddInstanceIx (&usedinstances_I%i, element_I%i); \n",i,i);
		fprintf (OutputNSGB, "\t\t\tpix = pix->next; \n");
		fprintf (OutputNSGB, "\t\t} \n \n \n");
	}

	fprintf (OutputNSGB, "\t\t/************ SINKS IDENTIFICATION *************/ \n");
	fprintf (OutputNSGB, "\t\tIdentifySinks(); \n \n \n");


	fprintf (OutputNSGB, "\t\t/*********** THREADS INITIALIZATION ************/ \n");
	fprintf (OutputNSGB, "\t\twhile(vertices != NULL){ \n");
	fprintf (OutputNSGB, "\t\t\tps = sinks; \n");
	fprintf (OutputNSGB, "\t\t\twhile (ps != NULL){ \n");
	fprintf (OutputNSGB, "\t\t\t\tpthread_create(&threads[ps->id], NULL, ThreadFunction, (void *)(&(*ps))); \n");
	fprintf (OutputNSGB, "\t\t\t\tps = ps->next; \n");
	fprintf (OutputNSGB, "\t\t\t} \n");
	fprintf (OutputNSGB, "\t\t\t// Finishing the threads \n");
	fprintf (OutputNSGB, "\t\t\tps = sinks; \n");
	fprintf (OutputNSGB, "\t\t\twhile (ps != NULL){ \n");
	fprintf (OutputNSGB, "\t\t\t\tpthread_join(threads[ps->id], NULL); \n");
	fprintf (OutputNSGB, "\t\t\t\tps = ps->next; \n");
	fprintf (OutputNSGB, "\t\t\t} \n \n");

	fprintf (OutputNSGB, "\t\t\t// Delete the sink list before starting another sinks identification... \n");
	fprintf (OutputNSGB, "\t\t\tDeleteAllSinks(); \n \n");

	fprintf (OutputNSGB, "\t\t\t// Identify new sinks to start another iteration... \n");
	fprintf (OutputNSGB, "\t\t\tIdentifySinks(); \n");
	fprintf (OutputNSGB, "\t\t} \n \n \n");

	fprintf (OutputNSGB, "\t\t/********** NEW INSTANCES GENERATION ***********/\n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\t\tDeleteAllInstanceIx (&instances_I%i);    // Delete all instance list before starting another iteration...\n",i);
		fprintf (OutputNSGB, "\t\tGeneratesInstancesRx (&multiset_M%i, &instances_I%i, &usedinstances_I%i);\n",i,i,i);
	}
	fprintf (OutputNSGB, "\n \n");

	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\t\t//Allowing the opportunity to generate again some instances...\n");
		fprintf (OutputNSGB, "\t\tif((instances_I%i==NULL) && (!allow_%i)){\n",i,i);
		fprintf (OutputNSGB, "\t\t\tallow_%i = TRUE;\n",i);
		fprintf (OutputNSGB, "\t\t\tDeleteAllInstanceIx(&usedinstances_I%i);\n",i);
		fprintf (OutputNSGB, "\t\t\tGeneratesInstancesRx (&multiset_M%i, &instances_I%i, &usedinstances_I%i);\n",i,i,i);
		fprintf (OutputNSGB, "\t\t}\n \n");
	}
	fprintf (OutputNSGB, "\t}		// End of the Main Loop \n \n \n");

	fprintf (OutputNSGB, "\t// Deleting the history of instances provided to both reactions... \n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "\tDeleteAllInstanceIx (&usedinstances_I%i); \n",i);
	}
	fprintf (OutputNSGB, "\n \n");

	fprintf (OutputNSGB, "\tprintf(\"\\n \\n\"); \n");
	fprintf (OutputNSGB, "\tprintf(\"######################## \\n\"); \n");
	fprintf (OutputNSGB, "\tprintf(\"END OF THE PROGRAM! \\n \\n\"); \n");
	fprintf (OutputNSGB, "\tprintf(\"Final Multiset: \\n \\n\"); \n");
	fprintf (OutputNSGB, "\tListingMultiset(); \n");
	fprintf (OutputNSGB, "\tprintf(\"\\n\"); \n");
	fprintf (OutputNSGB, "\tprintf(\"######################## \\n\"); \n \n");
	fprintf (OutputNSGB, "\texit (0); \n");
	fprintf (OutputNSGB, "} \n \n \n");

	fclose(OutputNSGB);

}	/* end NSGBMainLoop */


/*
 ****************************************************************
 * NSGB - Generate Code for Reactions			                *
 ****************************************************************
 */

void
NSGBGenCodeReactions (num_reactions)
int num_reactions;
{
	int i;
	register DEFS *pdefs = Program->p_defs;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 

	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "/***************************************************************/ \n");
		fprintf (OutputNSGB, "/*******  Function related to the R%i reaction execution ********/ \n",i);
		fprintf (OutputNSGB, "/***************************************************************/ \n");
		fprintf (OutputNSGB, "int R%iFunction(reg_sink *ps){ \n",i);
		fprintf (OutputNSGB, "\t// Name of the gamma reaction - according the gamma code: %s \n", pdefs->d_definition->d_name);
		fprintf (OutputNSGB, "\tint %s, %s, result; \n", pdefs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name, pdefs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name);
		fprintf (OutputNSGB, "\treg_multiset *pm; \n");
		fprintf (OutputNSGB, "\treg_I element_Ix, *pi; \n \n");

		fprintf (OutputNSGB, "\t%s  = ps->data.element1; \n", pdefs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name);
		fprintf (OutputNSGB, "\t%s  = ps->data.element2; \n", pdefs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name);

		fprintf (OutputNSGB, "\telement_Ix.data.element1 = ps->data.element1; \n");
		fprintf (OutputNSGB, "\telement_Ix.data.element2 = ps->data.element2; \n");

		strcpy(expression,"");
		NSGBCodePrintExpression(pdefs->d_definition->d_body->b_val.b_bbody->b_action->a_expression);
		fprintf (OutputNSGB, "\tresult = %s; \n \n",expression);

		strcpy(expression,"");
		NSGBCodePrintExpression(pdefs->d_definition->d_body->b_val.b_bbody->b_reaction);
		fprintf (OutputNSGB, "\tif (%s) { \n", expression);	

		fprintf (OutputNSGB, "\t\tsem_wait(&sem_multiset); \n");
		fprintf (OutputNSGB, "\t\tpm = AddMultisetElement (&id_element, result); \n");
		fprintf (OutputNSGB, "\t\tpi = SearchInstanceIx(&instances_I%i, element_Ix); \n",i);
		fprintf (OutputNSGB, "\t\tDeleteMultisetElementId (pi->id_data.id1); \n");
		fprintf (OutputNSGB, "\t\tDeleteMultisetElementId (pi->id_data.id2); \n");
		fprintf (OutputNSGB, "\t\tsem_post(&sem_multiset); \n");
		fprintf (OutputNSGB, "\t\treturn TRUE;  \n");
		fprintf (OutputNSGB, "\t} \n");
		fprintf (OutputNSGB, "\telse { \n");
		fprintf (OutputNSGB, "\t\treturn FALSE; \n");
		fprintf (OutputNSGB, "\t} \n");
		fprintf (OutputNSGB, "} \n \n");

		pdefs = pdefs->d_next; // to update the information (name vars, reaction condition, etc) to the next reaction
	}
	fprintf (OutputNSGB, "\n");


		fprintf (OutputNSGB, "/***************************************************************/ \n");
		fprintf (OutputNSGB, "/*******  Function for the Threads fired by the Sinks **********/ \n",i);
		fprintf (OutputNSGB, "/***************************************************************/ \n");
		
		fprintf (OutputNSGB, "void * ThreadFunction (void *arg){ \n");
		fprintf (OutputNSGB, "\treg_sink *ps; \n");
		fprintf (OutputNSGB, "\treg_multiset *pm; \n");
		fprintf (OutputNSGB, "\tps = (reg_sink *)(arg); \n");
		fprintf (OutputNSGB, "\tint reacted; \n \n");
		fprintf (OutputNSGB, "\t/************** REACTION EXECUTION *************/ \n");
		
		for(i = 1; i <= num_reactions; i++){
			fprintf (OutputNSGB, "\tif (ps->reaction == %i){   // Try to execute the reaction %i ... \n",i,i);
			fprintf (OutputNSGB, "\t\treacted = R%iFunction(ps); \n",i);	
			fprintf (OutputNSGB, "\t} \n");	
		}
		fprintf (OutputNSGB, "\n");	
		fprintf (OutputNSGB, "\tsem_wait(&sem_graph); \n \n");
		fprintf (OutputNSGB, "\tEdgesReversalSink(ps); \n \n");
		fprintf (OutputNSGB, "\t/********* VERTICES AFFECTED REMOVAL ***********/ \n");
		fprintf (OutputNSGB, "\t/******************** AND **********************/ \n");
		fprintf (OutputNSGB, "\t/**************** SINK REMOVAL *****************/ \n \n");
		fprintf (OutputNSGB, "\tif (reacted){ \n");					
		fprintf (OutputNSGB, "\t\tDeleteVerticesAffected(ps);     // all vertices affected, included its own vertex \n");					
		fprintf (OutputNSGB, "\t} \n");					
		fprintf (OutputNSGB, "\telse { \n");					
		fprintf (OutputNSGB, "\t\tDeleteVertex(ps->data, ps->reaction);   // Delete the vertex that execute as a Sink \n");
		fprintf (OutputNSGB, "\t} \n \n");				
		fprintf (OutputNSGB, "\tsem_post(&sem_graph);  \n \n");									
		fprintf (OutputNSGB, "}  \n \n");									


	fclose(OutputNSGB);

}	/* end NSGBGenCodeReactions  */


/*
 ****************************************************************
 * NSGB - Generate Declarations for the Reactions                *
 ****************************************************************
 */
void
NSGBGenDeclarationsReactions (num_reactions)
int num_reactions;
{
	int i;
	FILE *OutputNSGB;
	OutputNSGB = fopen ("../RunTimeNew/main.c","a"); 

	fprintf (OutputNSGB, "// Declaration of Functions used by the trheads \n");
	for(i = 1; i <= num_reactions; i++){
		fprintf (OutputNSGB, "int R%iFunction(reg_sink *ps); \n",i);
	}
	fprintf (OutputNSGB, "\n");

	fclose(OutputNSGB);
}	/* end NSGBGenDeclarationsReactions  */
