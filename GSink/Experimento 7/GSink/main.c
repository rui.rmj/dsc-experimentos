# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <pthread.h>
# include <semaphore.h>
# include "include/const.h"
# include "include/types.h"
# include "include/fnc.h"
 
 
// Declaration of Functions used by the trheads 
int R1Function(reg_sink *ps); 
int R2Function(reg_sink *ps); 
int R3Function(reg_sink *ps); 

//Global Variables
reg_multiset *multiset;     // Multiset elements list
reg_vertex *vertices;       // Graph - list of vertices and its edges
reg_sink *sinks;            // Sink list
reg_I *instances_I1;        // Instances List (I1)
reg_I *instances_I2;        // Instances List (I2)
reg_I *instances_I3;        // Instances List (I3)
sem_t sem_multiset;       	// Semaphore to control the shared memory access - the multiset
sem_t sem_graph;   			// Semaphore to control the shared memory access - the graph
int id_element;             // Multiset Id element
 
// Main
int main (void){ 
 
	/************ VARIABLES DECLARATION ************/
	// Local Variables
	reg_multiset *multiset_M1;                      // Copy of Multiset Elements List (M1)
	reg_I *usedinstances_I1;                        // Used Instances List (I1) - store the history of instances provided
	reg_multiset *multiset_M2;                      // Copy of Multiset Elements List (M2)
	reg_I *usedinstances_I2;                        // Used Instances List (I2) - store the history of instances provided
	reg_multiset *multiset_M3;                      // Copy of Multiset Elements List (M3)
	reg_I *usedinstances_I3;                        // Used Instances List (I3) - store the history of instances provided

	// Local Auxiliary Variables
	reg_multiset *pm;
	reg_vertex *pv;
	reg_sink *ps;
	reg_I element_I1; 
	reg_I element_I2; 
	reg_I element_I3; 
	reg_I *pi, *pix; 
	reg_data data; 
	int id_vertex, i; 
	int allow_1 = FALSE;                   // To allow the opportunity to generate again some instances...  
	int allow_2 = FALSE;                   // To allow the opportunity to generate again some instances...  
	int allow_3 = FALSE;                   // To allow the opportunity to generate again some instances...  
	pthread_t threads[THREADS]; 
 
 
	/********** VARIABLES INITIALIZATION ***********/ 
	// Initializing pointers 
	vertices = (reg_vertex *) NULL; 
	multiset = (reg_multiset *) NULL;  
	sinks = (reg_sink *) NULL; 
	instances_I1 = (reg_I *) NULL;  
	usedinstances_I1 = (reg_I *) NULL;  
	multiset_M1 = (reg_multiset *) NULL;  
	instances_I2 = (reg_I *) NULL;  
	usedinstances_I2 = (reg_I *) NULL;  
	multiset_M2 = (reg_multiset *) NULL;  
	instances_I3 = (reg_I *) NULL;  
	usedinstances_I3 = (reg_I *) NULL;  
	multiset_M3 = (reg_multiset *) NULL;  

	// Initializing variables 
	id_vertex = 0;                  //General Identifier for each vertex included 
	id_element = 0;                 //General Identifier for each multiset element included 
 
	//Initializing Semaphores 
	sem_init(&sem_multiset, 0, 1); 
	sem_init(&sem_graph, 0, 1); 
 
 
	/************** INITIAL MULTISET ***************/ 
	pm = AddMultisetElement (&id_element, 1); 
	pm = AddMultisetElement (&id_element, 2); 
	pm = AddMultisetElement (&id_element, 3); 
	pm = AddMultisetElement (&id_element, 4); 
	pm = AddMultisetElement (&id_element, 5); 
	pm = AddMultisetElement (&id_element, 6); 
	pm = AddMultisetElement (&id_element, 7); 
	pm = AddMultisetElement (&id_element, 8); 
	pm = AddMultisetElement (&id_element, 9); 
	pm = AddMultisetElement (&id_element, 10); 
	pm = AddMultisetElement (&id_element, 11); 
	pm = AddMultisetElement (&id_element, 12); 
	pm = AddMultisetElement (&id_element, 13); 
	pm = AddMultisetElement (&id_element, 14); 
	pm = AddMultisetElement (&id_element, 15); 
	pm = AddMultisetElement (&id_element, 16); 
	pm = AddMultisetElement (&id_element, 17); 
	pm = AddMultisetElement (&id_element, 18); 
	pm = AddMultisetElement (&id_element, 19); 
	pm = AddMultisetElement (&id_element, 20); 
	pm = AddMultisetElement (&id_element, 21); 
	pm = AddMultisetElement (&id_element, 22); 
	pm = AddMultisetElement (&id_element, 23); 
	pm = AddMultisetElement (&id_element, 24); 
	pm = AddMultisetElement (&id_element, 25); 
	pm = AddMultisetElement (&id_element, 26); 
	pm = AddMultisetElement (&id_element, 27); 
	pm = AddMultisetElement (&id_element, 28); 
	pm = AddMultisetElement (&id_element, 29); 
	pm = AddMultisetElement (&id_element, 30); 
	pm = AddMultisetElement (&id_element, 31); 
	pm = AddMultisetElement (&id_element, 32); 
	pm = AddMultisetElement (&id_element, 33); 
	pm = AddMultisetElement (&id_element, 34); 
	pm = AddMultisetElement (&id_element, 35); 
	pm = AddMultisetElement (&id_element, 36); 
	pm = AddMultisetElement (&id_element, 37); 
	pm = AddMultisetElement (&id_element, 38); 
	pm = AddMultisetElement (&id_element, 39); 
	pm = AddMultisetElement (&id_element, 40); 
	pm = AddMultisetElement (&id_element, 41); 
	pm = AddMultisetElement (&id_element, 42); 
	pm = AddMultisetElement (&id_element, 43); 
	pm = AddMultisetElement (&id_element, 44); 
	pm = AddMultisetElement (&id_element, 45); 
	pm = AddMultisetElement (&id_element, 46); 
	pm = AddMultisetElement (&id_element, 47); 
	pm = AddMultisetElement (&id_element, 48); 
	pm = AddMultisetElement (&id_element, 49); 
	pm = AddMultisetElement (&id_element, 50); 
	pm = AddMultisetElement (&id_element, 51); 
	pm = AddMultisetElement (&id_element, 52); 
	pm = AddMultisetElement (&id_element, 53); 
	pm = AddMultisetElement (&id_element, 54); 
	pm = AddMultisetElement (&id_element, 55); 
	pm = AddMultisetElement (&id_element, 56); 
	pm = AddMultisetElement (&id_element, 57); 
	pm = AddMultisetElement (&id_element, 58); 
	pm = AddMultisetElement (&id_element, 59); 
	pm = AddMultisetElement (&id_element, 60); 
	pm = AddMultisetElement (&id_element, 61); 
	pm = AddMultisetElement (&id_element, 62); 
	pm = AddMultisetElement (&id_element, 63); 
	pm = AddMultisetElement (&id_element, 64); 
	pm = AddMultisetElement (&id_element, 65); 
	pm = AddMultisetElement (&id_element, 66); 
	pm = AddMultisetElement (&id_element, 67); 
	pm = AddMultisetElement (&id_element, 68); 
	pm = AddMultisetElement (&id_element, 69); 
	pm = AddMultisetElement (&id_element, 70); 
	pm = AddMultisetElement (&id_element, 71); 
	pm = AddMultisetElement (&id_element, 72); 
	pm = AddMultisetElement (&id_element, 73); 
	pm = AddMultisetElement (&id_element, 74); 
	pm = AddMultisetElement (&id_element, 75); 
	pm = AddMultisetElement (&id_element, 76); 
	pm = AddMultisetElement (&id_element, 77); 
	pm = AddMultisetElement (&id_element, 78); 
	pm = AddMultisetElement (&id_element, 79); 
	pm = AddMultisetElement (&id_element, 80); 
	pm = AddMultisetElement (&id_element, 81); 
	pm = AddMultisetElement (&id_element, 82); 
	pm = AddMultisetElement (&id_element, 83); 
	pm = AddMultisetElement (&id_element, 84); 
	pm = AddMultisetElement (&id_element, 85); 
	pm = AddMultisetElement (&id_element, 86); 
	pm = AddMultisetElement (&id_element, 87); 
	pm = AddMultisetElement (&id_element, 88); 
	pm = AddMultisetElement (&id_element, 89); 
	pm = AddMultisetElement (&id_element, 90); 
	pm = AddMultisetElement (&id_element, 91); 
	pm = AddMultisetElement (&id_element, 92); 
	pm = AddMultisetElement (&id_element, 93); 
	pm = AddMultisetElement (&id_element, 94); 
	pm = AddMultisetElement (&id_element, 95); 
	pm = AddMultisetElement (&id_element, 96); 
	pm = AddMultisetElement (&id_element, 97); 
	pm = AddMultisetElement (&id_element, 98); 
	pm = AddMultisetElement (&id_element, 99); 
	pm = AddMultisetElement (&id_element, 100); 
	pm = AddMultisetElement (&id_element, 101); 
	pm = AddMultisetElement (&id_element, 102); 
	pm = AddMultisetElement (&id_element, 103); 
	pm = AddMultisetElement (&id_element, 104); 
	pm = AddMultisetElement (&id_element, 105); 
	pm = AddMultisetElement (&id_element, 106); 
	pm = AddMultisetElement (&id_element, 107); 
	pm = AddMultisetElement (&id_element, 108); 
	pm = AddMultisetElement (&id_element, 109); 
	pm = AddMultisetElement (&id_element, 110); 
	pm = AddMultisetElement (&id_element, 111); 
	pm = AddMultisetElement (&id_element, 112); 
	pm = AddMultisetElement (&id_element, 113); 
	pm = AddMultisetElement (&id_element, 114); 
	pm = AddMultisetElement (&id_element, 115); 
	pm = AddMultisetElement (&id_element, 116); 
	pm = AddMultisetElement (&id_element, 117); 
	pm = AddMultisetElement (&id_element, 118); 
	pm = AddMultisetElement (&id_element, 119); 
	pm = AddMultisetElement (&id_element, 120); 
	pm = AddMultisetElement (&id_element, 121); 
	pm = AddMultisetElement (&id_element, 122); 
	pm = AddMultisetElement (&id_element, 123); 
	pm = AddMultisetElement (&id_element, 124); 
	pm = AddMultisetElement (&id_element, 125); 
	pm = AddMultisetElement (&id_element, 126); 
	pm = AddMultisetElement (&id_element, 127); 
	pm = AddMultisetElement (&id_element, 128); 
	pm = AddMultisetElement (&id_element, 129); 
	pm = AddMultisetElement (&id_element, 130); 
	pm = AddMultisetElement (&id_element, 131); 
	pm = AddMultisetElement (&id_element, 132); 
	pm = AddMultisetElement (&id_element, 133); 
	pm = AddMultisetElement (&id_element, 134); 
	pm = AddMultisetElement (&id_element, 135); 
	pm = AddMultisetElement (&id_element, 136); 
	pm = AddMultisetElement (&id_element, 137); 
	pm = AddMultisetElement (&id_element, 138); 
	pm = AddMultisetElement (&id_element, 139); 
	pm = AddMultisetElement (&id_element, 140); 
	pm = AddMultisetElement (&id_element, 141); 
	pm = AddMultisetElement (&id_element, 142); 
	pm = AddMultisetElement (&id_element, 143); 
	pm = AddMultisetElement (&id_element, 144); 
	pm = AddMultisetElement (&id_element, 145); 
	pm = AddMultisetElement (&id_element, 146); 
	pm = AddMultisetElement (&id_element, 147); 
	pm = AddMultisetElement (&id_element, 148); 
	pm = AddMultisetElement (&id_element, 149); 
	pm = AddMultisetElement (&id_element, 150); 
	pm = AddMultisetElement (&id_element, 151); 
	pm = AddMultisetElement (&id_element, 152); 
	pm = AddMultisetElement (&id_element, 153); 
	pm = AddMultisetElement (&id_element, 154); 
	pm = AddMultisetElement (&id_element, 155); 
	pm = AddMultisetElement (&id_element, 156); 
	pm = AddMultisetElement (&id_element, 157); 
	pm = AddMultisetElement (&id_element, 158); 
	pm = AddMultisetElement (&id_element, 159); 
	pm = AddMultisetElement (&id_element, 160); 
	pm = AddMultisetElement (&id_element, 161); 
	pm = AddMultisetElement (&id_element, 162); 
	pm = AddMultisetElement (&id_element, 163); 
	pm = AddMultisetElement (&id_element, 164); 
	pm = AddMultisetElement (&id_element, 165); 
	pm = AddMultisetElement (&id_element, 166); 
	pm = AddMultisetElement (&id_element, 167); 
	pm = AddMultisetElement (&id_element, 168); 
	pm = AddMultisetElement (&id_element, 169); 
	pm = AddMultisetElement (&id_element, 170); 
	pm = AddMultisetElement (&id_element, 171); 
	pm = AddMultisetElement (&id_element, 172); 
	pm = AddMultisetElement (&id_element, 173); 
	pm = AddMultisetElement (&id_element, 174); 
	pm = AddMultisetElement (&id_element, 175); 
	pm = AddMultisetElement (&id_element, 176); 
	pm = AddMultisetElement (&id_element, 177); 
	pm = AddMultisetElement (&id_element, 178); 
	pm = AddMultisetElement (&id_element, 179); 
	pm = AddMultisetElement (&id_element, 180); 
	pm = AddMultisetElement (&id_element, 181); 
	pm = AddMultisetElement (&id_element, 182); 
	pm = AddMultisetElement (&id_element, 183); 
	pm = AddMultisetElement (&id_element, 184); 
	pm = AddMultisetElement (&id_element, 185); 
	pm = AddMultisetElement (&id_element, 186); 
	pm = AddMultisetElement (&id_element, 187); 
	pm = AddMultisetElement (&id_element, 188); 
	pm = AddMultisetElement (&id_element, 189); 
	pm = AddMultisetElement (&id_element, 190); 
	pm = AddMultisetElement (&id_element, 191); 
	pm = AddMultisetElement (&id_element, 192); 
	pm = AddMultisetElement (&id_element, 193); 
	pm = AddMultisetElement (&id_element, 194); 
	pm = AddMultisetElement (&id_element, 195); 
	pm = AddMultisetElement (&id_element, 196); 
	pm = AddMultisetElement (&id_element, 197); 
	pm = AddMultisetElement (&id_element, 198); 
	pm = AddMultisetElement (&id_element, 199); 
	pm = AddMultisetElement (&id_element, 200); 
	pm = AddMultisetElement (&id_element, 21000); 
	pm = AddMultisetElement (&id_element, 21001); 
	pm = AddMultisetElement (&id_element, 21002); 
	pm = AddMultisetElement (&id_element, 21003); 
	pm = AddMultisetElement (&id_element, 21004); 
	pm = AddMultisetElement (&id_element, 21005); 
	pm = AddMultisetElement (&id_element, 21006); 
	pm = AddMultisetElement (&id_element, 21007); 
	pm = AddMultisetElement (&id_element, 21008); 
	pm = AddMultisetElement (&id_element, 21009); 
	pm = AddMultisetElement (&id_element, 21010); 
	pm = AddMultisetElement (&id_element, 21011); 
	pm = AddMultisetElement (&id_element, 21012); 
	pm = AddMultisetElement (&id_element, 21013); 
	pm = AddMultisetElement (&id_element, 21014); 
	pm = AddMultisetElement (&id_element, 21015); 
	pm = AddMultisetElement (&id_element, 21016); 
	pm = AddMultisetElement (&id_element, 21017); 
	pm = AddMultisetElement (&id_element, 21018); 
	pm = AddMultisetElement (&id_element, 21019); 
	pm = AddMultisetElement (&id_element, 21020); 
	pm = AddMultisetElement (&id_element, 21021); 
	pm = AddMultisetElement (&id_element, 21022); 
	pm = AddMultisetElement (&id_element, 21023); 
	pm = AddMultisetElement (&id_element, 21024); 
	pm = AddMultisetElement (&id_element, 21025); 
	pm = AddMultisetElement (&id_element, 21026); 
	pm = AddMultisetElement (&id_element, 21027); 
	pm = AddMultisetElement (&id_element, 21028); 
	pm = AddMultisetElement (&id_element, 21029); 
	pm = AddMultisetElement (&id_element, 21030); 
	pm = AddMultisetElement (&id_element, 21031); 
	pm = AddMultisetElement (&id_element, 21032); 
	pm = AddMultisetElement (&id_element, 21033); 
	pm = AddMultisetElement (&id_element, 21034); 
	pm = AddMultisetElement (&id_element, 21035); 
	pm = AddMultisetElement (&id_element, 21036); 
	pm = AddMultisetElement (&id_element, 21037); 
	pm = AddMultisetElement (&id_element, 21038); 
	pm = AddMultisetElement (&id_element, 21039); 
	pm = AddMultisetElement (&id_element, 21040); 
	pm = AddMultisetElement (&id_element, 21041); 
	pm = AddMultisetElement (&id_element, 21042); 
	pm = AddMultisetElement (&id_element, 21043); 
	pm = AddMultisetElement (&id_element, 21044); 
	pm = AddMultisetElement (&id_element, 21045); 
	pm = AddMultisetElement (&id_element, 21046); 
	pm = AddMultisetElement (&id_element, 21047); 
	pm = AddMultisetElement (&id_element, 21048); 
	pm = AddMultisetElement (&id_element, 21049); 
	pm = AddMultisetElement (&id_element, 21050); 
	pm = AddMultisetElement (&id_element, 21051); 
	pm = AddMultisetElement (&id_element, 21052); 
	pm = AddMultisetElement (&id_element, 21053); 
	pm = AddMultisetElement (&id_element, 21054); 
	pm = AddMultisetElement (&id_element, 21055); 
	pm = AddMultisetElement (&id_element, 21056); 
	pm = AddMultisetElement (&id_element, 21057); 
	pm = AddMultisetElement (&id_element, 21058); 
	pm = AddMultisetElement (&id_element, 21059); 
	pm = AddMultisetElement (&id_element, 21060); 
	pm = AddMultisetElement (&id_element, 21061); 
	pm = AddMultisetElement (&id_element, 21062); 
	pm = AddMultisetElement (&id_element, 21063); 
	pm = AddMultisetElement (&id_element, 21064); 
	pm = AddMultisetElement (&id_element, 21065); 
	pm = AddMultisetElement (&id_element, 21066); 
	pm = AddMultisetElement (&id_element, 21067); 
	pm = AddMultisetElement (&id_element, 21068); 
	pm = AddMultisetElement (&id_element, 21069); 
	pm = AddMultisetElement (&id_element, 21070); 
	pm = AddMultisetElement (&id_element, 21071); 
	pm = AddMultisetElement (&id_element, 21072); 
	pm = AddMultisetElement (&id_element, 21073); 
	pm = AddMultisetElement (&id_element, 21074); 
	pm = AddMultisetElement (&id_element, 21075); 
	pm = AddMultisetElement (&id_element, 21076); 
	pm = AddMultisetElement (&id_element, 21077); 
	pm = AddMultisetElement (&id_element, 21078); 
	pm = AddMultisetElement (&id_element, 21079); 
	pm = AddMultisetElement (&id_element, 21080); 
	pm = AddMultisetElement (&id_element, 21081); 
	pm = AddMultisetElement (&id_element, 21082); 
	pm = AddMultisetElement (&id_element, 21083); 
	pm = AddMultisetElement (&id_element, 21084); 
	pm = AddMultisetElement (&id_element, 21085); 
	pm = AddMultisetElement (&id_element, 21086); 
	pm = AddMultisetElement (&id_element, 21087); 
	pm = AddMultisetElement (&id_element, 21088); 
	pm = AddMultisetElement (&id_element, 21089); 
	pm = AddMultisetElement (&id_element, 21090); 
	pm = AddMultisetElement (&id_element, 21091); 
	pm = AddMultisetElement (&id_element, 21092); 
	pm = AddMultisetElement (&id_element, 21093); 
	pm = AddMultisetElement (&id_element, 21094); 
	pm = AddMultisetElement (&id_element, 21095); 
	pm = AddMultisetElement (&id_element, 21096); 
	pm = AddMultisetElement (&id_element, 21097); 
	pm = AddMultisetElement (&id_element, 21098); 
	pm = AddMultisetElement (&id_element, 21099); 
	pm = AddMultisetElement (&id_element, 21100); 
	pm = AddMultisetElement (&id_element, 21101); 
	pm = AddMultisetElement (&id_element, 21102); 
	pm = AddMultisetElement (&id_element, 21103); 
	pm = AddMultisetElement (&id_element, 21104); 
	pm = AddMultisetElement (&id_element, 21105); 
	pm = AddMultisetElement (&id_element, 21106); 
	pm = AddMultisetElement (&id_element, 21107); 
	pm = AddMultisetElement (&id_element, 21108); 
	pm = AddMultisetElement (&id_element, 21109); 
	pm = AddMultisetElement (&id_element, 21110); 
	pm = AddMultisetElement (&id_element, 21111); 
	pm = AddMultisetElement (&id_element, 21112); 
	pm = AddMultisetElement (&id_element, 21113); 
	pm = AddMultisetElement (&id_element, 21114); 
	pm = AddMultisetElement (&id_element, 21115); 
	pm = AddMultisetElement (&id_element, 21116); 
	pm = AddMultisetElement (&id_element, 21117); 
	pm = AddMultisetElement (&id_element, 21118); 
	pm = AddMultisetElement (&id_element, 21119); 
	pm = AddMultisetElement (&id_element, 21120); 
	pm = AddMultisetElement (&id_element, 21121); 
	pm = AddMultisetElement (&id_element, 21122); 
	pm = AddMultisetElement (&id_element, 21123); 
	pm = AddMultisetElement (&id_element, 21124); 
	pm = AddMultisetElement (&id_element, 21125); 
	pm = AddMultisetElement (&id_element, 21126); 
	pm = AddMultisetElement (&id_element, 21127); 
	pm = AddMultisetElement (&id_element, 21128); 
	pm = AddMultisetElement (&id_element, 21129); 
	pm = AddMultisetElement (&id_element, 21130); 
	pm = AddMultisetElement (&id_element, 21131); 
	pm = AddMultisetElement (&id_element, 21132); 
	pm = AddMultisetElement (&id_element, 21133); 
	pm = AddMultisetElement (&id_element, 21134); 
	pm = AddMultisetElement (&id_element, 21135); 
	pm = AddMultisetElement (&id_element, 21136); 
	pm = AddMultisetElement (&id_element, 21137); 
	pm = AddMultisetElement (&id_element, 21138); 
	pm = AddMultisetElement (&id_element, 21139); 
	pm = AddMultisetElement (&id_element, 21140); 
	pm = AddMultisetElement (&id_element, 21141); 
	pm = AddMultisetElement (&id_element, 21142); 
	pm = AddMultisetElement (&id_element, 21143); 
	pm = AddMultisetElement (&id_element, 21144); 
	pm = AddMultisetElement (&id_element, 21145); 
	pm = AddMultisetElement (&id_element, 21146); 
	pm = AddMultisetElement (&id_element, 21147); 
	pm = AddMultisetElement (&id_element, 21148); 
	pm = AddMultisetElement (&id_element, 21149); 
	pm = AddMultisetElement (&id_element, 21150); 
	pm = AddMultisetElement (&id_element, 21151); 
	pm = AddMultisetElement (&id_element, 21152); 
	pm = AddMultisetElement (&id_element, 21153); 
	pm = AddMultisetElement (&id_element, 21154); 
	pm = AddMultisetElement (&id_element, 21155); 
	pm = AddMultisetElement (&id_element, 21156); 
	pm = AddMultisetElement (&id_element, 21157); 
	pm = AddMultisetElement (&id_element, 21158); 
	pm = AddMultisetElement (&id_element, 21159); 
	pm = AddMultisetElement (&id_element, 21160); 
	pm = AddMultisetElement (&id_element, 21161); 
	pm = AddMultisetElement (&id_element, 21162); 
	pm = AddMultisetElement (&id_element, 21163); 
	pm = AddMultisetElement (&id_element, 21164); 
	pm = AddMultisetElement (&id_element, 21165); 
	pm = AddMultisetElement (&id_element, 21166); 
	pm = AddMultisetElement (&id_element, 21167); 
	pm = AddMultisetElement (&id_element, 21168); 
	pm = AddMultisetElement (&id_element, 21169); 
	pm = AddMultisetElement (&id_element, 21170); 
	pm = AddMultisetElement (&id_element, 21171); 
	pm = AddMultisetElement (&id_element, 21172); 
	pm = AddMultisetElement (&id_element, 21173); 
	pm = AddMultisetElement (&id_element, 21174); 
	pm = AddMultisetElement (&id_element, 21175); 
	pm = AddMultisetElement (&id_element, 21176); 
	pm = AddMultisetElement (&id_element, 21177); 
	pm = AddMultisetElement (&id_element, 21178); 
	pm = AddMultisetElement (&id_element, 21179); 
	pm = AddMultisetElement (&id_element, 21180); 
	pm = AddMultisetElement (&id_element, 21181); 
	pm = AddMultisetElement (&id_element, 21182); 
	pm = AddMultisetElement (&id_element, 21183); 
	pm = AddMultisetElement (&id_element, 21184); 
	pm = AddMultisetElement (&id_element, 21185); 
	pm = AddMultisetElement (&id_element, 21186); 
	pm = AddMultisetElement (&id_element, 21187); 
	pm = AddMultisetElement (&id_element, 21188); 
	pm = AddMultisetElement (&id_element, 21189); 
	pm = AddMultisetElement (&id_element, 21190); 
	pm = AddMultisetElement (&id_element, 21191); 
	pm = AddMultisetElement (&id_element, 21192); 
	pm = AddMultisetElement (&id_element, 21193); 
	pm = AddMultisetElement (&id_element, 21194); 
	pm = AddMultisetElement (&id_element, 21195); 
	pm = AddMultisetElement (&id_element, 21196); 
	pm = AddMultisetElement (&id_element, 21197); 
	pm = AddMultisetElement (&id_element, 21198); 
	pm = AddMultisetElement (&id_element, 21199); 
	pm = AddMultisetElement (&id_element, 21200); 
	pm = AddMultisetElement (&id_element, 21201); 
	pm = AddMultisetElement (&id_element, 21202); 
	pm = AddMultisetElement (&id_element, 21203); 
	pm = AddMultisetElement (&id_element, 21204); 
	pm = AddMultisetElement (&id_element, 21205); 
	pm = AddMultisetElement (&id_element, 21206); 
	pm = AddMultisetElement (&id_element, 21207); 
	pm = AddMultisetElement (&id_element, 21208); 
	pm = AddMultisetElement (&id_element, 21209); 
	pm = AddMultisetElement (&id_element, 21210); 
	pm = AddMultisetElement (&id_element, 21211); 
	pm = AddMultisetElement (&id_element, 21212); 
	pm = AddMultisetElement (&id_element, 21213); 
	pm = AddMultisetElement (&id_element, 21214); 
	pm = AddMultisetElement (&id_element, 21215); 
	pm = AddMultisetElement (&id_element, 21216); 
	pm = AddMultisetElement (&id_element, 21217); 
	pm = AddMultisetElement (&id_element, 21218); 
	pm = AddMultisetElement (&id_element, 21219); 
	pm = AddMultisetElement (&id_element, 21220); 
	pm = AddMultisetElement (&id_element, 21221); 
	pm = AddMultisetElement (&id_element, 21222); 
	pm = AddMultisetElement (&id_element, 21223); 
	pm = AddMultisetElement (&id_element, 21224); 
	pm = AddMultisetElement (&id_element, 21225); 
	pm = AddMultisetElement (&id_element, 21226); 
	pm = AddMultisetElement (&id_element, 21227); 
	pm = AddMultisetElement (&id_element, 21228); 
	pm = AddMultisetElement (&id_element, 21229); 
	pm = AddMultisetElement (&id_element, 21230); 
	pm = AddMultisetElement (&id_element, 21231); 
	pm = AddMultisetElement (&id_element, 21232); 
	pm = AddMultisetElement (&id_element, 21233); 
	pm = AddMultisetElement (&id_element, 21234); 
	pm = AddMultisetElement (&id_element, 21235); 
	pm = AddMultisetElement (&id_element, 21236); 
	pm = AddMultisetElement (&id_element, 21237); 
	pm = AddMultisetElement (&id_element, 21238); 
	pm = AddMultisetElement (&id_element, 21239); 
	pm = AddMultisetElement (&id_element, 21240); 
	pm = AddMultisetElement (&id_element, 21241); 
	pm = AddMultisetElement (&id_element, 21242); 
	pm = AddMultisetElement (&id_element, 21243); 
	pm = AddMultisetElement (&id_element, 21244); 
	pm = AddMultisetElement (&id_element, 21245); 
	pm = AddMultisetElement (&id_element, 21246); 
	pm = AddMultisetElement (&id_element, 21247); 
	pm = AddMultisetElement (&id_element, 21248); 
	pm = AddMultisetElement (&id_element, 21249); 
	pm = AddMultisetElement (&id_element, 21250); 
	pm = AddMultisetElement (&id_element, 21251); 
	pm = AddMultisetElement (&id_element, 21252); 
	pm = AddMultisetElement (&id_element, 21253); 
	pm = AddMultisetElement (&id_element, 21254); 
	pm = AddMultisetElement (&id_element, 21255); 
	pm = AddMultisetElement (&id_element, 21256); 
	pm = AddMultisetElement (&id_element, 21257); 
	pm = AddMultisetElement (&id_element, 21258); 
	pm = AddMultisetElement (&id_element, 21259); 
	pm = AddMultisetElement (&id_element, 21260); 
	pm = AddMultisetElement (&id_element, 21261); 
	pm = AddMultisetElement (&id_element, 21262); 
	pm = AddMultisetElement (&id_element, 21263); 
	pm = AddMultisetElement (&id_element, 21264); 
	pm = AddMultisetElement (&id_element, 21265); 
	pm = AddMultisetElement (&id_element, 21266); 
	pm = AddMultisetElement (&id_element, 21267); 
	pm = AddMultisetElement (&id_element, 21268); 
	pm = AddMultisetElement (&id_element, 21269); 
	pm = AddMultisetElement (&id_element, 21270); 
	pm = AddMultisetElement (&id_element, 21271); 
	pm = AddMultisetElement (&id_element, 21272); 
	pm = AddMultisetElement (&id_element, 21273); 
	pm = AddMultisetElement (&id_element, 21274); 
	pm = AddMultisetElement (&id_element, 21275); 
	pm = AddMultisetElement (&id_element, 21276); 
	pm = AddMultisetElement (&id_element, 21277); 
	pm = AddMultisetElement (&id_element, 21278); 
	pm = AddMultisetElement (&id_element, 21279); 
	pm = AddMultisetElement (&id_element, 21280); 
	pm = AddMultisetElement (&id_element, 21281); 
	pm = AddMultisetElement (&id_element, 21282); 
	pm = AddMultisetElement (&id_element, 21283); 
	pm = AddMultisetElement (&id_element, 21284); 
	pm = AddMultisetElement (&id_element, 21285); 
	pm = AddMultisetElement (&id_element, 21286); 
	pm = AddMultisetElement (&id_element, 21287); 
	pm = AddMultisetElement (&id_element, 21288); 
	pm = AddMultisetElement (&id_element, 21289); 
	pm = AddMultisetElement (&id_element, 21290); 
	pm = AddMultisetElement (&id_element, 21291); 
	pm = AddMultisetElement (&id_element, 21292); 
	pm = AddMultisetElement (&id_element, 21293); 
	pm = AddMultisetElement (&id_element, 21294); 
	pm = AddMultisetElement (&id_element, 21295); 
	pm = AddMultisetElement (&id_element, 21296); 
	pm = AddMultisetElement (&id_element, 21297); 
	pm = AddMultisetElement (&id_element, 21298); 
	pm = AddMultisetElement (&id_element, 21299); 
	pm = AddMultisetElement (&id_element, 21300); 
	pm = AddMultisetElement (&id_element, 21301); 
	pm = AddMultisetElement (&id_element, 21302); 
	pm = AddMultisetElement (&id_element, 21303); 
	pm = AddMultisetElement (&id_element, 21304); 
	pm = AddMultisetElement (&id_element, 21305); 
	pm = AddMultisetElement (&id_element, 21306); 
	pm = AddMultisetElement (&id_element, 21307); 
	pm = AddMultisetElement (&id_element, 21308); 
	pm = AddMultisetElement (&id_element, 21309); 
	pm = AddMultisetElement (&id_element, 21310); 
	pm = AddMultisetElement (&id_element, 21311); 
	pm = AddMultisetElement (&id_element, 21312); 
	pm = AddMultisetElement (&id_element, 21313); 
	pm = AddMultisetElement (&id_element, 21314); 
	pm = AddMultisetElement (&id_element, 21315); 
	pm = AddMultisetElement (&id_element, 21316); 
	pm = AddMultisetElement (&id_element, 21317); 
	pm = AddMultisetElement (&id_element, 21318); 
	pm = AddMultisetElement (&id_element, 21319); 
	pm = AddMultisetElement (&id_element, 21320); 
	pm = AddMultisetElement (&id_element, 21321); 
	pm = AddMultisetElement (&id_element, 21322); 
	pm = AddMultisetElement (&id_element, 21323); 
	pm = AddMultisetElement (&id_element, 21324); 
	pm = AddMultisetElement (&id_element, 21325); 
	pm = AddMultisetElement (&id_element, 21326); 
	pm = AddMultisetElement (&id_element, 21327); 
	pm = AddMultisetElement (&id_element, 21328); 
	pm = AddMultisetElement (&id_element, 21329); 
	pm = AddMultisetElement (&id_element, 21330); 
	pm = AddMultisetElement (&id_element, 21331); 
	pm = AddMultisetElement (&id_element, 21332); 
	pm = AddMultisetElement (&id_element, 21333); 
	pm = AddMultisetElement (&id_element, 21334); 
	pm = AddMultisetElement (&id_element, 21335); 
	pm = AddMultisetElement (&id_element, 21336); 
	pm = AddMultisetElement (&id_element, 21337); 
	pm = AddMultisetElement (&id_element, 21338); 
	pm = AddMultisetElement (&id_element, 21339); 
	pm = AddMultisetElement (&id_element, 21340); 
	pm = AddMultisetElement (&id_element, 21341); 
	pm = AddMultisetElement (&id_element, 21342); 
	pm = AddMultisetElement (&id_element, 21343); 
	pm = AddMultisetElement (&id_element, 21344); 
	pm = AddMultisetElement (&id_element, 21345); 
	pm = AddMultisetElement (&id_element, 21346); 
	pm = AddMultisetElement (&id_element, 21347); 
	pm = AddMultisetElement (&id_element, 21348); 
	pm = AddMultisetElement (&id_element, 21349); 
	pm = AddMultisetElement (&id_element, 21350); 
	pm = AddMultisetElement (&id_element, 21351); 
	pm = AddMultisetElement (&id_element, 21352); 
	pm = AddMultisetElement (&id_element, 21353); 
	pm = AddMultisetElement (&id_element, 21354); 
	pm = AddMultisetElement (&id_element, 21355); 
	pm = AddMultisetElement (&id_element, 21356); 
	pm = AddMultisetElement (&id_element, 21357); 
	pm = AddMultisetElement (&id_element, 21358); 
	pm = AddMultisetElement (&id_element, 21359); 
	pm = AddMultisetElement (&id_element, 21360); 
	pm = AddMultisetElement (&id_element, 21361); 
	pm = AddMultisetElement (&id_element, 21362); 
	pm = AddMultisetElement (&id_element, 21363); 
	pm = AddMultisetElement (&id_element, 21364); 
	pm = AddMultisetElement (&id_element, 21365); 
	pm = AddMultisetElement (&id_element, 21366); 
	pm = AddMultisetElement (&id_element, 21367); 
	pm = AddMultisetElement (&id_element, 21368); 
	pm = AddMultisetElement (&id_element, 21369); 
	pm = AddMultisetElement (&id_element, 21370); 
	pm = AddMultisetElement (&id_element, 21371); 
	pm = AddMultisetElement (&id_element, 21372); 
	pm = AddMultisetElement (&id_element, 21373); 
	pm = AddMultisetElement (&id_element, 21374); 
	pm = AddMultisetElement (&id_element, 21375); 
	pm = AddMultisetElement (&id_element, 21376); 
	pm = AddMultisetElement (&id_element, 21377); 
	pm = AddMultisetElement (&id_element, 21378); 
	pm = AddMultisetElement (&id_element, 21379); 
	pm = AddMultisetElement (&id_element, 21380); 
	pm = AddMultisetElement (&id_element, 21381); 
	pm = AddMultisetElement (&id_element, 21382); 
	pm = AddMultisetElement (&id_element, 21383); 
	pm = AddMultisetElement (&id_element, 21384); 
	pm = AddMultisetElement (&id_element, 21385); 
	pm = AddMultisetElement (&id_element, 21386); 
	pm = AddMultisetElement (&id_element, 21387); 
	pm = AddMultisetElement (&id_element, 21388); 
	pm = AddMultisetElement (&id_element, 21389); 
	pm = AddMultisetElement (&id_element, 21390); 
	pm = AddMultisetElement (&id_element, 21391); 
	pm = AddMultisetElement (&id_element, 21392); 
	pm = AddMultisetElement (&id_element, 21393); 
	pm = AddMultisetElement (&id_element, 21394); 
	pm = AddMultisetElement (&id_element, 21395); 
	pm = AddMultisetElement (&id_element, 21396); 
	pm = AddMultisetElement (&id_element, 21397); 
	pm = AddMultisetElement (&id_element, 21398); 
	pm = AddMultisetElement (&id_element, 21399); 
	pm = AddMultisetElement (&id_element, 21400); 
	pm = AddMultisetElement (&id_element, 21401); 
	pm = AddMultisetElement (&id_element, 21402); 
	pm = AddMultisetElement (&id_element, 21403); 
	pm = AddMultisetElement (&id_element, 21404); 
	pm = AddMultisetElement (&id_element, 21405); 
	pm = AddMultisetElement (&id_element, 21406); 
	pm = AddMultisetElement (&id_element, 21407); 
	pm = AddMultisetElement (&id_element, 21408); 
	pm = AddMultisetElement (&id_element, 21409); 
	pm = AddMultisetElement (&id_element, 21410); 
	pm = AddMultisetElement (&id_element, 21411); 
	pm = AddMultisetElement (&id_element, 21412); 
	pm = AddMultisetElement (&id_element, 21413); 
	pm = AddMultisetElement (&id_element, 21414); 
	pm = AddMultisetElement (&id_element, 21415); 
	pm = AddMultisetElement (&id_element, 21416); 
	pm = AddMultisetElement (&id_element, 21417); 
	pm = AddMultisetElement (&id_element, 21418); 
	pm = AddMultisetElement (&id_element, 21419); 
	pm = AddMultisetElement (&id_element, 21420); 
	pm = AddMultisetElement (&id_element, 21421); 
	pm = AddMultisetElement (&id_element, 21422); 
	pm = AddMultisetElement (&id_element, 21423); 
	pm = AddMultisetElement (&id_element, 21424); 
	pm = AddMultisetElement (&id_element, 21425); 
	pm = AddMultisetElement (&id_element, 21426); 
	pm = AddMultisetElement (&id_element, 21427); 
	pm = AddMultisetElement (&id_element, 21428); 
	pm = AddMultisetElement (&id_element, 21429); 
	pm = AddMultisetElement (&id_element, 21430); 
	pm = AddMultisetElement (&id_element, 21431); 
	pm = AddMultisetElement (&id_element, 21432); 
	pm = AddMultisetElement (&id_element, 21433); 
	pm = AddMultisetElement (&id_element, 21434); 
	pm = AddMultisetElement (&id_element, 21435); 
	pm = AddMultisetElement (&id_element, 21436); 
	pm = AddMultisetElement (&id_element, 21437); 
	pm = AddMultisetElement (&id_element, 21438); 
	pm = AddMultisetElement (&id_element, 21439); 
	pm = AddMultisetElement (&id_element, 21440); 
	pm = AddMultisetElement (&id_element, 21441); 
	pm = AddMultisetElement (&id_element, 21442); 
	pm = AddMultisetElement (&id_element, 21443); 
	pm = AddMultisetElement (&id_element, 21444); 
	pm = AddMultisetElement (&id_element, 21445); 
	pm = AddMultisetElement (&id_element, 21446); 
	pm = AddMultisetElement (&id_element, 21447); 
	pm = AddMultisetElement (&id_element, 21448); 
	pm = AddMultisetElement (&id_element, 21449); 
	pm = AddMultisetElement (&id_element, 21450); 
	pm = AddMultisetElement (&id_element, 21451); 
	pm = AddMultisetElement (&id_element, 21452); 
	pm = AddMultisetElement (&id_element, 21453); 
	pm = AddMultisetElement (&id_element, 21454); 
	pm = AddMultisetElement (&id_element, 21455); 
	pm = AddMultisetElement (&id_element, 21456); 
	pm = AddMultisetElement (&id_element, 21457); 
	pm = AddMultisetElement (&id_element, 21458); 
	pm = AddMultisetElement (&id_element, 21459); 
	pm = AddMultisetElement (&id_element, 21460); 
	pm = AddMultisetElement (&id_element, 21461); 
	pm = AddMultisetElement (&id_element, 21462); 
	pm = AddMultisetElement (&id_element, 21463); 
	pm = AddMultisetElement (&id_element, 21464); 
	pm = AddMultisetElement (&id_element, 21465); 
	pm = AddMultisetElement (&id_element, 21466); 
	pm = AddMultisetElement (&id_element, 21467); 
	pm = AddMultisetElement (&id_element, 21468); 
	pm = AddMultisetElement (&id_element, 21469); 
	pm = AddMultisetElement (&id_element, 21470); 
	pm = AddMultisetElement (&id_element, 21471); 
	pm = AddMultisetElement (&id_element, 21472); 
	pm = AddMultisetElement (&id_element, 21473); 
	pm = AddMultisetElement (&id_element, 21474); 
	pm = AddMultisetElement (&id_element, 21475); 
	pm = AddMultisetElement (&id_element, 21476); 
	pm = AddMultisetElement (&id_element, 21477); 
	pm = AddMultisetElement (&id_element, 21478); 
	pm = AddMultisetElement (&id_element, 21479); 
	pm = AddMultisetElement (&id_element, 21480); 
	pm = AddMultisetElement (&id_element, 21481); 
	pm = AddMultisetElement (&id_element, 21482); 
	pm = AddMultisetElement (&id_element, 21483); 
	pm = AddMultisetElement (&id_element, 21484); 
	pm = AddMultisetElement (&id_element, 21485); 
	pm = AddMultisetElement (&id_element, 21486); 
	pm = AddMultisetElement (&id_element, 21487); 
	pm = AddMultisetElement (&id_element, 21488); 
	pm = AddMultisetElement (&id_element, 21489); 
	pm = AddMultisetElement (&id_element, 21490); 
	pm = AddMultisetElement (&id_element, 21491); 
	pm = AddMultisetElement (&id_element, 21492); 
	pm = AddMultisetElement (&id_element, 21493); 
	pm = AddMultisetElement (&id_element, 21494); 
	pm = AddMultisetElement (&id_element, 21495); 
	pm = AddMultisetElement (&id_element, 21496); 
	pm = AddMultisetElement (&id_element, 21497); 
	pm = AddMultisetElement (&id_element, 21498); 
	pm = AddMultisetElement (&id_element, 21499); 
	pm = AddMultisetElement (&id_element, 21500); 
	pm = AddMultisetElement (&id_element, 21501); 
	pm = AddMultisetElement (&id_element, 21502); 
	pm = AddMultisetElement (&id_element, 21503); 
	pm = AddMultisetElement (&id_element, 21504); 
	pm = AddMultisetElement (&id_element, 21505); 
	pm = AddMultisetElement (&id_element, 21506); 
	pm = AddMultisetElement (&id_element, 21507); 
	pm = AddMultisetElement (&id_element, 21508); 
	pm = AddMultisetElement (&id_element, 21509); 
	pm = AddMultisetElement (&id_element, 21510); 
	pm = AddMultisetElement (&id_element, 21511); 
	pm = AddMultisetElement (&id_element, 21512); 
	pm = AddMultisetElement (&id_element, 21513); 
	pm = AddMultisetElement (&id_element, 21514); 
	pm = AddMultisetElement (&id_element, 21515); 
	pm = AddMultisetElement (&id_element, 21516); 
	pm = AddMultisetElement (&id_element, 21517); 
	pm = AddMultisetElement (&id_element, 21518); 
	pm = AddMultisetElement (&id_element, 21519); 
	pm = AddMultisetElement (&id_element, 21520); 
	pm = AddMultisetElement (&id_element, 21521); 
	pm = AddMultisetElement (&id_element, 21522); 
	pm = AddMultisetElement (&id_element, 21523); 
	pm = AddMultisetElement (&id_element, 21524); 
	pm = AddMultisetElement (&id_element, 21525); 
	pm = AddMultisetElement (&id_element, 21526); 
	pm = AddMultisetElement (&id_element, 21527); 
	pm = AddMultisetElement (&id_element, 21528); 
	pm = AddMultisetElement (&id_element, 21529); 
	pm = AddMultisetElement (&id_element, 21530); 
	pm = AddMultisetElement (&id_element, 21531); 
	pm = AddMultisetElement (&id_element, 21532); 
	pm = AddMultisetElement (&id_element, 21533); 
	pm = AddMultisetElement (&id_element, 21534); 
	pm = AddMultisetElement (&id_element, 21535); 
	pm = AddMultisetElement (&id_element, 21536); 
	pm = AddMultisetElement (&id_element, 21537); 
	pm = AddMultisetElement (&id_element, 21538); 
	pm = AddMultisetElement (&id_element, 21539); 
	pm = AddMultisetElement (&id_element, 21540); 
	pm = AddMultisetElement (&id_element, 21541); 
	pm = AddMultisetElement (&id_element, 21542); 
	pm = AddMultisetElement (&id_element, 21543); 
	pm = AddMultisetElement (&id_element, 21544); 
	pm = AddMultisetElement (&id_element, 21545); 
	pm = AddMultisetElement (&id_element, 21546); 
	pm = AddMultisetElement (&id_element, 21547); 
	pm = AddMultisetElement (&id_element, 21548); 
	pm = AddMultisetElement (&id_element, 21549); 
	pm = AddMultisetElement (&id_element, 21550); 
	pm = AddMultisetElement (&id_element, 21551); 
	pm = AddMultisetElement (&id_element, 21552); 
	pm = AddMultisetElement (&id_element, 21553); 
	pm = AddMultisetElement (&id_element, 21554); 
	pm = AddMultisetElement (&id_element, 21555); 
	pm = AddMultisetElement (&id_element, 21556); 
	pm = AddMultisetElement (&id_element, 21557); 
	pm = AddMultisetElement (&id_element, 21558); 
	pm = AddMultisetElement (&id_element, 21559); 
	pm = AddMultisetElement (&id_element, 21560); 
	pm = AddMultisetElement (&id_element, 21561); 
	pm = AddMultisetElement (&id_element, 21562); 
	pm = AddMultisetElement (&id_element, 21563); 
	pm = AddMultisetElement (&id_element, 21564); 
	pm = AddMultisetElement (&id_element, 21565); 
	pm = AddMultisetElement (&id_element, 21566); 
	pm = AddMultisetElement (&id_element, 21567); 
	pm = AddMultisetElement (&id_element, 21568); 
	pm = AddMultisetElement (&id_element, 21569); 
	pm = AddMultisetElement (&id_element, 21570); 
	pm = AddMultisetElement (&id_element, 21571); 
	pm = AddMultisetElement (&id_element, 21572); 
	pm = AddMultisetElement (&id_element, 21573); 
	pm = AddMultisetElement (&id_element, 21574); 
	pm = AddMultisetElement (&id_element, 21575); 
	pm = AddMultisetElement (&id_element, 21576); 
	pm = AddMultisetElement (&id_element, 21577); 
	pm = AddMultisetElement (&id_element, 21578); 
	pm = AddMultisetElement (&id_element, 21579); 
	pm = AddMultisetElement (&id_element, 21580); 
	pm = AddMultisetElement (&id_element, 21581); 
	pm = AddMultisetElement (&id_element, 21582); 
	pm = AddMultisetElement (&id_element, 21583); 
	pm = AddMultisetElement (&id_element, 21584); 
	pm = AddMultisetElement (&id_element, 21585); 
	pm = AddMultisetElement (&id_element, 21586); 
	pm = AddMultisetElement (&id_element, 21587); 
	pm = AddMultisetElement (&id_element, 21588); 
	pm = AddMultisetElement (&id_element, 21589); 
	pm = AddMultisetElement (&id_element, 21590); 
	pm = AddMultisetElement (&id_element, 21591); 
	pm = AddMultisetElement (&id_element, 21592); 
	pm = AddMultisetElement (&id_element, 21593); 
	pm = AddMultisetElement (&id_element, 21594); 
	pm = AddMultisetElement (&id_element, 21595); 
	pm = AddMultisetElement (&id_element, 21596); 
	pm = AddMultisetElement (&id_element, 21597); 
	pm = AddMultisetElement (&id_element, 21598); 
	pm = AddMultisetElement (&id_element, 21599); 
	pm = AddMultisetElement (&id_element, 21600); 
	pm = AddMultisetElement (&id_element, 21601); 
	pm = AddMultisetElement (&id_element, 21602); 
	pm = AddMultisetElement (&id_element, 21603); 
	pm = AddMultisetElement (&id_element, 21604); 
	pm = AddMultisetElement (&id_element, 21605); 
	pm = AddMultisetElement (&id_element, 21606); 
	pm = AddMultisetElement (&id_element, 21607); 
	pm = AddMultisetElement (&id_element, 21608); 
	pm = AddMultisetElement (&id_element, 21609); 
	pm = AddMultisetElement (&id_element, 21610); 
	pm = AddMultisetElement (&id_element, 21611); 
	pm = AddMultisetElement (&id_element, 21612); 
	pm = AddMultisetElement (&id_element, 21613); 
	pm = AddMultisetElement (&id_element, 21614); 
	pm = AddMultisetElement (&id_element, 21615); 
	pm = AddMultisetElement (&id_element, 21616); 
	pm = AddMultisetElement (&id_element, 21617); 
	pm = AddMultisetElement (&id_element, 21618); 
	pm = AddMultisetElement (&id_element, 21619); 
	pm = AddMultisetElement (&id_element, 21620); 
	pm = AddMultisetElement (&id_element, 21621); 
	pm = AddMultisetElement (&id_element, 21622); 
	pm = AddMultisetElement (&id_element, 21623); 
	pm = AddMultisetElement (&id_element, 21624); 
	pm = AddMultisetElement (&id_element, 21625); 
	pm = AddMultisetElement (&id_element, 21626); 
	pm = AddMultisetElement (&id_element, 21627); 
	pm = AddMultisetElement (&id_element, 21628); 
	pm = AddMultisetElement (&id_element, 21629); 
	pm = AddMultisetElement (&id_element, 21630); 
	pm = AddMultisetElement (&id_element, 21631); 
	pm = AddMultisetElement (&id_element, 21632); 
	pm = AddMultisetElement (&id_element, 21633); 
	pm = AddMultisetElement (&id_element, 21634); 
	pm = AddMultisetElement (&id_element, 21635); 
	pm = AddMultisetElement (&id_element, 21636); 
	pm = AddMultisetElement (&id_element, 21637); 
	pm = AddMultisetElement (&id_element, 21638); 
	pm = AddMultisetElement (&id_element, 21639); 
	pm = AddMultisetElement (&id_element, 21640); 
	pm = AddMultisetElement (&id_element, 21641); 
	pm = AddMultisetElement (&id_element, 21642); 
	pm = AddMultisetElement (&id_element, 21643); 
	pm = AddMultisetElement (&id_element, 21644); 
	pm = AddMultisetElement (&id_element, 21645); 
	pm = AddMultisetElement (&id_element, 21646); 
	pm = AddMultisetElement (&id_element, 21647); 
	pm = AddMultisetElement (&id_element, 21648); 
	pm = AddMultisetElement (&id_element, 21649); 
	pm = AddMultisetElement (&id_element, 21650); 
	pm = AddMultisetElement (&id_element, 21651); 
	pm = AddMultisetElement (&id_element, 21652); 
	pm = AddMultisetElement (&id_element, 21653); 
	pm = AddMultisetElement (&id_element, 21654); 
	pm = AddMultisetElement (&id_element, 21655); 
	pm = AddMultisetElement (&id_element, 21656); 
	pm = AddMultisetElement (&id_element, 21657); 
	pm = AddMultisetElement (&id_element, 21658); 
	pm = AddMultisetElement (&id_element, 21659); 
	pm = AddMultisetElement (&id_element, 21660); 
	pm = AddMultisetElement (&id_element, 21661); 
	pm = AddMultisetElement (&id_element, 21662); 
	pm = AddMultisetElement (&id_element, 21663); 
	pm = AddMultisetElement (&id_element, 21664); 
	pm = AddMultisetElement (&id_element, 21665); 
	pm = AddMultisetElement (&id_element, 21666); 
	pm = AddMultisetElement (&id_element, 21667); 
	pm = AddMultisetElement (&id_element, 21668); 
	pm = AddMultisetElement (&id_element, 21669); 
	pm = AddMultisetElement (&id_element, 21670); 
	pm = AddMultisetElement (&id_element, 21671); 
	pm = AddMultisetElement (&id_element, 21672); 
	pm = AddMultisetElement (&id_element, 21673); 
	pm = AddMultisetElement (&id_element, 21674); 
	pm = AddMultisetElement (&id_element, 21675); 
	pm = AddMultisetElement (&id_element, 21676); 
	pm = AddMultisetElement (&id_element, 21677); 
	pm = AddMultisetElement (&id_element, 21678); 
	pm = AddMultisetElement (&id_element, 21679); 
	pm = AddMultisetElement (&id_element, 21680); 
	pm = AddMultisetElement (&id_element, 21681); 
	pm = AddMultisetElement (&id_element, 21682); 
	pm = AddMultisetElement (&id_element, 21683); 
	pm = AddMultisetElement (&id_element, 21684); 
	pm = AddMultisetElement (&id_element, 21685); 
	pm = AddMultisetElement (&id_element, 21686); 
	pm = AddMultisetElement (&id_element, 21687); 
	pm = AddMultisetElement (&id_element, 21688); 
	pm = AddMultisetElement (&id_element, 21689); 
	pm = AddMultisetElement (&id_element, 21690); 
	pm = AddMultisetElement (&id_element, 21691); 
	pm = AddMultisetElement (&id_element, 21692); 
	pm = AddMultisetElement (&id_element, 21693); 
	pm = AddMultisetElement (&id_element, 21694); 
	pm = AddMultisetElement (&id_element, 21695); 
	pm = AddMultisetElement (&id_element, 21696); 
	pm = AddMultisetElement (&id_element, 21697); 
	pm = AddMultisetElement (&id_element, 21698); 
	pm = AddMultisetElement (&id_element, 21699); 
	pm = AddMultisetElement (&id_element, 21700); 
	pm = AddMultisetElement (&id_element, 21701); 
	pm = AddMultisetElement (&id_element, 21702); 
	pm = AddMultisetElement (&id_element, 21703); 
	pm = AddMultisetElement (&id_element, 21704); 
	pm = AddMultisetElement (&id_element, 21705); 
	pm = AddMultisetElement (&id_element, 21706); 
	pm = AddMultisetElement (&id_element, 21707); 
	pm = AddMultisetElement (&id_element, 21708); 
	pm = AddMultisetElement (&id_element, 21709); 
	pm = AddMultisetElement (&id_element, 21710); 
	pm = AddMultisetElement (&id_element, 21711); 
	pm = AddMultisetElement (&id_element, 21712); 
	pm = AddMultisetElement (&id_element, 21713); 
	pm = AddMultisetElement (&id_element, 21714); 
	pm = AddMultisetElement (&id_element, 21715); 
	pm = AddMultisetElement (&id_element, 21716); 
	pm = AddMultisetElement (&id_element, 21717); 
	pm = AddMultisetElement (&id_element, 21718); 
	pm = AddMultisetElement (&id_element, 21719); 
	pm = AddMultisetElement (&id_element, 21720); 
	pm = AddMultisetElement (&id_element, 21721); 
	pm = AddMultisetElement (&id_element, 21722); 
	pm = AddMultisetElement (&id_element, 21723); 
	pm = AddMultisetElement (&id_element, 21724); 
	pm = AddMultisetElement (&id_element, 21725); 
	pm = AddMultisetElement (&id_element, 21726); 
	pm = AddMultisetElement (&id_element, 21727); 
	pm = AddMultisetElement (&id_element, 21728); 
	pm = AddMultisetElement (&id_element, 21729); 
	pm = AddMultisetElement (&id_element, 21730); 
	pm = AddMultisetElement (&id_element, 21731); 
	pm = AddMultisetElement (&id_element, 21732); 
	pm = AddMultisetElement (&id_element, 21733); 
	pm = AddMultisetElement (&id_element, 21734); 
	pm = AddMultisetElement (&id_element, 21735); 
	pm = AddMultisetElement (&id_element, 21736); 
	pm = AddMultisetElement (&id_element, 21737); 
	pm = AddMultisetElement (&id_element, 21738); 
	pm = AddMultisetElement (&id_element, 21739); 
	pm = AddMultisetElement (&id_element, 21740); 
	pm = AddMultisetElement (&id_element, 21741); 
	pm = AddMultisetElement (&id_element, 21742); 
	pm = AddMultisetElement (&id_element, 21743); 
	pm = AddMultisetElement (&id_element, 21744); 
	pm = AddMultisetElement (&id_element, 21745); 
	pm = AddMultisetElement (&id_element, 21746); 
	pm = AddMultisetElement (&id_element, 21747); 
	pm = AddMultisetElement (&id_element, 21748); 
	pm = AddMultisetElement (&id_element, 21749); 
	pm = AddMultisetElement (&id_element, 21750); 
	pm = AddMultisetElement (&id_element, 21751); 
	pm = AddMultisetElement (&id_element, 21752); 
	pm = AddMultisetElement (&id_element, 21753); 
	pm = AddMultisetElement (&id_element, 21754); 
	pm = AddMultisetElement (&id_element, 21755); 
	pm = AddMultisetElement (&id_element, 21756); 
	pm = AddMultisetElement (&id_element, 21757); 
	pm = AddMultisetElement (&id_element, 21758); 
	pm = AddMultisetElement (&id_element, 21759); 
	pm = AddMultisetElement (&id_element, 21760); 
	pm = AddMultisetElement (&id_element, 21761); 
	pm = AddMultisetElement (&id_element, 21762); 
	pm = AddMultisetElement (&id_element, 21763); 
	pm = AddMultisetElement (&id_element, 21764); 
	pm = AddMultisetElement (&id_element, 21765); 
	pm = AddMultisetElement (&id_element, 21766); 
	pm = AddMultisetElement (&id_element, 21767); 
	pm = AddMultisetElement (&id_element, 21768); 
	pm = AddMultisetElement (&id_element, 21769); 
	pm = AddMultisetElement (&id_element, 21770); 
	pm = AddMultisetElement (&id_element, 21771); 
	pm = AddMultisetElement (&id_element, 21772); 
	pm = AddMultisetElement (&id_element, 21773); 
	pm = AddMultisetElement (&id_element, 21774); 
	pm = AddMultisetElement (&id_element, 21775); 
	pm = AddMultisetElement (&id_element, 21776); 
	pm = AddMultisetElement (&id_element, 21777); 
	pm = AddMultisetElement (&id_element, 21778); 
	pm = AddMultisetElement (&id_element, 21779); 
	pm = AddMultisetElement (&id_element, 21780); 
	pm = AddMultisetElement (&id_element, 21781); 
	pm = AddMultisetElement (&id_element, 21782); 
	pm = AddMultisetElement (&id_element, 21783); 
	pm = AddMultisetElement (&id_element, 21784); 
	pm = AddMultisetElement (&id_element, 21785); 
	pm = AddMultisetElement (&id_element, 21786); 
	pm = AddMultisetElement (&id_element, 21787); 
	pm = AddMultisetElement (&id_element, 21788); 
	pm = AddMultisetElement (&id_element, 21789); 
	pm = AddMultisetElement (&id_element, 21790); 
	pm = AddMultisetElement (&id_element, 21791); 
	pm = AddMultisetElement (&id_element, 21792); 
	pm = AddMultisetElement (&id_element, 21793); 
	pm = AddMultisetElement (&id_element, 21794); 
	pm = AddMultisetElement (&id_element, 21795); 
	pm = AddMultisetElement (&id_element, 21796); 
	pm = AddMultisetElement (&id_element, 21797); 
	pm = AddMultisetElement (&id_element, 21798); 
	pm = AddMultisetElement (&id_element, 21799); 
	pm = AddMultisetElement (&id_element, 21800); 
	pm = AddMultisetElement (&id_element, 21801); 
	pm = AddMultisetElement (&id_element, 21802); 
	pm = AddMultisetElement (&id_element, 21803); 
	pm = AddMultisetElement (&id_element, 21804); 
	pm = AddMultisetElement (&id_element, 21805); 
	pm = AddMultisetElement (&id_element, 21806); 
	pm = AddMultisetElement (&id_element, 21807); 
	pm = AddMultisetElement (&id_element, 21808); 
	pm = AddMultisetElement (&id_element, 21809); 
	pm = AddMultisetElement (&id_element, 21810); 
	pm = AddMultisetElement (&id_element, 21811); 
	pm = AddMultisetElement (&id_element, 21812); 
	pm = AddMultisetElement (&id_element, 21813); 
	pm = AddMultisetElement (&id_element, 21814); 
	pm = AddMultisetElement (&id_element, 21815); 
	pm = AddMultisetElement (&id_element, 21816); 
	pm = AddMultisetElement (&id_element, 21817); 
	pm = AddMultisetElement (&id_element, 21818); 
	pm = AddMultisetElement (&id_element, 21819); 
	pm = AddMultisetElement (&id_element, 21820); 
	pm = AddMultisetElement (&id_element, 21821); 
	pm = AddMultisetElement (&id_element, 21822); 
	pm = AddMultisetElement (&id_element, 21823); 
	pm = AddMultisetElement (&id_element, 21824); 
	pm = AddMultisetElement (&id_element, 21825); 
	pm = AddMultisetElement (&id_element, 21826); 
	pm = AddMultisetElement (&id_element, 21827); 
	pm = AddMultisetElement (&id_element, 21828); 
	pm = AddMultisetElement (&id_element, 21829); 
	pm = AddMultisetElement (&id_element, 21830); 
	pm = AddMultisetElement (&id_element, 21831); 
	pm = AddMultisetElement (&id_element, 21832); 
	pm = AddMultisetElement (&id_element, 21833); 
	pm = AddMultisetElement (&id_element, 21834); 
	pm = AddMultisetElement (&id_element, 21835); 
	pm = AddMultisetElement (&id_element, 21836); 
	pm = AddMultisetElement (&id_element, 21837); 
	pm = AddMultisetElement (&id_element, 21838); 
	pm = AddMultisetElement (&id_element, 21839); 
	pm = AddMultisetElement (&id_element, 21840); 
	pm = AddMultisetElement (&id_element, 21841); 
	pm = AddMultisetElement (&id_element, 21842); 
	pm = AddMultisetElement (&id_element, 21843); 
	pm = AddMultisetElement (&id_element, 21844); 
	pm = AddMultisetElement (&id_element, 21845); 
	pm = AddMultisetElement (&id_element, 21846); 
	pm = AddMultisetElement (&id_element, 21847); 
	pm = AddMultisetElement (&id_element, 21848); 
	pm = AddMultisetElement (&id_element, 21849); 
	pm = AddMultisetElement (&id_element, 21850); 
	pm = AddMultisetElement (&id_element, 21851); 
	pm = AddMultisetElement (&id_element, 21852); 
	pm = AddMultisetElement (&id_element, 21853); 
	pm = AddMultisetElement (&id_element, 21854); 
	pm = AddMultisetElement (&id_element, 21855); 
	pm = AddMultisetElement (&id_element, 21856); 
	pm = AddMultisetElement (&id_element, 21857); 
	pm = AddMultisetElement (&id_element, 21858); 
	pm = AddMultisetElement (&id_element, 21859); 
	pm = AddMultisetElement (&id_element, 21860); 
	pm = AddMultisetElement (&id_element, 21861); 
	pm = AddMultisetElement (&id_element, 21862); 
	pm = AddMultisetElement (&id_element, 21863); 
	pm = AddMultisetElement (&id_element, 21864); 
	pm = AddMultisetElement (&id_element, 21865); 
	pm = AddMultisetElement (&id_element, 21866); 
	pm = AddMultisetElement (&id_element, 21867); 
	pm = AddMultisetElement (&id_element, 21868); 
	pm = AddMultisetElement (&id_element, 21869); 
	pm = AddMultisetElement (&id_element, 21870); 
	pm = AddMultisetElement (&id_element, 21871); 
	pm = AddMultisetElement (&id_element, 21872); 
	pm = AddMultisetElement (&id_element, 21873); 
	pm = AddMultisetElement (&id_element, 21874); 
	pm = AddMultisetElement (&id_element, 21875); 
	pm = AddMultisetElement (&id_element, 21876); 
	pm = AddMultisetElement (&id_element, 21877); 
	pm = AddMultisetElement (&id_element, 21878); 
	pm = AddMultisetElement (&id_element, 21879); 
	pm = AddMultisetElement (&id_element, 21880); 
	pm = AddMultisetElement (&id_element, 21881); 
	pm = AddMultisetElement (&id_element, 21882); 
	pm = AddMultisetElement (&id_element, 21883); 
	pm = AddMultisetElement (&id_element, 21884); 
	pm = AddMultisetElement (&id_element, 21885); 
	pm = AddMultisetElement (&id_element, 21886); 
	pm = AddMultisetElement (&id_element, 21887); 
	pm = AddMultisetElement (&id_element, 21888); 
	pm = AddMultisetElement (&id_element, 21889); 
	pm = AddMultisetElement (&id_element, 21890); 
	pm = AddMultisetElement (&id_element, 21891); 
	pm = AddMultisetElement (&id_element, 21892); 
	pm = AddMultisetElement (&id_element, 21893); 
	pm = AddMultisetElement (&id_element, 21894); 
	pm = AddMultisetElement (&id_element, 21895); 
	pm = AddMultisetElement (&id_element, 21896); 
	pm = AddMultisetElement (&id_element, 21897); 
	pm = AddMultisetElement (&id_element, 21898); 
	pm = AddMultisetElement (&id_element, 21899); 
	pm = AddMultisetElement (&id_element, 21900); 
	pm = AddMultisetElement (&id_element, 21901); 
	pm = AddMultisetElement (&id_element, 21902); 
	pm = AddMultisetElement (&id_element, 21903); 
	pm = AddMultisetElement (&id_element, 21904); 
	pm = AddMultisetElement (&id_element, 21905); 
	pm = AddMultisetElement (&id_element, 21906); 
	pm = AddMultisetElement (&id_element, 21907); 
	pm = AddMultisetElement (&id_element, 21908); 
	pm = AddMultisetElement (&id_element, 21909); 
	pm = AddMultisetElement (&id_element, 21910); 
	pm = AddMultisetElement (&id_element, 21911); 
	pm = AddMultisetElement (&id_element, 21912); 
	pm = AddMultisetElement (&id_element, 21913); 
	pm = AddMultisetElement (&id_element, 21914); 
	pm = AddMultisetElement (&id_element, 21915); 
	pm = AddMultisetElement (&id_element, 21916); 
	pm = AddMultisetElement (&id_element, 21917); 
	pm = AddMultisetElement (&id_element, 21918); 
	pm = AddMultisetElement (&id_element, 21919); 
	pm = AddMultisetElement (&id_element, 21920); 
	pm = AddMultisetElement (&id_element, 21921); 
	pm = AddMultisetElement (&id_element, 21922); 
	pm = AddMultisetElement (&id_element, 21923); 
	pm = AddMultisetElement (&id_element, 21924); 
	pm = AddMultisetElement (&id_element, 21925); 
	pm = AddMultisetElement (&id_element, 21926); 
	pm = AddMultisetElement (&id_element, 21927); 
	pm = AddMultisetElement (&id_element, 21928); 
	pm = AddMultisetElement (&id_element, 21929); 
	pm = AddMultisetElement (&id_element, 21930); 
	pm = AddMultisetElement (&id_element, 21931); 
	pm = AddMultisetElement (&id_element, 21932); 
	pm = AddMultisetElement (&id_element, 21933); 
	pm = AddMultisetElement (&id_element, 21934); 
	pm = AddMultisetElement (&id_element, 21935); 
	pm = AddMultisetElement (&id_element, 21936); 
	pm = AddMultisetElement (&id_element, 21937); 
	pm = AddMultisetElement (&id_element, 21938); 
	pm = AddMultisetElement (&id_element, 21939); 
	pm = AddMultisetElement (&id_element, 21940); 
	pm = AddMultisetElement (&id_element, 21941); 
	pm = AddMultisetElement (&id_element, 21942); 
	pm = AddMultisetElement (&id_element, 21943); 
	pm = AddMultisetElement (&id_element, 21944); 
	pm = AddMultisetElement (&id_element, 21945); 
	pm = AddMultisetElement (&id_element, 21946); 
	pm = AddMultisetElement (&id_element, 21947); 
	pm = AddMultisetElement (&id_element, 21948); 
	pm = AddMultisetElement (&id_element, 21949); 
	pm = AddMultisetElement (&id_element, 21950); 
	pm = AddMultisetElement (&id_element, 21951); 
	pm = AddMultisetElement (&id_element, 21952); 
	pm = AddMultisetElement (&id_element, 21953); 
	pm = AddMultisetElement (&id_element, 21954); 
	pm = AddMultisetElement (&id_element, 21955); 
	pm = AddMultisetElement (&id_element, 21956); 
	pm = AddMultisetElement (&id_element, 21957); 
	pm = AddMultisetElement (&id_element, 21958); 
	pm = AddMultisetElement (&id_element, 21959); 
	pm = AddMultisetElement (&id_element, 21960); 
	pm = AddMultisetElement (&id_element, 21961); 
	pm = AddMultisetElement (&id_element, 21962); 
	pm = AddMultisetElement (&id_element, 21963); 
	pm = AddMultisetElement (&id_element, 21964); 
	pm = AddMultisetElement (&id_element, 21965); 
	pm = AddMultisetElement (&id_element, 21966); 
	pm = AddMultisetElement (&id_element, 21967); 
	pm = AddMultisetElement (&id_element, 21968); 
	pm = AddMultisetElement (&id_element, 21969); 
	pm = AddMultisetElement (&id_element, 21970); 
	pm = AddMultisetElement (&id_element, 21971); 
	pm = AddMultisetElement (&id_element, 21972); 
	pm = AddMultisetElement (&id_element, 21973); 
	pm = AddMultisetElement (&id_element, 21974); 
	pm = AddMultisetElement (&id_element, 21975); 
	pm = AddMultisetElement (&id_element, 21976); 
	pm = AddMultisetElement (&id_element, 21977); 
	pm = AddMultisetElement (&id_element, 21978); 
	pm = AddMultisetElement (&id_element, 21979); 
	pm = AddMultisetElement (&id_element, 21980); 
	pm = AddMultisetElement (&id_element, 21981); 
	pm = AddMultisetElement (&id_element, 21982); 
	pm = AddMultisetElement (&id_element, 21983); 
	pm = AddMultisetElement (&id_element, 21984); 
	pm = AddMultisetElement (&id_element, 21985); 
	pm = AddMultisetElement (&id_element, 21986); 
	pm = AddMultisetElement (&id_element, 21987); 
	pm = AddMultisetElement (&id_element, 21988); 
	pm = AddMultisetElement (&id_element, 21989); 
	pm = AddMultisetElement (&id_element, 21990); 
	pm = AddMultisetElement (&id_element, 21991); 
	pm = AddMultisetElement (&id_element, 21992); 
	pm = AddMultisetElement (&id_element, 21993); 
	pm = AddMultisetElement (&id_element, 21994); 
	pm = AddMultisetElement (&id_element, 21995); 
	pm = AddMultisetElement (&id_element, 21996); 
	pm = AddMultisetElement (&id_element, 21997); 
	pm = AddMultisetElement (&id_element, 21998); 
	pm = AddMultisetElement (&id_element, 21999); 
	pm = AddMultisetElement (&id_element, 22000); 
	pm = AddMultisetElement (&id_element, 23000); 
	pm = AddMultisetElement (&id_element, 23001); 
	pm = AddMultisetElement (&id_element, 23002); 
	pm = AddMultisetElement (&id_element, 23003); 
	pm = AddMultisetElement (&id_element, 23004); 
	pm = AddMultisetElement (&id_element, 23005); 
	pm = AddMultisetElement (&id_element, 23006); 
	pm = AddMultisetElement (&id_element, 23007); 
	pm = AddMultisetElement (&id_element, 23008); 
	pm = AddMultisetElement (&id_element, 23009); 
	pm = AddMultisetElement (&id_element, 23010); 
	pm = AddMultisetElement (&id_element, 23011); 
	pm = AddMultisetElement (&id_element, 23012); 
	pm = AddMultisetElement (&id_element, 23013); 
	pm = AddMultisetElement (&id_element, 23014); 
	pm = AddMultisetElement (&id_element, 23015); 
	pm = AddMultisetElement (&id_element, 23016); 
	pm = AddMultisetElement (&id_element, 23017); 
	pm = AddMultisetElement (&id_element, 23018); 
	pm = AddMultisetElement (&id_element, 23019); 
	pm = AddMultisetElement (&id_element, 23020); 
	pm = AddMultisetElement (&id_element, 23021); 
	pm = AddMultisetElement (&id_element, 23022); 
	pm = AddMultisetElement (&id_element, 23023); 
	pm = AddMultisetElement (&id_element, 23024); 
	pm = AddMultisetElement (&id_element, 23025); 
	pm = AddMultisetElement (&id_element, 23026); 
	pm = AddMultisetElement (&id_element, 23027); 
	pm = AddMultisetElement (&id_element, 23028); 
	pm = AddMultisetElement (&id_element, 23029); 
	pm = AddMultisetElement (&id_element, 23030); 
	pm = AddMultisetElement (&id_element, 23031); 
	pm = AddMultisetElement (&id_element, 23032); 
	pm = AddMultisetElement (&id_element, 23033); 
	pm = AddMultisetElement (&id_element, 23034); 
	pm = AddMultisetElement (&id_element, 23035); 
	pm = AddMultisetElement (&id_element, 23036); 
	pm = AddMultisetElement (&id_element, 23037); 
	pm = AddMultisetElement (&id_element, 23038); 
	pm = AddMultisetElement (&id_element, 23039); 
	pm = AddMultisetElement (&id_element, 23040); 
	pm = AddMultisetElement (&id_element, 23041); 
	pm = AddMultisetElement (&id_element, 23042); 
	pm = AddMultisetElement (&id_element, 23043); 
	pm = AddMultisetElement (&id_element, 23044); 
	pm = AddMultisetElement (&id_element, 23045); 
	pm = AddMultisetElement (&id_element, 23046); 
	pm = AddMultisetElement (&id_element, 23047); 
	pm = AddMultisetElement (&id_element, 23048); 
	pm = AddMultisetElement (&id_element, 23049); 
	pm = AddMultisetElement (&id_element, 23050); 
	pm = AddMultisetElement (&id_element, 23051); 
	pm = AddMultisetElement (&id_element, 23052); 
	pm = AddMultisetElement (&id_element, 23053); 
	pm = AddMultisetElement (&id_element, 23054); 
	pm = AddMultisetElement (&id_element, 23055); 
	pm = AddMultisetElement (&id_element, 23056); 
	pm = AddMultisetElement (&id_element, 23057); 
	pm = AddMultisetElement (&id_element, 23058); 
	pm = AddMultisetElement (&id_element, 23059); 
	pm = AddMultisetElement (&id_element, 23060); 
	pm = AddMultisetElement (&id_element, 23061); 
	pm = AddMultisetElement (&id_element, 23062); 
	pm = AddMultisetElement (&id_element, 23063); 
	pm = AddMultisetElement (&id_element, 23064); 
	pm = AddMultisetElement (&id_element, 23065); 
	pm = AddMultisetElement (&id_element, 23066); 
	pm = AddMultisetElement (&id_element, 23067); 
	pm = AddMultisetElement (&id_element, 23068); 
	pm = AddMultisetElement (&id_element, 23069); 
	pm = AddMultisetElement (&id_element, 23070); 
	pm = AddMultisetElement (&id_element, 23071); 
	pm = AddMultisetElement (&id_element, 23072); 
	pm = AddMultisetElement (&id_element, 23073); 
	pm = AddMultisetElement (&id_element, 23074); 
	pm = AddMultisetElement (&id_element, 23075); 
	pm = AddMultisetElement (&id_element, 23076); 
	pm = AddMultisetElement (&id_element, 23077); 
	pm = AddMultisetElement (&id_element, 23078); 
	pm = AddMultisetElement (&id_element, 23079); 
	pm = AddMultisetElement (&id_element, 23080); 
	pm = AddMultisetElement (&id_element, 23081); 
	pm = AddMultisetElement (&id_element, 23082); 
	pm = AddMultisetElement (&id_element, 23083); 
	pm = AddMultisetElement (&id_element, 23084); 
	pm = AddMultisetElement (&id_element, 23085); 
	pm = AddMultisetElement (&id_element, 23086); 
	pm = AddMultisetElement (&id_element, 23087); 
	pm = AddMultisetElement (&id_element, 23088); 
	pm = AddMultisetElement (&id_element, 23089); 
	pm = AddMultisetElement (&id_element, 23090); 
	pm = AddMultisetElement (&id_element, 23091); 
	pm = AddMultisetElement (&id_element, 23092); 
	pm = AddMultisetElement (&id_element, 23093); 
	pm = AddMultisetElement (&id_element, 23094); 
	pm = AddMultisetElement (&id_element, 23095); 
	pm = AddMultisetElement (&id_element, 23096); 
	pm = AddMultisetElement (&id_element, 23097); 
	pm = AddMultisetElement (&id_element, 23098); 
	pm = AddMultisetElement (&id_element, 23099); 
	pm = AddMultisetElement (&id_element, 23100); 
	pm = AddMultisetElement (&id_element, 23101); 
	pm = AddMultisetElement (&id_element, 23102); 
	pm = AddMultisetElement (&id_element, 23103); 
	pm = AddMultisetElement (&id_element, 23104); 
	pm = AddMultisetElement (&id_element, 23105); 
	pm = AddMultisetElement (&id_element, 23106); 
	pm = AddMultisetElement (&id_element, 23107); 
	pm = AddMultisetElement (&id_element, 23108); 
	pm = AddMultisetElement (&id_element, 23109); 
	pm = AddMultisetElement (&id_element, 23110); 
	pm = AddMultisetElement (&id_element, 23111); 
	pm = AddMultisetElement (&id_element, 23112); 
	pm = AddMultisetElement (&id_element, 23113); 
	pm = AddMultisetElement (&id_element, 23114); 
	pm = AddMultisetElement (&id_element, 23115); 
	pm = AddMultisetElement (&id_element, 23116); 
	pm = AddMultisetElement (&id_element, 23117); 
	pm = AddMultisetElement (&id_element, 23118); 
	pm = AddMultisetElement (&id_element, 23119); 
	pm = AddMultisetElement (&id_element, 23120); 
	pm = AddMultisetElement (&id_element, 23121); 
	pm = AddMultisetElement (&id_element, 23122); 
	pm = AddMultisetElement (&id_element, 23123); 
	pm = AddMultisetElement (&id_element, 23124); 
	pm = AddMultisetElement (&id_element, 23125); 
	pm = AddMultisetElement (&id_element, 23126); 
	pm = AddMultisetElement (&id_element, 23127); 
	pm = AddMultisetElement (&id_element, 23128); 
	pm = AddMultisetElement (&id_element, 23129); 
	pm = AddMultisetElement (&id_element, 23130); 
	pm = AddMultisetElement (&id_element, 23131); 
	pm = AddMultisetElement (&id_element, 23132); 
	pm = AddMultisetElement (&id_element, 23133); 
	pm = AddMultisetElement (&id_element, 23134); 
	pm = AddMultisetElement (&id_element, 23135); 
	pm = AddMultisetElement (&id_element, 23136); 
	pm = AddMultisetElement (&id_element, 23137); 
	pm = AddMultisetElement (&id_element, 23138); 
	pm = AddMultisetElement (&id_element, 23139); 
	pm = AddMultisetElement (&id_element, 23140); 
	pm = AddMultisetElement (&id_element, 23141); 
	pm = AddMultisetElement (&id_element, 23142); 
	pm = AddMultisetElement (&id_element, 23143); 
	pm = AddMultisetElement (&id_element, 23144); 
	pm = AddMultisetElement (&id_element, 23145); 
	pm = AddMultisetElement (&id_element, 23146); 
	pm = AddMultisetElement (&id_element, 23147); 
	pm = AddMultisetElement (&id_element, 23148); 
	pm = AddMultisetElement (&id_element, 23149); 
	pm = AddMultisetElement (&id_element, 23150); 
	pm = AddMultisetElement (&id_element, 23151); 
	pm = AddMultisetElement (&id_element, 23152); 
	pm = AddMultisetElement (&id_element, 23153); 
	pm = AddMultisetElement (&id_element, 23154); 
	pm = AddMultisetElement (&id_element, 23155); 
	pm = AddMultisetElement (&id_element, 23156); 
	pm = AddMultisetElement (&id_element, 23157); 
	pm = AddMultisetElement (&id_element, 23158); 
	pm = AddMultisetElement (&id_element, 23159); 
	pm = AddMultisetElement (&id_element, 23160); 
	pm = AddMultisetElement (&id_element, 23161); 
	pm = AddMultisetElement (&id_element, 23162); 
	pm = AddMultisetElement (&id_element, 23163); 
	pm = AddMultisetElement (&id_element, 23164); 
	pm = AddMultisetElement (&id_element, 23165); 
	pm = AddMultisetElement (&id_element, 23166); 
	pm = AddMultisetElement (&id_element, 23167); 
	pm = AddMultisetElement (&id_element, 23168); 
	pm = AddMultisetElement (&id_element, 23169); 
	pm = AddMultisetElement (&id_element, 23170); 
	pm = AddMultisetElement (&id_element, 23171); 
	pm = AddMultisetElement (&id_element, 23172); 
	pm = AddMultisetElement (&id_element, 23173); 
	pm = AddMultisetElement (&id_element, 23174); 
	pm = AddMultisetElement (&id_element, 23175); 
	pm = AddMultisetElement (&id_element, 23176); 
	pm = AddMultisetElement (&id_element, 23177); 
	pm = AddMultisetElement (&id_element, 23178); 
	pm = AddMultisetElement (&id_element, 23179); 
	pm = AddMultisetElement (&id_element, 23180); 
	pm = AddMultisetElement (&id_element, 23181); 
	pm = AddMultisetElement (&id_element, 23182); 
	pm = AddMultisetElement (&id_element, 23183); 
	pm = AddMultisetElement (&id_element, 23184); 
	pm = AddMultisetElement (&id_element, 23185); 
	pm = AddMultisetElement (&id_element, 23186); 
	pm = AddMultisetElement (&id_element, 23187); 
	pm = AddMultisetElement (&id_element, 23188); 
	pm = AddMultisetElement (&id_element, 23189); 
	pm = AddMultisetElement (&id_element, 23190); 
	pm = AddMultisetElement (&id_element, 23191); 
	pm = AddMultisetElement (&id_element, 23192); 
	pm = AddMultisetElement (&id_element, 23193); 
	pm = AddMultisetElement (&id_element, 23194); 
	pm = AddMultisetElement (&id_element, 23195); 
	pm = AddMultisetElement (&id_element, 23196); 
	pm = AddMultisetElement (&id_element, 23197); 
	pm = AddMultisetElement (&id_element, 23198); 
	pm = AddMultisetElement (&id_element, 23199); 
	pm = AddMultisetElement (&id_element, 23200); 

 
	/************ INSTANCES GENERATION *************/
	GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
	GeneratesInstancesRx (&multiset_M2, &instances_I2, &usedinstances_I2);
	GeneratesInstancesRx (&multiset_M3, &instances_I3, &usedinstances_I3);

 
	/****************** MAIN LOOP ******************/
	while ((instances_I1 != NULL) || (instances_I2 != NULL) || (instances_I3 != NULL)) { 
 
		/*************** GRAPH GENERATION **************/
		//Creating Initial Graph (from I1 Instances List) 
		pix = instances_I1; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 1); 
			CreateOutputEdges (data, 1); 
			element_I1.data = pix->data; 
			element_I1.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I1, element_I1); 
			pix = pix->next; 
		} 
 
 
		//Creating Initial Graph (from I2 Instances List) 
		pix = instances_I2; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 2); 
			CreateOutputEdges (data, 2); 
			element_I2.data = pix->data; 
			element_I2.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I2, element_I2); 
			pix = pix->next; 
		} 
 
 
		//Creating Initial Graph (from I3 Instances List) 
		pix = instances_I3; 
		while(pix != NULL){ 
			data.element1 = pix->data.element1; 
			data.element2 = pix->data.element2; 
			pv = AddVertex (&id_vertex, data, 3); 
			CreateOutputEdges (data, 3); 
			element_I3.data = pix->data; 
			element_I3.id_data = pix->id_data; 
			pi = AddInstanceIx (&usedinstances_I3, element_I3); 
			pix = pix->next; 
		} 
 
 
		/************ SINKS IDENTIFICATION *************/ 
		IdentifySinks(); 
 
 
		/*********** THREADS INITIALIZATION ************/ 
		while(vertices != NULL){ 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_create(&threads[ps->id], NULL, ThreadFunction, (void *)(&(*ps))); 
				ps = ps->next; 
			} 
			// Finishing the threads 
			ps = sinks; 
			while (ps != NULL){ 
				pthread_join(threads[ps->id], NULL); 
				ps = ps->next; 
			} 
 
			// Delete the sink list before starting another sinks identification... 
			DeleteAllSinks(); 
 
			// Identify new sinks to start another iteration... 
			IdentifySinks(); 
		} 
 
 
		/********** NEW INSTANCES GENERATION ***********/
		DeleteAllInstanceIx (&instances_I1);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
		DeleteAllInstanceIx (&instances_I2);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M2, &instances_I2, &usedinstances_I2);
		DeleteAllInstanceIx (&instances_I3);    // Delete all instance list before starting another iteration...
		GeneratesInstancesRx (&multiset_M3, &instances_I3, &usedinstances_I3);

 
		//Allowing the opportunity to generate again some instances...
		if((instances_I1==NULL) && (!allow_1)){
			allow_1 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I1);
			GeneratesInstancesRx (&multiset_M1, &instances_I1, &usedinstances_I1);
		}
 
		//Allowing the opportunity to generate again some instances...
		if((instances_I2==NULL) && (!allow_2)){
			allow_2 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I2);
			GeneratesInstancesRx (&multiset_M2, &instances_I2, &usedinstances_I2);
		}
 
		//Allowing the opportunity to generate again some instances...
		if((instances_I3==NULL) && (!allow_3)){
			allow_3 = TRUE;
			DeleteAllInstanceIx(&usedinstances_I3);
			GeneratesInstancesRx (&multiset_M3, &instances_I3, &usedinstances_I3);
		}
 
	}		// End of the Main Loop 
 
 
	// Deleting the history of instances provided to both reactions... 
	DeleteAllInstanceIx (&usedinstances_I1); 
	DeleteAllInstanceIx (&usedinstances_I2); 
	DeleteAllInstanceIx (&usedinstances_I3); 

 
	printf("\n \n"); 
	printf("######################## \n"); 
	printf("END OF THE PROGRAM! \n \n"); 
	printf("Final Multiset: \n \n"); 
	ListingMultiset(); 
	printf("\n"); 
	printf("######################## \n"); 
 
	exit (0); 
} 
 
 
/***************************************************************/ 
/*******  Function related to the R1 reaction execution ********/ 
/***************************************************************/ 
int R1Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: first 
	int x, y, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 

	x  = ps->data.element1; 
	y  = ps->data.element2; 
 
	if (((x <=  20100 ) && (y <=  20100 ))) { 
		element_Ix.data.element1 = ps->data.element1; 
		element_Ix.data.element2 = ps->data.element2; 
		result = (x + y); 

    	// Test - Matrix multiplication
		int line,row, i, aux;
    	int dim = 100;
    	int mat1[dim][dim], mat2[dim][dim], mat3[dim][dim];
    	for(line=0; line<dim; line++)
        	for(row=0; row<dim; row++){
            	aux=0;
            	for(i=0; i<dim; i++) {
                	aux = aux + (mat1[line][i]*mat2[i][row]);
            	}
            	mat3[line][row]=aux;
        	}
		// End Test Matrix Multiplication


		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I1, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 
/***************************************************************/ 
/*******  Function related to the R2 reaction execution ********/ 
/***************************************************************/ 
int R2Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: second 
	int m, n, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 

	m  = ps->data.element1; 
	n  = ps->data.element2; 

	if ((((((m > n) && (m >=  21000 )) && (m <=  22000 )) && (n >=  21000 )) && (n <=  22000 ))) { 
		element_Ix.data.element1 = ps->data.element1; 
		element_Ix.data.element2 = ps->data.element2; 
		result = m; 

		// Test - Matrix multiplication
		int line,row, i, aux;
    	int dim = 100;
    	int mat1[dim][dim], mat2[dim][dim], mat3[dim][dim];
    	for(line=0; line<dim; line++)
        	for(row=0; row<dim; row++){
            	aux=0;
            	for(i=0; i<dim; i++) {
                	aux = aux + (mat1[line][i]*mat2[i][row]);
            	}
            	mat3[line][row]=aux;
        	}
		// End Test Matrix Multiplication

		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I2, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 
/***************************************************************/ 
/*******  Function related to the R3 reaction execution ********/ 
/***************************************************************/ 
int R3Function(reg_sink *ps){ 
	// Name of the gamma reaction - according the gamma code: third 
	int a, b, result; 
	reg_multiset *pm; 
	reg_I element_Ix, *pi; 
 
	a  = ps->data.element1; 
	b  = ps->data.element2; 
 
	if (((a >=  23000 ) && (b >=  23000 ))) { 
		element_Ix.data.element1 = ps->data.element1; 
		element_Ix.data.element2 = ps->data.element2; 
		result = ((a + b) -  10000 ); 

		// Test - Matrix multiplication
		int line,row, i, aux;
		int dim = 100;
		int mat1[dim][dim], mat2[dim][dim], mat3[dim][dim];
		for(line=0; line<dim; line++)
			for(row=0; row<dim; row++){
				aux=0;
				for(i=0; i<dim; i++) {
					aux = aux + (mat1[line][i]*mat2[i][row]);
				}
				mat3[line][row]=aux;
			}
		// End Test Matrix Multiplication

		sem_wait(&sem_multiset); 
		pm = AddMultisetElement (&id_element, result); 
		pi = SearchInstanceIx(&instances_I3, element_Ix); 
		DeleteMultisetElementId (pi->id_data.id1); 
		DeleteMultisetElementId (pi->id_data.id2); 
		sem_post(&sem_multiset); 
		return TRUE;  
	} 
	else { 
		return FALSE; 
	} 
} 
 

/***************************************************************/ 
/*******  Function for the Threads fired by the Sinks **********/ 
/***************************************************************/ 
void * ThreadFunction (void *arg){ 
	reg_sink *ps; 
	reg_multiset *pm; 
	ps = (reg_sink *)(arg); 
	int reacted; 
 
	/************** REACTION EXECUTION *************/ 
	if (ps->reaction == 1){   // Try to execute the reaction 1 ... 
		reacted = R1Function(ps); 
	} 
	if (ps->reaction == 2){   // Try to execute the reaction 2 ... 
		reacted = R2Function(ps); 
	} 
	if (ps->reaction == 3){   // Try to execute the reaction 3 ... 
		reacted = R3Function(ps); 
	} 

	sem_wait(&sem_graph); 
 
	EdgesReversalSink(ps); 
 
	/********* VERTICES AFFECTED REMOVAL ***********/ 
	/******************** AND **********************/ 
	/**************** SINK REMOVAL *****************/ 
 
	if (reacted){ 
		DeleteVerticesAffected(ps);     // all vertices affected, included its own vertex 
	} 
	else { 
		DeleteVertex(ps->data, ps->reaction);   // Delete the vertex that execute as a Sink 
	} 
 
	sem_post(&sem_graph);  
 
}  
 
// Tests Rui - Definitions 
// Program->p_defs->d_definition->d_name: first 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: x 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: y 

// Program->p_defs->d_definition->d_name: second 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: m 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: n 

// Program->p_defs->d_definition->d_name: third 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_expression->e_val.e_name: a 
// Program->p_defs->d_definition->d_body->b_val.b_bbody->b_pattern->p_next->p_expression->e_val.e_name: b 


